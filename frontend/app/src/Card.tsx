import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom'
import { RootState } from './providers/store';
import { useSelector } from "react-redux";
import {backendURL} from "./api/routes";

function Card({ title }) {
    const currentUser = useSelector((state: RootState) => state.userProvider.user);
    const [users, setUsers] = useState([])
    const status = new Map()
    status.set("online", "bg-primary")
    status.set("offline", "bg-secondary")
    status.set("playing", "bg-success")
    status.set("watching", "bg-success")
    status.set("queueing", "bg-warning")

  function sortByScore(users) {
    const ranked = users.slice(0);
    ranked.sort(function(a,b) {
      return b.max_score - a.max_score;
    })
    return ranked;
  }

  useEffect(() => {
    axios.get(backendURL + '/users').then( users => {
        if (title === 'Leaderboard') {
          let ranked = sortByScore(users.data)
          setUsers(ranked)
        }
        else if (title === 'Friends') {
          let friends = users.data.filter(u => currentUser["friends"]?.includes(u.intra_id.toString()))
          setUsers(friends)
        }
        else
          setUsers(users.data)
      })
  }, [currentUser, title]);

  return (
      <>
        <div className="col mx-4 h-100 justify-content-center">
          <div className="card h-100 border-light mb-3 w-100">
            <div className="card-header">
                <h5>{ title }</h5>
            </div>
            <div className="card-body">
              { 
                users && users.length ?
                users.map((u) =>
                    <div key={ u["uid"] } className="card-body">
                      <img src={ u["avatar"] } className="rounded-circle mx-2" width="40" height="40" alt=""/>
                      <Link key={ u["uid"] } style={{ color: 'black' }} className="card-title" to={`/users/${u["intra_id"]}`}>{ u["username"] }
                        { 
                          title === 'Leaderboard' ?
                            <span className={`badge float-end ${status.get(u["status"])}`}>{u["max_score"]} points</span>
                          :
                            <span className={`badge float-end ${status.get(u["status"])}`}>{u["status"]}</span>
                        }
                      </Link>
                    </div>)
                :
                <h6>No { title } yet</h6>
              }
            </div>
          </div>
        </div>
      </>
  )
}

export default Card
