import './style/Profile.css'
import { FC, useState } from "react";
import { Link, useNavigate } from 'react-router-dom'
import { useSelector } from "react-redux";
import { RootState } from './providers/store';
import Card from "./Card"
import EditProfile from "./EditProfile"
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import axios from 'axios';
import { toast } from 'react-toastify';
import {backendURL} from "./api/routes";
import GameHistory from "./GameHistory";

const Dashboard: FC = () =>  {
    const user = useSelector((state: RootState) => state.userProvider.user);
    const [dialog,setDialog] = useState(false)
    const [password,setPassword] = useState('')
    const navigate = useNavigate()

    function onCreateChat() {
        const typechat = "public";
        const owner = user["intra_id"].toString();
        const members = owner
        const admin = []
        if (password == null)
            setPassword("")
        axios({
            url: backendURL + "/chat/",
            method: "POST",
            data: { typechat, owner, password, members, admin }
        }).then((res) => {
            toast.success("New Channel Created")
            navigate("/channelchats")
        }).catch(e => { toast.error(e)    })
    }

    return (
        <>
        {
            user["first_login"] ?
                <EditProfile />
            :
            
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="profile">
                                <div className="profile-header">
                                    <div className="profile-header-content">
                                        <div className="profile-header-img">
                                            <img style={{ objectFit: "cover" }} className="img-fluid h-100" src={ user["avatar"] } alt=""/>
                                        </div>
                                        <div className="profile-header-info">
                                            <h4 className="m-t-10 m-b-5">{ user["username"] }</h4>
                                            { user["title"] && <h6 className="m-b-10 fst-italic">{ user["title"] }</h6> }
                                            { user["mail"] && <h6 className="m-b-10">MP: { user["mail"] }</h6> } 
                                            <Link to="/editProfile" className="btn btn-sm btn-info mt-2">Edit Profile</Link>
                                            <Link to="/play" className="btn btn-sm float-end btn-success mt-2 mx-2">Start New Game</Link>
                                            <button onClick={() => setDialog(true)} className="btn btn-sm float-end btn-warning mt-2">Start New Chat</button>
                                            {
                                                <Dialog open={dialog} onClose={onCreateChat}>
                                                <DialogTitle>New Channel Chat</DialogTitle>
                                                <DialogContent>
                                                  <DialogContentText>
                                                    Enter a password for your channel (or leave it blank)
                                                  </DialogContentText>
                                                  <TextField
                                                    autoFocus
                                                    margin="dense"
                                                    id="psw"
                                                    label="Password"
                                                    type="password"
                                                    fullWidth
                                                    variant="standard"
                                                    onChange={ (e) => setPassword(e.target.value) }
                                                  />
                                                </DialogContent>
                                                <DialogActions>
                                                  <button onClick={() => setDialog(false)} className="btn btn-sm float-end btn-danger mt-2">Cancel</button>
                                                  <button onClick={onCreateChat} className="btn btn-sm float-end btn-primary mt-2">Create New Channel</button>
                                                </DialogActions>
                                              </Dialog>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container vh-100 my-5">
                        <div className="row h-100 justify-content-center mx-5">
                            <Card title="Users status"/>
                            <Card title="Friends"/>
                            <Card title="Leaderboard"/>
                        </div>
                    </div>
                    {
                        user && <GameHistory userProps={user} />
                    }
                </div>
            
        }
        </>
    );
}

export default Dashboard;
