import { useDispatch, useSelector } from "react-redux";
import {backendURL, routes} from "./api/routes";
import {useCallback, useContext, useEffect} from "react";
import { setCurrentUser, setToken } from "./providers/userProvider";
import {Link, useNavigate} from "react-router-dom"
import { RootState } from "./providers/store";
import jsCookie from "js-cookie";
import SearchBar from "./Searchbar";
import {GameSocketContext} from "./context/gameSocket";
import axios from "axios";

function Navbar() {
    const loggedIn = useSelector((state: RootState) => state.userProvider.token);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const gameSocket = useContext(GameSocketContext);

    const onHandleClick = useCallback((event) => {
        event.preventDefault();
        document.cookie = "connect.sid=false, Max-age=-100000000000"
        window.location.href = routes.logout + "?user=" + jsCookie.get('jwt_token');
    }, []);

    useEffect(() => {
        gameSocket.on("invitedToPlay", (data) => {
            gameSocket.emit("joinPrivateRoom", {user: data.user, roomId: data.roomId});
            navigate("/play", {replace: true});
        });

        gameSocket.off("updateUserAfterGame").on("updateUserAfterGame", (data) => {
            axios({
                url: backendURL + "/users/" + parseInt(data.intra_id),
                method: "PATCH",
                headers: {
                    Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
                },
                data: {
                    status: "online",
                    max_score: data.score,
                    games: data.games,
                }
            }).then(res => {
                dispatch(setCurrentUser(res.data));
            }).catch(err => console.error(err));
        });
        dispatch(setToken(''));
        dispatch(setCurrentUser(''));
    }, [onHandleClick, dispatch, gameSocket, navigate]);

    return (
        <>
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="./">42</Link>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                            </li>
                            {
                                loggedIn ?
                                <>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/play">Games</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/channelchats">Chats</Link>
                                    </li>
                                </>
                                    :
                                    <div></div>
                            }
                        </ul>
                        
                        {
                            loggedIn ?    
                            <>
                                {/* <button type="button" className="btn btn-secondary position-relative mx-3">
                                    &#128276;
                                    <span className="position-absolute top-0 start-100 translate-middle p-2 bg-danger border border-light rounded-circle">
                                        <span className="visually-hidden">alert</span>
                                    </span>
                                </button> */}
                                <SearchBar />
                                <ul className="navbar-nav mx-2 mb-2 mb-lg-0">
                                    <li className="nav-item">
                                        <Link to="*" className="nav-link" onClick={ (event) => onHandleClick(event) }>Log Out</Link>
                                    </li>
                                </ul>
                            </>
                            :
                            <div></div>
                        }                        
                    </div>
                </div>
            </nav>
        </>
    )
}

export default Navbar
