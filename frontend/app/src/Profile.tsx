import './style/Profile.css'
import { useParams, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from "react-redux";
import React, {useEffect, useState, FC, useContext } from 'react';
import {setCurrentRoom, setCurrentUser} from "./providers/userProvider";
import { RootState } from './providers/store';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import LoadingPage from './LoadingPage'
import Chat from './Chat';
import jsCookie from 'js-cookie'
import InviteToPlay from "./pong/InviteToPlay";
import GameHistory from "./GameHistory";
import {GameSocketContext} from "./context/gameSocket";
import {backendURL} from "./api/routes";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';

const Profile: FC = () => {
    const currentUser = useSelector((state: RootState) => state.userProvider.user);         // user logged in
    const params = useParams();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [blocked,setBlocked] = useState(false)
    const [friend,setFriend] = useState(false)
    const [chatting,setChatting] = useState(false)
    const [show,setShow] = useState(true)
    const [user, setUser] = useState<null | any>(null)                                      // user to which the profile belongs
    const room_uid = useSelector((state: RootState) => state.userProvider.roomId);
    const gameSocket = useContext(GameSocketContext);

    function onBefriendUser() {
        let request = backendURL + "/users/friends/add/";
        if (friend)
            request = backendURL + "/users/friends/delete/";
        const friends = params.id
        axios({
            url: request,
            method: "POST",
            headers: {
                Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
            },
            data: { friends }
        })
        .then(
            u => {

                dispatch(setCurrentUser(u.data))
                setFriend(!friend)
        }).catch(e => {
            toast.error(e);
        })
    }
    function onJoinSpec() {
        gameSocket.emit("joinSpecFromProfile", {username: user.username});
    }

    function onBlockUser() {
        let request = backendURL + "/users/block/add/" + params.id;
        if (blocked)
            request = backendURL + "/users/block/delete/" + params.id;
        axios({
            url: request,
            method: "GET",
            headers: {
                Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
            }
        })
        .then(
            u => {
                dispatch(setCurrentUser(u.data));
                setBlocked(!blocked);
                setChatting(false);
                gameSocket.emit("updateBlockUser", {user: currentUser, target: user, isBlocked: blocked});
            }).catch(e => {
            toast.error(e);
        })
    }

    async function onStartChat() {
        const password = ""
        if (chatting === false) {
            axios({
                url: backendURL + "/chat/findroom/" + room_uid,
                method: "GET",
            }).then(res => {
                if (!res.data.hasOwnProperty("id")) {
                    const typechat = "friends"
                    const name = ""
                    axios({
                        url: backendURL + "/chat/",
                        method: "POST",
                        data: { typechat, name, room_uid, password }
                    })
                }
            }).catch(e => { toast.error(e) })
        }
        setChatting(!chatting)
    }

    useEffect(() => {
        if (!user || !currentUser)
            return ;
        setFriend(currentUser["friends"].includes(params.id));
        if (user["blacklist"].includes(currentUser["intra_id"].toString())) {
            setShow(false);
        }

        if (currentUser["blacklist"].includes(user["intra_id"].toString()))
            setBlocked((true));
    }, [currentUser, user, params.id]);

    useEffect(() => {
		if (!currentUser)
			return ;
        if (currentUser["intra_id"] === parseInt(params.id as string)) {
                navigate("/"); return ;
            }
	axios({
            url: backendURL + "/users/" + params.id,
            method: "GET",
            headers: {
                Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
            }
        }).then(u => {
            if (!u.data) {
                toast.error("Username not found");
                return ;
            }
            setUser(u.data);
            if (currentUser["intra_id"] === parseInt(params.id as string)) {
                navigate("/");
            }
            dispatch(setCurrentRoom(u.data["intra_id"].toString()))

        }).catch(e => { return ; 
/*if (!e.response) {
        console.error("Already on profile");
        return;
    }
    if (e.response !== undefined && e.response.status === 404) {
        navigate("/notfound");
    } else {
        console.error(e);
    }*/
        });

        gameSocket.off('joinSpecFromProfile').on('joinSpecFromProfile', (data) => {
            gameSocket.emit("joinSpec", { user: currentUser, gameId: data.gameId, fromProfile: true });
            navigate("/play", {replace: true});
        });
    }, [currentUser, gameSocket, navigate, params.id, dispatch]);

    useEffect(() => {
        if (user && room_uid) {
            gameSocket.emit('join_room', { name: room_uid, intra_id: currentUser["intra_id"] } )
            gameSocket.off("updateBlockUser").on("updateBlockUser", (data) => {
                if (!data.isBlocked) {
                    toast.warning("You have been blocked by " + data.user.username);
                    setShow(false);
                    setChatting(false);
                } else {
                    toast.success("You have been unblocked by " + data.user.username);
                    setShow(true);
                }
            });
        }
    }, [show, gameSocket, room_uid, user, currentUser]);

    if (!user)
        return <LoadingPage />

    return (
        <>
        {
            user ?
            <>
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="profile">
                            <div className="profile-header">
                                <div className="profile-header-content">
                                    <div className="profile-header-img">
                                        <img style={{ objectFit: "cover" }} className="img-fluid w-100 h-100" src={ user["avatar"] } alt=""/>
                                    </div>
                                    <div className="profile-header-info">
                                        <h4 className="m-t-10 m-b-5">{ user["username"] }
                                            {
                                                show ?
                                                    <span className="float-end">
                                                        <button onClick={onBefriendUser}
                                                                className="btn btn-sm top-0 end-0 btn-success mx-2"
                                                                disabled={blocked}>
                                                            { friend ? "Unfriend" : "Add Friend" }
                                                        </button>
                                                        <button onClick={onBlockUser}
                                                                className="btn btn-sm top-0 end-0 btn-warning mx-2">
                                                            {blocked ? "Unblock" : "Block User"}
                                                        </button>
                                                        <button onClick={onStartChat}
                                                                className="btn btn-sm top-0 end-0 btn-primary mx-2"
                                                                disabled={blocked}>
                                                            {chatting ? "Close Chat" : "Start Chat"}
                                                        </button>
                                                        {
                                                            currentUser.status === "online" && user.status === "playing" &&
                                                            <button onClick={onJoinSpec} className="btn btn-sm top-0 end-0 btn-outline-dark mx-2">
                                                                In a game <FontAwesomeIcon icon={faEye}/>
                                                            </button>
                                                        }
                                                        {
                                                            currentUser.status === "online" && user.status === "online" &&
                                                            <InviteToPlay disabled={blocked} userProps={currentUser} targetProps={user}/>
                                                        }
                                                    </span>

                                                :
                                                    <p className="ml-0 my-4">You have been blocked by this user!</p>
                                            }
                                        </h4>
                                        {
                                            user["title"] && show ?
                                            <h6 className="m-b-10 fst-italic">{ user["title"] }</h6>
                                            :
                                            show && <h6 className="m-b-10 fst-italic">[No title...]</h6>
                                        }
                                        { user["mail"] && show && <h6 className="m-b-10">MP: { user["mail"] }</h6> }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-12">
                        {
                            chatting ?
                                <Chat friend={user} roomid={room_uid}></Chat>
                            :
                            <></>
                        }
                    </div>
                    <div className="col-md-12">
                        {show && <GameHistory userProps={user}/>}
                    </div>
                </div>
            </div>
            <ToastContainer autoClose={500}/>
            </>
        :
        <LoadingPage/>
        }
        </>

    );
}

export default Profile;
