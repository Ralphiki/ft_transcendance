import React, {useCallback, useEffect, useState} from "react";
import axios from "axios";
import jsCookie from "js-cookie";
import Loading from "./LoadingPage";
import {backendURL} from "./api/routes";

function GameHistory({ userProps: user }) {
    const [games, setGames] = useState<null | Array<Object>>([]);
    const [loading, setLoading] = useState<boolean>(true);

    const getGamesById = useCallback(async () => {
        let games = [];
        for (const id of user.games) {
            try {
                const res = await axios({
                    url: backendURL + "/games/find/" + id,
                    method: "GET",
                    headers: {
                        Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
                    }
                });
                games.push(res.data);
            } catch (e) {
                console.error(e);
            }
        }
        return games;
    }, [user.games]);

    useEffect(() => {
        getGamesById().then((res) => {
            setGames(res.reverse());
            setLoading(false);
        });
    }, [getGamesById, loading]);

    return (
        <>
            <div className="content">
                <div className="container p-0">
                    <div className="card m-5">
                        <div className="col-12 col-lg-7 col-xl-9 m-auto">
                            <div className="py-2 px-4 border-bottom d-none d-lg-block">
                                <div className="d-flex align-items-center py-1">
                                    Game History
                                </div>
                            </div>

                            <div className="position-relative">
                                <div className="games p-4">
                                    { loading ? <Loading /> : null }

                                    {
                                        (games?.length && !loading) ?
                                            games.map((game,idx) => {
                                                const date = new Date(game.createdAt);
                                                const formattedDate = `${date.toLocaleDateString('fr-FR', { month: '2-digit', day: '2-digit', year: '2-digit' })} at ${date.toLocaleTimeString('fr-FR', { hour: '2-digit', minute:'2-digit' })}`;
                                                return (
                                                    <div key={idx} className="game pb-4">
                                                        {game.player1} {game.score1} - {game.score2} {game.player2} in mode {game.mode} on {formattedDate}
                                                    </div>
                                                )
                                            })
                                        :
                                            null
                                    }
                                    {
                                        (!games?.length && !loading) ?
                                            <h3>{user.username} never played any game !</h3>
                                        :
                                            null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default GameHistory;
