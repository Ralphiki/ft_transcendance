import React, {useEffect, useState} from "react";
import axios from "axios";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEye} from "@fortawesome/free-solid-svg-icons";
import NoGames from "../assets/NoGames.gif";
import Loading from "../LoadingPage";
import {backendURL} from "../api/routes";

const GameList: React.FC<{ onJoinSpec: any }> = ({onJoinSpec}) =>  {
    const [activeGames, setActiveGames] = useState<null | Array<Object>>([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios({
            url: backendURL + "/games/active",
            method: "GET"
        }).then(res => {
            setActiveGames(res.data.reverse());
            setLoading(false);
        }).catch(err => console.error(err));
    }, []);

    return(
        <>
            { loading ? <Loading /> : null }
            <div className="card border-light m-5 col-10">
                {
                    activeGames?.length && !loading ?
                        <>
                        <div className="card-header p-3 text-center">
                            <h5>Active Games</h5>
                        </div>
                            <div className="card-body">
                                <div className="p-3 col-12 activeGames">
                                    {
                                        activeGames?.map((game =>
                                                <div key={game["id"]} className="col-6 m-auto activeGame">
                                                    <div className="my-5">
                                                        <img src={ game["p1Avatar"] } className="rounded-circle m-1" width="40" height="40" alt=""/>
                                                        [{game["player1"]}]  VS  [{game["player2"]}]
                                                        <img src={ game["p2Avatar"] } className="rounded-circle m-1" width="40" height="40" alt=""/>
                                                        <button className="btn btn-outline-dark float-end" onClick={() => onJoinSpec(game["id"])}>Watch Game<FontAwesomeIcon className="mx-2" icon={faEye}/></button>
                                                    </div>
                                                </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </>
                        
                    :
                        null
                }
                {
                    !activeGames?.length && !loading ?
                        <>
                            <div className="card-header">
                                <h5>There is actually no game ...</h5>
                            </div>
                            <div className="card-body"><img src={NoGames} alt="no_games.gif" /></div>
                        </>
                    :
                        null
                }
            </div>
        </>
    );
}

export default GameList;
