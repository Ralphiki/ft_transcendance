import React, {FC, useEffect, useRef, useState} from 'react';
import {Socket} from "socket.io-client";
import './pong.css'
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBook, faBookOpen, faWindowMinimize, faArrowDown, faArrowUp} from "@fortawesome/free-solid-svg-icons";
import {useSelector} from "react-redux";
import {RootState} from "../providers/store";
import {PowerUp} from "./PowerUp";
import Loading from "../LoadingPage";

const CANVAS_WIDTH = 1080;
const CANVAS_HEIGHT = 600;
const PAD_WIDTH = 15;
const BALL_RAD = 8;
const MAX_SCORE = 3;

const Pong: FC<{socketProps: Socket, roomProps: any, onBack2GameMenu: any, isSpec: boolean}> = ({socketProps, roomProps, onBack2GameMenu, isSpec}) =>
{
    const user = useSelector((state: RootState) => state.userProvider.user);
    const canvasRef = useRef<null | HTMLCanvasElement>(null);
    const contextCanvas = useRef<CanvasRenderingContext2D | null>(null);
    const roomRef = useRef<any>(roomProps);
    const [gameRunning, setGameRunning] = useState(true);
    const [ballLaunched, setBallLaunched] = useState(false);
    const [scoreP1, setScoreP1] = useState(0);
    const [scoreP2, setScoreP2] = useState(0);
    const [winner, setWinner] = useState('');
    const [leaver, setLeaver] = useState('');
    const [openManual, setOpenManual] = useState(false);
    const [canvasWidth, setCanvasWidth] = useState(1080);
    const [canvasHeight, setCanvasHeight] = useState(600);
    let socket: Socket = socketProps;

    function onKeyDown(event: KeyboardEvent) {
        socket.emit("onKeyDown", {roomId: roomRef.current["roomId"], keycode: event.key, userId: user.intra_id});
    }

    const leave = () => {
        setGameRunning(false);
        setLeaver(user.username);
        if (user.username === roomRef.current["players"][1]["username"]) {
            setWinner(roomRef.current["players"][0]["username"]);
        } else {
            setWinner(roomRef.current["players"][1]["username"]);
        }
        socket.emit("leave", {roomId: roomRef.current["roomId"], user: user});
    }

    const drawGame = ((canvas, context) => {
        //clear && fill black screen
        context.clearRect(0, 0, canvasWidth, canvasHeight);
        context.fillRect(0, 0, canvasWidth, canvasHeight);

        //mid-line
        context.fillStyle = "white";
        context.fillRect((canvasWidth / 2) - (PAD_WIDTH / 2),0,
            PAD_WIDTH, canvasHeight);

        //pad P1
        const pad1X = roomRef.current["players"][0]["padX"] * (canvasWidth / CANVAS_WIDTH);
        const pad1Y = roomRef.current["players"][0]["padY"] * (canvasHeight / CANVAS_HEIGHT);
        const pad1W = PAD_WIDTH * (canvasWidth / CANVAS_WIDTH);
        const pad1H = roomRef.current["players"][0]["padH"] * (canvasHeight / CANVAS_HEIGHT);
        context.fillRect(pad1X, pad1Y, pad1W, pad1H);

        //pad P2
        const pad2X = roomRef.current["players"][1]["padX"] * (canvasWidth / CANVAS_WIDTH);
        const pad2Y = roomRef.current["players"][1]["padY"] * (canvasHeight / CANVAS_HEIGHT);
        const pad2W = PAD_WIDTH * (canvasWidth / CANVAS_WIDTH);
        const pad2H = roomRef.current["players"][1]["padH"] * (canvasHeight / CANVAS_HEIGHT);
        context.fillRect(pad2X, pad2Y, -pad2W, pad2H);

        //ball
        const ballX = roomRef.current["game"]["ballX"] * (canvasWidth / CANVAS_WIDTH);
        const ballY = roomRef.current["game"]["ballY"] * (canvasHeight / CANVAS_HEIGHT);
        const ballRad = (BALL_RAD * (canvasHeight / CANVAS_HEIGHT));
        context.beginPath();
        context.arc(ballX, ballY, ballRad, 0, Math.PI*2, false);
        context.closePath();
        context.fill();

        // powerUp
        if (roomRef.current["game"]["mode"] === "survival") {
            const powerUp = new PowerUp();
            if (roomRef.current["game"]["powerUpW"] * (canvasHeight / CANVAS_HEIGHT) > 0) {
                context.drawImage(powerUp.image, roomRef.current["game"]["powerUpX"] * (canvasHeight / CANVAS_HEIGHT),
                    roomRef.current["game"]["powerUpY"] * (canvasHeight / CANVAS_HEIGHT),
                    roomRef.current["game"]["powerUpW"] * (canvasHeight / CANVAS_HEIGHT),
                    roomRef.current["game"]["powerUpW"] * (canvasHeight / CANVAS_HEIGHT));
            }
        }
    });

    useEffect(() => {
        function updateCanvasDimensions() {
            const canvas = canvasRef.current;
            const parent = canvas.parentElement;
            if (!parent) return;

            const maxWidth = 1080;
            const maxHeight = 600;
            const parentWidth = parent.clientWidth;
            const parentHeight = parent.clientHeight;
            const aspectRatio = canvas.width / canvas.height;

            let canvasWidth = parentWidth;
            let canvasHeight = parentWidth / aspectRatio;
            if (canvasHeight > parentHeight) {
                canvasHeight = parentHeight;
                canvasWidth = parentHeight * aspectRatio;
            }

            // make sure the canvas dimensions don't exceed the maximum dimensions
            canvasWidth = Math.min(canvasWidth, maxWidth);
            canvasHeight = Math.min(canvasHeight, maxHeight);

            setCanvasWidth(canvasWidth);
            setCanvasHeight(canvasHeight);

            // redraw the canvas with the new dimensions
            canvas.width = canvasWidth;
            canvas.height = canvasHeight;
        }

        window.addEventListener('resize', updateCanvasDimensions);
        updateCanvasDimensions();

        return () => window.removeEventListener('resize', updateCanvasDimensions);
    }, []);

    useEffect(() => {
        const canvas = canvasRef.current;
        let animFrame: number;
        if (!canvas || !roomRef.current)
            return;

        socket.off("setRoom").on("setRoom", (data) => {
            roomRef.current = data.room;
            setBallLaunched(data.room["game"]["ballLaunched"]);
            setScoreP1(roomRef.current["game"]["score1"]);
            setScoreP2(roomRef.current["game"]["score2"]);
            if (roomRef.current["game"]["winnerId"]) {
                if (roomRef.current["game"]["winnerId"] === roomRef.current["players"][0]["intra_id"]) {
                    setWinner(roomRef.current["players"][0]["username"]);
                } else {
                    setWinner(roomRef.current["players"][1]["username"]);
                }
            }
        });

        socket.off("stopGame").on("stopGame", () => {
            setGameRunning(false);
        });

        socket.off("unexpectedDisconnection").on("unexpectedDisconnection", (data) => {
            setGameRunning(false);
            setWinner(data.winner);
            setLeaver(data.leaver);
        });

        if (!isSpec)
            window.addEventListener("keydown", onKeyDown);

        const gameLoop = () => {
            contextCanvas.current = canvas!.getContext("2d");
            let context = contextCanvas.current;
            context!.fillStyle = "black";
            if (user.intra_id === roomRef.current["players"][0].intra_id && gameRunning && (!winner.length || !leaver.length)) {
                socket.emit('gameloop', {roomId: roomRef.current["roomId"]});
            }
            drawGame(canvas, context);
            animFrame = window.requestAnimationFrame(gameLoop);
        }

        if (!ballLaunched && roomRef.current && !winner.length && !isSpec && user.status && user.status !== "playing") {
            socket.emit("initGame", {roomId: roomRef.current["roomId"]});
        }

        gameLoop();

        return () => {
            window.cancelAnimationFrame(animFrame);
            if (!isSpec) {
                window.removeEventListener("keydown", onKeyDown);
            }
        };
    });

    return (
        <>
            { !gameRunning && !winner.length ? <Loading /> : null }
            {
                gameRunning ?
                    <div className="board d-flex justify-content-center">
                        <div className="text-center">
                            <h1 id="score">Mode {roomRef.current["game"]["mode"]}</h1>
                            <h1 id="score">{roomRef.current["players"][0].username} {scoreP1} {scoreP2} {roomRef.current["players"][1].username}</h1>
                        </div>
                        <div className="canvas text-center">
                            <canvas
                                className="border"
                                width={canvasWidth}
                                height={canvasHeight}
                                ref={canvasRef}>
                                Sorry, your browser does not support canvas HTML...
                            </canvas>
                        </div>
                        {
                            openManual ?
                                <div className="text-center">
                                    <h2>Manual</h2>
                                    <hr/>
                                    <div className="text-center">
                                        <p><FontAwesomeIcon icon={faArrowUp}/> Press arrow up to go up</p>
                                        <p><FontAwesomeIcon icon={faArrowDown}/> Press arrow down to go down</p>
                                        <p><FontAwesomeIcon icon={faWindowMinimize}/> Press spacebar to launch the ball</p>
                                        <hr/>
                                        <p>Score {MAX_SCORE} points to win the game !</p>
                                    </div>
                                    <button onClick={() => setOpenManual(false)} className="btn btn-light"><FontAwesomeIcon
                                        icon={faBook}/> Close Manual
                                    </button>
                                </div>
                                :
                                <div className="text-center">
                                    <button onClick={() => setOpenManual(true)} className="btn btn-light"><FontAwesomeIcon
                                        icon={faBookOpen}/> Open Manual
                                    </button>
                                </div>
                        }
                        {
                            !isSpec ?
                                <div className="text-center">
                                    <button onClick={() => leave()} className="btn btn-danger">Leave</button>
                                </div>
                            :
                                null
                        }
                    </div>
                :
                    null
            }
            {
                winner.length ?
                    <div style={{ height: '100vh' }} className="d-flex align-items-center">
                        <div className="card border-light m-auto p-5">
                            <div className="container my-auto">
                                {
                                    leaver.length ?
                                        <h1 className="text-center">{leaver} left the game :(</h1>
                                     :
                                        <h1 className="text-center">{scoreP1} - {scoreP2}</h1>
                                }
                                <h1 className="text-center">{winner} won !</h1>
                                <Link to="*" className="btn btn-link nav-link text-center" onClick={ (event) => onBack2GameMenu(event) }>Back to Game Menu</Link>
                            </div>
                        </div>
                    </div>
                :
                    null
            }
        </>
    );
}

export default Pong;
