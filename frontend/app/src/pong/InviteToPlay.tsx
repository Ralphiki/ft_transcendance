import {FC, useContext} from "react";
import {useNavigate} from "react-router-dom";
import { toast } from "react-toastify";
import {GameSocketContext} from "../context/gameSocket";

const InviteToPlay: FC<{ disabled: boolean, userProps: any, targetProps: any }> = ({disabled, userProps, targetProps}) => {
    const navigate = useNavigate();
    const socket = useContext(GameSocketContext);

    const launchGame = async () => {
        if (userProps["status"] === "online" && targetProps["status"] === "online" &&
            targetProps["blacklist"].includes(userProps["intra_id"].toString()) === false) {
            socket.emit("startGameOnInvite", {user: userProps, target: targetProps});
            navigate("/play", {replace: true});
        }
        else {
            toast.info("One of you is either not online or has blocked the other one...");
        }
    }

    return (
        <>
            <button disabled={disabled} className="btn btn-sm top-0 end-0 btn-outline-dark mx-2 inviteToPlay" onClick={launchGame}>Invite to play</button>
        </>
    )
}

export default InviteToPlay;
