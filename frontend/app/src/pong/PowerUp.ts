class PowerUp {
    image: any

    constructor() {
        this.image = new Image()
        this.image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAP40lEQVRoQ61aCZQU1bn+qrt6X2aG\n" +
            "2RAYdF4EJUbzjGs0MYk+E7OhItvT+HKCOahExGgSkiNmNYbjSSSKIsYtDmDCIBIVEaPJE6MRxZgH\n" +
            "RuUpEcTIMszaMz3TW3Xl/+9SfbunGQaOdU6dW1VdVff/7vf9y73VFj6kzcUpgW7s+FwRzhwf8Ck/\n" +
            "0EhtwAIsauGD5VKbpcOdPvietGE/YKHrrQ+pe1A/R765cK0PkLjKh+IvLVhRMh5+eqUvmYA742IU\n" +
            "v/pFWEdPJCQW7D37EHhiI+yH2+Hr7TM6td4lgLMtpLYcuSU0Wkf68E5El9HDV2vjubWmXwTntsXw\n" +
            "hYKwLAt+H/FAu8/Y+brPKSJw7Xfh++0qYYArjaCrvktt9LUfiU2HDWQ7ItPJvDUBMfqKgTF1cF99\n" +
            "Hr5odLjRJCxmRIAhEAyMwfCObBbBSScD+zuE7QrQEA3BRy307jocQIcFZBui2wJwTwzQONrUC+/W\n" +
            "2Z8E2tvg9/uFsbr1KQCCATae7zVBKCv5d3vqLFgbn/XsLgpQvgeJnTmjBTMqIK+SI/vwRioEK2wb\n" +
            "IPwfPxF4cq00nnabDaXWb7AgRn8EENpQ+3NfhvXiZo8ZZocAHbBxw1gLP2ZsI26HBPI+JkT2oXOA\n" +
            "VO8LChaIeJZUgMS1Yxts2xZA/NSydPx+apWUtITMVlsjpFWxBRLjhNy0zBQYx0Z93MKuzEhIRgTC\n" +
            "TOTxRoYAEAgtJ9m6z66Hf9JHYBMgWzHCzu3zscRKPkEnAiBv2vhqIMTvu3bDP5l8Rm0KCDPjBjEx\n" +
            "bOGN3MHAjAhkE0JDYZKTZkL6BRnJzv3KJgEioIFoAMyOPmYAKmLpjg4GQhvoP/oEWHv3lbHiCJm5\n" +
            "uQgGQocN5E8Iv0NPHcsgOEJp3xBu++By+D99lpAVAxGGMwC/ZoSdXkcpydBIkjKNs/76Mnyf/VIZ\n" +
            "EGaGwRSA7Qn0T6kGpiojGxD+aRjuTeTcCoQMs8wI+4f75t8UiCABYR8hmCJiSSBCYoIZDrf0pAi3\n" +
            "pe5HZMV1YYUaPCD6KfZ2h2KZA+uGJPpvqwQzDMjmzZube848Zx9JikCYQBQYMhCvvyKA2DZLi5yb\n" +
            "gLDxfptGnyKWyBkEioOApc4ZyKFk5RkXbabhp/E3fEX6i4s8tbkDO5ONjY39ZUxWInt+0/MDmUtm\n" +
            "x+zOLk9SOvmxrOy6OgqTz5KDK1mR8XzMbAhWRDaXrAhgIncwuPIxGwlUscJP2EYGIcDUj0Fhx9YD\n" +
            "dXV1TQcF8vTTT8+Nx+L3OEvvQn712jLfkFmcfHfcePifedwDYitpCQCc/LS8RFvyGQZSafzBwDgn\n" +
            "nAb3nX8OcwWR+f97Btx778TQ0OBUYuUJz/fMuzc+9ZRbW1sLa+vrSF013wPCANg/uIr1J5OwX/yz\n" +
            "jFYEguUlwy/zpYFIEDJ6SaZkhpdh2eu8Si7h33LjJgOdnVWB2A8/AGvaVKTT6WJ9fT2bJjaP7/WP\n" +
            "P74wHIksjsXisFMpdF8w1StDvCTIYKjzwNaXBRABRgFhGTEQrqtYSrpgLC8cJStercUG0LnLDq5A\n" +
            "8XE20kgaqp7MQ1tfAo6fTHkzi8HBwW+PHTv212VA1j36qJNM1vjCkTCCVId2nXNeWaQS+UPJy97y\n" +
            "AgJhuo+q3IB2eCUj4dRe7lAM8YxEZH2VHA0ww+TlOEhHm8rKctO7Iu9vB5oakc/nMUisNDU3C1bE\n" +
            "PevXr59SdJw3o9EYQqGQmA11ffo8FW5LYVfMNVhmNy6EPWMagsSIAGNk9xIrcuR1Cc/96GOTFRkM\n" +
            "SqbmV63G0JyrZX2m4JhAYvt2ABRwHAI8NDTEgE4kVv4h7mlfvfqFSCRydjgcQTBIhhUddJ57gZKW\n" +
            "Ml77CIMhw0ObObPLyCUlxv4i6y5RkqhR15Vv2bnxW2Vp31ND9VYmo4CU65+NTbxHk8qxzaS8opRX\n" +
            "Ov3auPHjTxFAHl61qhiPx61QiOQSDMDXP4CeC6eL0ZehtyQrOW0FgotvRuB8Yo3BMAgFRhSQAowM\n" +
            "uZwYvRpLaMBgSs1PNCt5KuX7p832mOB+vNJGHSeoNPJR1c1Acrkc+vv7MWHCBMtqb2+vyedyvbGY\n" +
            "klUgCHf7dqTmX68mTnoCJebdMgSrPfzic/CTtLjy5cTIgDjLlxKjBDJsUqUAyetq4kXXOpNjBQUa\n" +
            "gJSX9AB9HLvrNgSv+LoIEOwnFL2YmXHWyodWznMt964oze7YP1ge2bZVyPxutVeSaCeXICQ7Yqdn\n" +
            "wn/aKJxYS8um6MXAZCVcyiOVs0Rz+svzl45jjgdoLq8N1kFan+s2SCpIPrFGMEJ+TUAGkR5M32S1\n" +
            "tbVtoZR5ajgcEkB4RFNz5wF793pTWZMFfSxyBoNJxBHZ8JiYnzATnFMYlCxTVN2lcolmQOYTmXsY\n" +
            "cOfxVLp3d3sOzkZXsuIBomjZ2PMBXAZCrFAIRqov9ZbV9lBbmniKstaDQWbEj96LZ3qjbi4uaCbM\n" +
            "jkSHpPXIsjsQOOljynhmRYPQVbFkyFyMKL7+BvounAEf5xE1MOXvLknKZGZs+oAQHMsrTUD6U/0Z\n" +
            "a+WKlcRSkbMSAsLR0+i/ksNfuYMPk5XSb0nH5EtUFUQ4CHx0SqmsV0VjqcSnrra/jfS8BXCpnvNk\n" +
            "qt6n1sDKJFaSmwTWsGYlwl/6gmCEfYSAuMyIKGGYKt4yty9F8f+2igdKnRh+YVwXOUM8JZ3S1DdP\n" +
            "vgInnQS79RgRjt33dov3oqvbk40p2TLfM6QlJSb70XKzqXBs2f02rR8VBZC+PvKtFW0riCGqLFlz\n" +
            "tPdeMbfsoXJAGlx1yqsBks+XDDEZ0Mcc3quBquy7BMZCy863YNUklbRS5UDSDz6E7PN/4Tmy2IeN\n" +
            "chn9w5nQQLg1R/Jgg6GNN1uds0zAsoqTaUDbFKAVzOaXNulc4lor21ZkSWvBIlHUO2++rPm9XS2Z\n" +
            "lcmnOht8pykzk51qLJg5Sc481eqMIV3TL0tgS/3Xtd0P/xmnMitZltabJK0pvd+8EkVKMJVATCgl\n" +
            "Q6v7hmZC4fZGT59rcPqdXhFKP5StlxmB5mBsaVtqX/xfZHy+N4mRld/vvWbBL4qpPsFENSC640rH\n" +
            "NjNuZeIyGTGBVQ6M+W497zFXMjVz3sTOcHrxXg79f3hkoXUPEg05DB3gF+pdLr/I6aWeFVRnpuQn\n" +
            "kg3JlGmcmdhMH+JjU476XN/DNZ65elMCUoqgeoDiiNWKQV4K26GX+jQQuWJRAmKC1Eaa92omww0J\n" +
            "NP5nCxpOmoja1kaEkhHKTVRQ8uyxSIMykEV6dyc6tuzE/lfeRaZrwHNiU37msZg2KFCVfqTucy5F\n" +
            "3hZA7oS9iAz7mSkrBiKXXyQ7uuV72JN4NcMhWhvOOhatUz+BGgIRiwYRCVOJz/N4W5YfnAgDXLrQ\n" +
            "zi3XXJx8HdqLNHxDPQPYtvzP2LWechdd05FJD5RmyItWCphmmnq47hvI3q5lz6zQa0oRixdjCmQw\n" +
            "r1HycU4d8wJsIWTjmEs/iaZJzahNRBCPhalOs3Hvqpcw/Ssfx6TWJrGm5QHh8icgwfBU5f//uR8r\n" +
            "1r6Kn32XFq7pvEAz0myugL2v7cIL3/s9ipm8iGK8Da+E5TX1m3sNCuLUA7IMwYsKKK7To8+G83Jy\n" +
            "htohatO099Nx/edPwPiPtWBMXQw1BCIRCwkW3trRga/NW4K5/zOTDDxXzElsYoVZkCW+rLXY6KsW\n" +
            "rsO9K9bgr0/egrNOaRWlRqFA84t8AYNDOby9cRteu/VJ4SOlkS8lTc0Oifb8eciI7xEeED5ZisBz\n" +
            "JKfPMAMMgKpJpOi4h9oekkjrrNPRQABqa2Koq40KIMl4GNFIUIzwkvvW47pvzMQvFp2HGLHkJ/+w\n" +
            "qBK26KMEIROj6GbzuPKaR7D0oTW49cbLsIhYcfMOcsTCYIbCDrXpwSx6uwfw2Oy74CeA/KS5/qzC\n" +
            "9lPzUZBrq5VA+ALVsO9m4bYOkPF9tHfTtS5qx884DUly3rpkVLDRRD7RMCZOoKIEJoLrf7IWM746\n" +
            "FW2r1mLDugWwCCAICHk6gTDq5kIO55/5Y8z5+kxs+ttfsPy31wqfQYH47xtEhnymh9q+1CA6OlJY\n" +
            "d+kywQx/DaA30m7x8fYFyJetAZcxotEtQWBTCsVzugkAry51x4OYfMmpJCOaC9Qn0DKuDkc11RAr\n" +
            "MXEtHApg2tx7UBcIY29XHzZvuol6pDEkSYF8QwAR33bZIfI4+bjrMamlGf76EH639gdKQOyhpIUs\n" +
            "aYHB7O/Dex904cHL7kaRgEXo1zg9n4D1xxtQ+IK29aCM6B9+hdBXulF4fC99ue1rjOO4S05Dgka5\n" +
            "taUe45prUUPssJOzf7APzJp3HwJ5C+/v78Y/tvycFqJpHBkEMyJYUcomPzh24rfwickTERobw4rV\n" +
            "31FAuGdVHDkEiBjZ885e3DxtCfJ7ekHL2kWqpy9YiOwzlSCqSqvypkUIzO9tit121NfOsGsSUYw/\n" +
            "agzqSVrxaIjCLTk6+QeH22t/1I79/+rD+x3deHvLLeLLLoIaBLFBzi5CVM5By4SrCUgLppx+NBb/\n" +
            "eo7BiIqbBQr25EtFyjM3nP2jfPCDnmtvRXF5NQCHZMR86M5bLq7PDTidCfKFOvKJMSSpOEUrlhQ7\n" +
            "eoh84ZENf8cDbS9QmC5i67M3IUS/l6RFANjZOYeQkUcfuwDjauNY9NPp+PL0MyVA9hOeE5Fzg5yf\n" +
            "gTipIfTsHUo2fmph2cr7ETGiH1r2k2lulFYhOV9wpGIHD9On0RDNKsN0je044fyf45c3XoQrZp0t\n" +
            "mPJ70uKoRTuv+9J9dy//I677YTsyB+6DxQFBqIoAcH3BbJD8OJf09qZRf/L3q/rxqH2k8sb7F8/e\n" +
            "Q0ntKM4LnK2ZkXiUgcmMHRTZ3C8YChO4IMlKJ0GRCDUQ9nldZLHvcADQbFCO4VCcp+TI+aSjs3/n\n" +
            "cf9183+MJKnDkhbf3H7H5Z8pOO5z8ls6lx8WgZDSYhAByhUCEI0w+0yA2JCJUCZGrrfkvF2tyquF\n" +
            "On43f/ngcqVAyzt5ApLJFtA/MISurvRpZ0xf8uqHCoRftvr2yznBR8VHHTaEx1MB4tGXWbzUBlWN\n" +
            "JYEwk7Js8b76quKCp9oOsVEgWXGpwgmR2Og99/K760YDYlRRy3zRY/fPSWQHsinxBUqs75q/ypEW\n" +
            "RtMeFKuPDIrPpRxl7UX3iaVS9eGHQZB/MBNZcvABAtE/kHGbGupjZ81cwtXRqLZROZL5pg0b5ocG\n" +
            "d3Ttpv6b5JqueoWeiBhv1CvzDJClKADo7yAsJy7tCYRDsuKWmaESeE9rKHHMqVf+hgvsUW+HDUS/\n" +
            "+dE7Z012cs7vaWntZPnJtmRkqXeDNTGRMadncoFNeIj40IMt/lBg9iXzHn531NYbNx4xkDKW7rgs\n" +
            "OeAMfd51nFlk1emkm0ayjb5u83/NZBcWTTbcouWSwVkyvYNWul6mJdXVwWTdMxde8cAh88ShwP0b\n" +
            "SK8AXurinYkAAAAASUVORK5CYII=";
    }
}

export {PowerUp};
