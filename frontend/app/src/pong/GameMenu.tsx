import React, {useCallback, useContext, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../providers/store";
import Pong from "./Pong";
import GameList from "./GameList";
import {GameSocketContext} from "../context/gameSocket";
import axios from "axios";
import {setCurrentUser} from "../providers/userProvider";
import jsCookie from "js-cookie"
import {backendURL} from "../api/routes";

const GameMenu: React.FC = () => {
    const socket = useContext(GameSocketContext);
    const [startGame, setStartGame] = useState<boolean>(false);
    const [room, setRoom] = useState({});
    const [mode, setMode] = useState('');
    const [isSpec, setIsSpec] = useState(false);
    const user = useSelector((state: RootState) => state.userProvider.user);
    const dispatch = useDispatch();

    function chooseMode(mode: string) {
        socket.emit("setMode", {roomId: room["roomId"], mode: mode});
    }

    const setStatus = useCallback((status, id) => {
        axios({
            url: backendURL + "/users/" + id,
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
            },
            data: { status: status }
        }).then(res => {
            dispatch(setCurrentUser(res.data));
        }).catch(err => console.error(err));
    }, [dispatch]);

    function onBack2GameMenu(event) {
        event.preventDefault();
        setStartGame(false);
        setStatus("online", user["intra_id"]);
    }

    function onJoinSpec(gameId: number) {
        if (user.status === "queueing") {
            onCancelFindGame();
        }
        setIsSpec(true);
        setStartGame(true);
        socket.emit("joinSpec", {user: user, gameId: gameId, fromProfile: false});
    }

    useEffect(() => {
        if (socket) {
            if (user.status === "playing" && !startGame) {
                socket.emit("joinRoom", user);
            }
        }
    });

    useEffect(() => {
        if (room.hasOwnProperty("game") && room["game"] !== null && room["game"]["isActive"] && room["game"]["mode"] !== null) {
            setMode(room["game"]["mode"]);
        }
        if (!startGame && room.hasOwnProperty("game") && user.status === "online") {
            setRoom({});
            setMode('');
        }
        if (socket) {
            socket.off('updateStatus').on('updateStatus', (data) => {
                setStatus(data.status, user["intra_id"]);
            });

            socket.off('setClientRoom').on('setClientRoom', (data) => {
                setRoom(data.room);
            });

            socket.off("startGame").on("startGame", (data) => {
                if (data.fromProfile) {
                    setIsSpec(true);
                }
                if (user.status !== "playing"
                    && user.status !== "watching") {
                    setStatus(data.status, data.intra_id);
                }
                setRoom(data.room);
                setStartGame(true);
            });

            socket.off('setMode').on('setMode', (data) => {
                setMode(data.mode);
                setRoom(data.room);
            });
        }
    }, [room, startGame, user, socket, setStatus, mode]);

    function onFindGame() {
        socket.emit("joinRoom", user);
        setStatus("queueing", user["intra_id"]);
    }

    function onCancelFindGame() {
        socket.emit("deleteRoom");
        setStatus("online", user["intra_id"]);
        setRoom({});
    }
    return (
        <div style={{ height: '90vh' }} className="d-flex align-items-center">
            <div className="container my-auto">
                {
                    startGame && room.hasOwnProperty("roomId") && mode.length ?
                        <Pong onBack2GameMenu={onBack2GameMenu} socketProps={socket} roomProps={room} isSpec={isSpec} />
                    :
                        null
                }
                {
                    !startGame &&
                    <div className="row">
                        <div className="col-12 text-center">
                            {
                                user.status === "queueing" ?
                                    <button className="btn col-6 btn-lg mt-5 m-auto btn-warning" onClick={onCancelFindGame}>Cancel</button>
                                :
                                    <button className="btn col-6 btn-lg mt-5 m-auto btn-success" onClick={onFindGame}>Find game</button>
                            }
                            <GameList onJoinSpec={onJoinSpec} />
                        </div>
                    </div>
                }
                {
                     startGame && room.hasOwnProperty("roomId") && !mode.length ?
                        <div className="card h-100 border-light mb-3 w-100">
                            <div className="text-center">
                                <h1>Choose your mode !</h1>
                                <button onClick={() => chooseMode('classic')} className="btn btn-dark m-1">Classic</button>
                                <button onClick={() => chooseMode('survival')} className="btn btn-dark m-1">Survival</button>
                            </div>
                        </div>
                     :
                         null
                }
            </div>

        </div>
    )
}

export default GameMenu;
