import {
    BrowserRouter as Router,
    Route,
    Routes
} from 'react-router-dom';
import {FC, useContext, useEffect} from "react";
import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'
import Navbar from "./Navbar";
import Home from './Home'
import Dashboard from './Dashboard'
import Profile from './Profile'
import EditProfile from "./EditProfile";
import NotFound from "./NotFound";
import ChannelChats from "./ChannelChats"
import GameMenu from "./pong/GameMenu";
import {useSelector} from "react-redux";
import {RootState} from "./providers/store";
import {GameSocketContext} from "./context/gameSocket";
import Login from "./Login";

const App: FC = () =>  {
    const jwt = useSelector((state: RootState) => state.userProvider.token);
    const user = useSelector((state: RootState) => state.userProvider.user);
    const gameSocket = useContext(GameSocketContext);

    useEffect(() => {
        if (user) {
            gameSocket.emit("onStartAppConnection", user);
        }
    }, [user, gameSocket]);

    return (
        <Router >
            <GameSocketContext.Provider value={gameSocket}>
                <Navbar />
                {
                   jwt.length ?
                        <Routes>
                            <Route path="/" element={<Home />} />
                            <Route path="/dashboard" element={<Dashboard />} />
                            <Route path="/users/:id" element={<Profile />} />
                            <Route path="/editprofile" element={<EditProfile />} />
                            <Route path="/channelchats" element={<ChannelChats />} />
                            <Route path="/play" element={<GameMenu />} />
                            <Route path="*" element={<NotFound/>}/>
                        </Routes>
                    :
                        <Login />
                }
            </GameSocketContext.Provider>
        </Router>
    );
}

export default App;
