import './style/Chat.css'
import {useContext, useEffect, useRef, useState} from 'react'
import { useSelector } from "react-redux";
import { RootState } from './providers/store';
import moment from 'moment'
import { GameSocketContext } from './context/gameSocket';

function Chat({ friend, roomid }) {
    const user = useSelector((state: RootState) => state.userProvider.user);
	const [msg,setMsg] = useState<Array<{}>>([])
	const [text,setText] = useState("")
	const chatboxRef = useRef<null | HTMLDivElement>(null);
	const socket = useContext(GameSocketContext)

	function sendMessage() {
		if (text.trim() === "")
			return ;
		const json = {
			message_data: text,
			user_id: user["intra_id"],
			room_id: roomid,
			sentAt: moment().format('lll')
		}
		socket.emit('new_mp', { json: json, roomid: roomid }, () => {
			setText("");
		})		
	}

	useEffect(() => {
		if (socket)
		{
			socket.emit('getMsgHistory', { roomid: roomid }, (history) => {
				setMsg(history);
			})

			socket.on('broadcast_mp', (message) => {
				setMsg(prevMsg => [...prevMsg, message]);
			})
		}

		if (chatboxRef.current) {
			chatboxRef.current.scrollTo(0, 0)
		}
	}, [roomid, user, socket])

    return (
        <>
			<div className="content">
				<div className="container p-0">
					<div className="card m-5">
						<div className="col-12 col-lg-7 col-xl-9 m-auto">
							<div className="py-2 px-4 border-bottom d-none d-lg-block">
								<div className="d-flex align-items-center py-1">
									Your Chat with { friend["username"] }
								</div>
							</div>

							<div className="position-relative">
								<div className="chat-messages p-4">
									{
										msg?.map((m,idx) => {
											if ( user["intra_id"] === m["user_id"] )
												return (
													<div key={idx} className="chat-message-right pb-4" ref={ idx === msg.length - 1 ? chatboxRef : null } >
														<div>
															<img src={ user["avatar"] } className="rounded-circle mr-1" width="40" height="40" alt=""/>
															<div className="text-muted small text-nowrap mt-2">{m["sentAt"]}</div>
														</div>
														<div className="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
															<div className="font-weight-bold mb-1">You</div>
															{ m["message"] }
														</div>
													</div>
												)
											return (
											<div key={idx} className="chat-message-left pb-4">
												<div>
													<img src={ friend["avatar"] } className="rounded-circle mr-1" width="40" height="40" alt=""/>
													<div className="text-muted small text-nowrap mt-2">{ m["sentAt"] }</div>
												</div>
												<div className="flex-shrink-1 bg-light rounded py-2 px-3 ml-3">
													<div className="font-weight-bold mb-1">{ friend["username"] }</div>
													{ m["message"] }
												</div>
											</div>
											)
										})
									}
								</div>
							</div>

							<div className="flex-grow-0 py-3 px-4 border-top circle-form">
								<div className="input-group">
									<input value={text} type="text" className="form-control" onChange={ (e)=> { setText(e.target.value); } } placeholder="Type your message"/>
									<button onClick={sendMessage} className="btn btn-primary">Send</button>
								</div>
							</div>
					 	</div>
					</div>
				</div>
			</div> 
        </>
        )
}

export default Chat;
