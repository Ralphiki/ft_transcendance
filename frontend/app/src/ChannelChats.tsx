import './style/Chat.css'
import { useEffect, useState, useContext, Fragment } from 'react'
import { useSelector } from "react-redux";
import { RootState } from './providers/store';
import { Link } from 'react-router-dom';
import {backendURL} from "./api/routes";
import axios from 'axios';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {toast, ToastContainer} from 'react-toastify';
import moment from "moment/moment";
import { GameSocketContext } from './context/gameSocket';
import InviteToPlay from './pong/InviteToPlay';

function ChannelChats() {
	const user = useSelector((state: RootState) => state.userProvider.user);
	const [msg,setMsg] = useState([])
	const [text,setText] = useState("")
	const [channels,setChannels] = useState([])
	const [currentChannel,setCurrentChannel] = useState({})
	const [mute, setMute] = useState<boolean>(false)
	const [members,setMembers] = useState<Array<Object>>([])
	const [newMember, setNewMember] = useState("")
	const [show,setShow] = useState(false)
	const [password,setPassword] = useState("")
	const [insertedPassword,setInsertedPassword] = useState("")
	const [chatters, setChatters] = useState([])
    const [key] = useState("")
	const socket = useContext(GameSocketContext)

	function sendMessage() {
		if (text.trim() === "")
			return ;
		const json = {
			message_data: text,
			user_id: user["intra_id"],
			user_name: user["username"],
			user_avatar: user["avatar"],
			room_id: currentChannel["room_uid"],
			sentAt: moment().format('lll')
		}
		socket.emit('new_mp', { json: json, roomid: currentChannel["room_uid"] }, () => {
			setText("");
		})
	}

	function onKickMembers(intra_id) {
		socket.emit('kick_member', { intra_id: intra_id, roomid: currentChannel["room_uid"] }, () => { })
		toast.success("You're kicking ass today!");
	}

	function onCheckPassword() {
		socket.emit('decrypt', {  intra_id: user["intra_id"], password: insertedPassword, roomid: currentChannel["room_uid"] }, () => {});
	}

	async function updateChannels() {
		axios({
            url: backendURL + "/chat/",
            method: "GET",
        })
        .then(
            res => {
				const filtered = res.data.filter( u => u.typechat === "public")
				setChannels(filtered);
        }).catch(e => {
            toast.error(e);
        })
	}

	useEffect(() => {
		if (socket) {
			socket.emit('join_room', { name: currentChannel["room_uid"], intra_id: user["intra_id"] } )
			
			socket.emit('getMsgHistory', { roomid: currentChannel["room_uid"] }, (history) => {
					setMsg(history);
			});

			socket.off('updateMembers').on('updateMembers', (data) => {
				axios({
					url: backendURL + "/chat/findmembers/" + data.id,
					method: "GET",
				}).then( res => {	setMembers(res.data.filter( u => u["intra_id"] !== user["intra_id"]));	})
			})

			socket.off('broadcast_mp').on('broadcast_mp', (message) => {
				setMsg(prevMsg => [...prevMsg, message]);
			})

			socket.off("made_admin").on("made_admin", (data) => {
				toast.success("UPGRADED to admin !");
				axios({
					url: backendURL + "/chat/findmembers/" + data.id,
					method: "GET",
				}).then( res => {	setMembers(res.data.filter( u => u["intra_id"] !== user["intra_id"]));	})
			});

			socket.off("been_unbanned").on("been_unbanned", (data) => {
				toast.error("You have been UNBANNED !")
				updateChannels();
			});

			socket.off("been_banned").on("been_banned", (data) => {
				setShow(false);
				toast.error("You have been BANNED !")
				socket.emit("leave_room", {roomid: data.roomid});
				updateChannels();
			});

			socket.off('been_added').on('been_added', (data) => {
				updateChannels();
			})

			socket.off('been_muted').on('been_muted', () => {
				setMute(true);
				toast.info("You have been muted. Behave!")
			})

			socket.off('been_unmuted').on('been_unmuted', () => {
				setMute(false);
				toast.info("Freedom of speech!")
			})

			socket.off('been_kicked').on('been_kicked', () => {
				setShow(false);
				toast.info("You have been kicked out of the room. Behave!")
			})

			socket.off('been_decrypted').on('been_decrypted', (psw) => {
				if (psw){
					toast.success("Password ok!")
					setPassword("");
				}
				else {
					toast.error("Password incorrect... Retry")
				}			
			})

		}

	}, [currentChannel, user, channels, socket])

    useEffect(() => {
        axios.get(backendURL + '/users')
            .then( users => {
                setChatters(users.data.filter((u) => u.username.startsWith(newMember)))
        })
    }, [newMember]);

	function joinChannel (id) {
		const intra_id = user["intra_id"].toString();
		const action = "addmembers";
		axios({
			url: backendURL + "/chat/update/" + id,
			method: "PUT",
			data: { id, intra_id, action }
		}).then( res => {
			if (res.data.members.length === members.length - 1)
				return ;
			setCurrentChannel(res.data);
			axios({
				url: backendURL + "/chat/findmembers/" + id,
				method: "GET",
			}).then( res => {
				setMembers(res.data.filter( u => u["intra_id"] !== user["intra_id"]));
				updateChannels();
				socket.emit("updateMembers", {id: id, room_uid: res.data["room_uid"]});
			})
		})
	}

	function showChannel (id) {
		axios({
            url: backendURL + "/chat/find/" + id,
            method: "GET",
        })
        .then(res => {
			if (res.data["banned"].includes(user["intra_id"].toString())) {
				toast.warning("You have been temporarily banned from this channel");
				return ;
			}
			setCurrentChannel(res.data);
			setPassword(res.data["password"]);
			setShow(true);
			setMute(currentChannel["muted"]?.includes(user.intra_id));
			axios({
				url: backendURL + "/chat/findmembers/" + id,
				method: "GET",
			}).then(res => {
				setMembers(res.data.filter( u => u["intra_id"] !== user["intra_id"]));
			}).catch(e => toast.error(e) )
		})

	}

	function onLeave() {
		const id = currentChannel["id"];
		const intra_id = user["intra_id"].toString();
		const action = "deletemembers";
		axios({
			url: backendURL + "/chat/update/" + id,
			method: "PUT",
			data: { id, intra_id, action }
		}).then( res => {
			setShow(false);
			updateChannels();
			socket.emit("updateMembers", {id: id, room_uid: res.data["room_uid"]});
		}).catch(e => {	toast.error(e) });
	}

	function onAddMembers() {
		if (!newMember.length) {
			return ;
		}
		axios({
            url: backendURL + "/users/username/" + newMember, // show toast err if username not found
            method: "GET",
        }).then(
            res => {
				if (!res.data) {
					toast.error("Username not found"); return ;
				} else if (user["blacklist"].includes(res.data.intra_id.toString())) {
					toast.error("You blocked this user... ");
					return ;
				} else if (res.data["blacklist"].includes(user.intra_id.toString())) {
					toast.error("You have been blocked by this user... ");
					return;
				}
				const id = currentChannel["id"];
				const intra_id = res.data["intra_id"].toString();
				const action = "addmembers";
				axios({
					url: backendURL + "/chat/update/" + id,
					method: "PUT",
					data: { id, intra_id, action }
				}).then( res => {
					if (res.data.members.length === members.length - 1)
						return ;
					setCurrentChannel(res.data);
					socket.emit("updateMembers", {id: id, room_uid: currentChannel["room_uid"]});
					socket.emit("addMember", {intra_id: intra_id, room_uid: currentChannel["room_uid"]});
			})
		}).catch(e => {	toast.error(e) });
	}

	function onMakeAdmin(intra_id) {
		const id = currentChannel["id"];
		const action = "addadmin";
		axios({
			url: backendURL + "/chat/update/" + id,
			method: "PUT",
			data: { id, intra_id, action }
		}).then( res => {
			setCurrentChannel(res.data);
			socket.emit("add_admin", {  id: currentChannel["id"], intra_id: intra_id, roomid: currentChannel["room_uid"] });
			toast.success("Member UPGRADED to admin!");
		}).catch(e => {	toast.error(e) });
	}
	
	function onUnbanMembers(intra_id) {
		const id = currentChannel["id"];
		const action = "deletebanned";
		axios({
			url: backendURL + "/chat/update/" + id,
			method: "PUT",
			data: { id, intra_id, action }
		}).then( res => {
			socket.emit("unban_member", {  id: id, intra_id: intra_id, roomid: currentChannel["room_uid"] });
			toast.success("Member UNBANNED!");
			setCurrentChannel(res.data)
		}).catch(e => {	toast.error(e) });
	}

	function onBanMembers(intra_id) {
		const id = currentChannel["id"];
		const action = "addbanned";
		axios({
			url: backendURL + "/chat/update/" + id,
			method: "PUT",
			data: { id, intra_id, action }
		}).then( res => {
			socket.emit("ban_member", {  id: id, intra_id: intra_id, roomid: currentChannel["room_uid"] });
			toast.success("Member BANNED! 😈 ");
			setCurrentChannel(res.data);
		}).catch(e => {	toast.error(e) });
	}

	function onUnMuteMembers(intra_id) {
		const id = currentChannel["id"];
		const action = "unmute";
		axios({
			url: backendURL + "/chat/update/" + id,
			method: "PUT",
			data: { id, intra_id, action }
		}).then( res => {
			socket.emit('unmute_member', { intra_id: intra_id, roomid: currentChannel["room_uid"] })
			toast.success("Member UNmuted!");
			setCurrentChannel(res.data);
			socket.emit("updateMembers", {id: id, room_uid: res.data["room_uid"]});
		}).catch(e => {	toast.error(e) });
	}

	function onMuteMembers(intra_id) {
		const id = currentChannel["id"];
		const action = "mute";
		axios({
			url: backendURL + "/chat/update/" + id,
			method: "PUT",
			data: { id, intra_id, action }
		}).then( res => {
			socket.emit('mute_member', { intra_id: intra_id, roomid: currentChannel["room_uid"] })
			toast.success("Silent as a mouse");
			setCurrentChannel(res.data);
			socket.emit("updateMembers", {id: id, room_uid: res.data["room_uid"]});
		}).catch(e => {	toast.error(e) });
	}

    useEffect( () => {
		axios({
            url: backendURL + "/chat/",
            method: "GET",
        })
        .then(
            res => {
				const filtered = res.data.filter( u => u.typechat === "public")
				setChannels(filtered);
        }).catch(e => {
            toast.error(e);
        })
	}, [user]);

	useEffect( () => {
		setMembers(members);
	}, [members]);

	useEffect(() => {
        socket.off("updateBlockUser").on("updateBlockUser", (data) => {
            if (!data.isBlocked) {
                toast.warning("You have been blocked by " + data.user.username);
            } else {
                toast.success("You have been unblocked by " + data.user.username);
            }
        });
    }, [socket]);

	return (
		<>
			<div className="container-fluid p-5 row">
				<div className="col-sx-12 col-3">
					<div className="card m-4">
						<div className="card-header">
							<h5>Your Channels</h5>
						</div>
						<div className="card-body">
							{
								channels && channels.length ?
									channels.map((u,idx) =>
										<div key={ idx } className="card-body">
											{
												user && u["banned"].includes(user["intra_id"].toString()) ?
													<div  style={{ color: 'black' }} className="card-title"><button className="btn btn-outline-danger" onClick={ () => showChannel(u["id"]) } disabled={true}>Banned from { u["name"] }</button></div>
												:
													null
											}
											{
												user && u["members"].includes(user["intra_id"].toString()) && u["banned"].includes(user["intra_id"].toString()) === false  ?
													<div  style={{ color: 'black' }} className="card-title"><button className="btn btn-outline-primary" onClick={ () => showChannel(u["id"]) }>{ u["name"] }</button></div>
												:
													null
											}
											{
												user && u["members"].includes(user["intra_id"].toString()) === false && u["banned"].includes(user["intra_id"].toString()) === false ?
													<div  style={{ color: 'black' }} className="card-title"><button className="btn btn-outline-dark" onClick={ () => joinChannel(u["id"]) }>Join { u["name"] }</button></div>
												:
													null
											}
										</div>)
								:
									<h6>No channels yet</h6>
							}
						</div>
					</div>
				</div>

				<div className="col-sx-12 col-9 mr-5">
					<div className="card m-4">
						<div className="row g-0">
							{
								show ?
									<Fragment>
										<div className="col-3 col-lg-5 col-xl-3 border-right">
											{
												<Dialog open={password?.length > 0 && password !== "null"} onClose={onCheckPassword}>
													<DialogTitle>New Channel Chat</DialogTitle>
													<DialogContent>
														<DialogContentText>
															This channel is protected by password\nPlease enter a password to chat
														</DialogContentText>
														<TextField
															autoFocus
															margin="dense"
															id="psw"
															label="Password"
															type="password"
															fullWidth
															variant="standard"
															onChange={ (e) => setInsertedPassword(e.target.value) }
														/>
													</DialogContent>
													<DialogActions>
														<button onClick={() => setShow(false)} className="btn btn-sm float-end btn-danger mt-2">Cancel</button>
														<button onClick={onCheckPassword} className="btn btn-sm float-end btn-primary mt-2">Confirm</button>
													</DialogActions>
												</Dialog>
											}

											{
												currentChannel["owner"] === user["intra_id"].toString() || currentChannel["admin"]?.includes(user["intra_id"].toString()) ?
													<div className="px-4 d-none d-md-block">
														<div className="d-flex align-items-center my-3">

															<form className="d-flex">
																<div className="dropdown">
																	<input onInput={ (e) => { setNewMember(e.currentTarget.value) } } className="form-control me-2 align-self-center" type="search" placeholder="Search for users" aria-label="Search"/>
																	{
																		key && key.length &&

																		<div className="dropdown-menu w-100 show">
																			{
																				chatters.length ?
																					chatters.map((u,idx) => <li key={idx} className="dropdown-item">{ u["username"] }</li>)
																					:
																					<li key="0" className="dropdown-item disabled">No match</li>
																			}
																		</div>

																	}
																</div>
															</form>
															<button onClick={onAddMembers} className="btn btn-primary m-1">Add</button>
														</div>
													</div>
													:
													<div className="py-2 px-4 border-bottom d-none d-lg-block">
														<div className="d-flex align-items-center py-1">
															<h1 className="flex-grow-1 pr-3">
																<strong className="text-secondary">Members</strong>
															</h1>
														</div>
													</div>
											}

											{
												members.length ?
													members.map( (m, idx) =>
														<div key={ idx } className="d-flex align-items-start mx-3 px-3 pb-3">
															<img src={ m["avatar"] } className="mr-1 mt-1" width="40" height="40" alt=""/>
															<div className="flex-grow-1 ml-5">
																<Link to={`/users/${m["intra_id"]}`} className="list-group-item list-group-item-action m-2">{ m["username"] }</Link>
																{
																		currentChannel["owner"] === user["intra_id"].toString() ?
																			<span className="float-end mr-1"><button onClick={() => onMakeAdmin(m["intra_id"].toString())} className="btn m-2 btn-sx btn-dark">+Admin</button></span>
																			:
																			null
																}
																{
																	user["status"] === "online"  && m["status"] === "online" && 
																	user["blacklist"].includes(m["intra_id"].toString()) === false ?
																		<InviteToPlay disabled={false} userProps={user} targetProps={m} />
																	:
																		null
																}
																{
																	currentChannel["owner"] === user["intra_id"].toString() || (
																		currentChannel["admin"].includes(user["intra_id"].toString())
																		&& m["intra_id"].toString() !== currentChannel["owner"]) ?
																		<span className="float-end">
																			{
																				currentChannel["banned"].includes(m["intra_id"].toString()) ?
																					<button
																					onClick={() => onUnbanMembers(m["intra_id"].toString())}
																					className="btn m-1 btn-sx btn-outline-danger">Unban</button>
																				:
																					<button
																					onClick={() => onBanMembers(m["intra_id"].toString())}
																					className="btn m-1 btn-sx btn-danger">Ban</button>
																			}
																			<button
																				onClick={() => onKickMembers(m["intra_id"].toString())}
																				className="btn m-1 btn-sx btn-warning">Kick</button>
																			{
																				currentChannel["muted"].includes(m["intra_id"].toString()) ?
																					<button
																						onClick={() => onUnMuteMembers(m["intra_id"].toString())}
																						className="btn btn-sx btn-outline-primary">Unmute</button>
																					:
																					<button
																						onClick={() => onMuteMembers(m["intra_id"].toString())}
																						className="btn btn-sx btn-primary">Mute</button>
																			}
																</span>
																		:
																		null
																}

															</div>
														</div>
													)
												:
													<div className="flex-grow-1 m-3">Only you in this channel. Add some members!</div>
											}
										</div>

										<div className="col-6 col-lg-7 col-xl-9">
											<div className="py-2 px-4 border-bottom d-none d-lg-block">
												<div className="d-flex align-items-center py-1">
													<h1 className="flex-grow-1 pl-3"><button onClick={onLeave} className="float-end m-1 btn btn-danger">Leave</button>
														<strong className="float-end text-primary mr-2">{ currentChannel["name"] }</strong>
													</h1>
												</div>
											</div>


											<div className="chat-messages p-4">
												{
													msg?.map((m,idx) => {
														return <div key={idx} className={`chat-message-${user["intra_id"] === m["user_id"] ? "right" : "left" } pb-4`} >
															{
																user["blacklist"].includes(m["user_id"].toString()) === false ?
																	<>
																		<div>
																			<img src={ m["avatar"] } className="rounded-circle mr-1" width="40" height="40" alt=""/>
																			<div className="text-muted small text-nowrap mt-2">{m["sentAt"]}</div>
																		</div>
																		<div className="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
																			<div className="font-weight-bold mb-1">{ user["intra_id"] === m["user_id"] ? "You" : m["user_name"] }</div>
																			{ m["message"] }
																		</div>
																	</>
																:
																	null
															}
														</div>
													})
												}
											</div>
										</div>

										<div className="flex-grow-0 py-3 px-4 border-top circle-form">
											<div className="input-group">
												<input value={text} type="text" className="form-control" onChange={ (e)=> { setText(e.target.value); } } placeholder="Type your message"/>
												<button placeholder={ mute ? "You have been temporarily muted" : "" } onClick={sendMessage} className="btn btn-primary" disabled={mute}>Send</button>
											</div>
										</div>
									</Fragment>
								:
									<div className="col-8 col-lg-5 col-xl-3 border-right">
										<div className="px-4 d-none d-md-block">
											<div className="d-flex align-items-center my-3">
												<div>No channel selected</div>
											</div>
										</div>
									</div>
							}

						</div>
					</div>
				</div>
			</div>
			<ToastContainer autoClose={1000}/>
		</>
	)
}

export default ChannelChats;
