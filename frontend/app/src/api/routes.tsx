export const backendURL = `https://localhost:3001`;
export const frontendURL = `https://localhost:3000`;
export const gameSocketURL = `wss://localhost:3001`;

export const routes = 
{
    login: `${backendURL}/login`,
    loginByUsername: `${backendURL}/loginbyusername`,
    logout: `${backendURL}/logout`,
    gameMenu: `${frontendURL}/play`,
};
