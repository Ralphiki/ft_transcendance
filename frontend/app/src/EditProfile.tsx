import './style/Profile.css'
import {useState, FC, useEffect} from 'react';
import { useSelector, useDispatch } from "react-redux";
import {backendURL} from "./api/routes";
import jsCookie from "js-cookie"
import axios from 'axios';
import { useNavigate } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify';
import { setCurrentUser } from './providers/userProvider';
import { RootState } from './providers/store';
import Loading from "./LoadingPage";

const EditProfile: FC = () =>  {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const user = useSelector((state: RootState) => state.userProvider.user);
    let [username, setUsername] = useState(user["username"]);
    let [title, setTitle] = useState(user["title"]);
    let [mail, setMail] = useState(user["mail"]);
    let [image, setImage] = useState<File | null>();

    const onHandleUpdate = async(event) => {
        event.preventDefault();
        const first_login = false
        let data = { }
        if (mail === "")
            data = { username, first_login, title }
        else if (username === "")
            data = { username, first_login, mail, title }
        else
            data = { username, first_login, mail, title }
        axios({
            url: backendURL + "/users/profile/" + user["intra_id"],
            method: "PATCH",
            headers: {
                Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
            },
            data: data
        }).then(res => {
            if (!image){
                toast.success("Success!", { position: toast.POSITION.TOP_RIGHT });
                dispatch(setCurrentUser(res.data))
                navigate("/"); return; }
            }).catch(err => {
                toast.error(err.response.data.message[0], {position: toast.POSITION.TOP_RIGHT})
            })
        if (image) {
            const formData = new FormData();
            formData.append("image", image, image.name);
            axios({
                url: backendURL + "/users/avatar/" + user["intra_id"],
                method: "POST",
                headers: {
                    Authorization: `Bearer ${jsCookie.get('jwt_token')}`,
                },
                data: formData
            }).then(res => {
                toast.success("Success!", { position: toast.POSITION.TOP_RIGHT });
                dispatch(setCurrentUser(res.data))
                navigate("/")
            }).catch(err => {
                    toast.error(err.response.data.message, {position: toast.POSITION.TOP_RIGHT})
            })
        }
    }

    useEffect(() => {
        dispatch(setCurrentUser(user));
    }, [dispatch, user]);

    if (!user)
        return ( <Loading /> );
    return (
        <>
            <div className="container">
                {
                    user["first_login"] ?
                        <div className="row profile-header mt-5">
                            <div className="profile-header-content row align-items-start">
                                <h1 className="m-10 text-center">Welcome to the platform!</h1>
                                <h3 className="m-10 text-center">Tell us something about yourself</h3>
                            </div>
                        </div>
                    :
                    null
                }
                <div className="row">
                    <div className="col-md-12">
                        <div className="profile">
                            <div className="profile-header">
                                <div className="profile-header-content row align-items-start">
                                    <h4 className="m-10">Profile Information</h4>
                                    
                                    <div className='col'> 
                                        <div className="profile-header-img">
                                            <img style={{ objectFit: "cover" }} className="img-fluid w-100 h-100" src={ user["avatar"] } alt="avatar"/>
                                        </div>
                                        <div className="profile-header-info">
                                            <form className="d-grid">
                                            <div className="input-group m-2">
                                                <span className="input-group-text">Username</span>
                                                <input className="m-t-10 m-b-5 form-control" placeholder={ user["username"] } onChange={ (e)=> 
                                                    {
                                                        if (e.target.value.length === 0)
                                                            setUsername(user["username"])
                                                        else
                                                            setUsername(e.target.value)
                                                    }}/>
                                            </div>
                                            <div className="input-group m-2">
                                                <span className="input-group-text">Title</span>
                                                <input maxLength={32} className="m-t-10 m-b-5 form-control" placeholder={ user["title"] } onChange={ (e)=> setTitle(e.target.value) }/>
                                            </div>
                                            <div className="input-group m-2">
                                                <span className="input-group-text">Email</span>
                                                <input className="m-t-10 m-b-5 form-control" placeholder={ user["mail"] } onChange={ (e)=> {
                                                    if (e.target.value.length === 0)
                                                        setMail(user["mail"])
                                                    else
                                                        setMail(e.target.value)} }/>  
                                            </div>
                                            <div className="input-group m-2">
                                                <span className="input-group-text">Avatar</span>
                                                <input className="m-t-10 m-b-5 form-control"
                                                    type="file"
                                                    onChange={ (event) => {
                                                        if (event.target.files && event.target.files[0]) {
                                                            setImage(event.target.files[0]);
                                                        }
                                                    }
                                                }/>  
                                            </div>                                              
                                                <button type="submit" onClick={ (event) => onHandleUpdate(event) } className="btn btn-sm btn-warning mt-3">Update Profile</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer autoClose={1000}/>
        </>
    );
                                        
}

export default EditProfile;
