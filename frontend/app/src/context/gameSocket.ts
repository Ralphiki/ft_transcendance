import {io} from "socket.io-client";
import {createContext} from "react";
import {gameSocketURL} from "../api/routes";

export const gameSocket = io(gameSocketURL, { transports: ['websocket', 'polling']});
export const GameSocketContext = createContext(gameSocket);

