import {io} from "socket.io-client";
import {createContext} from "react";
import {chatSocketURL} from "../api/routes";

export const chatSocket = io(chatSocketURL, { transports: ['websocket', 'polling']});
export const ChatSocketContext = createContext(chatSocket);
