# Ft_transcendance

## About

This is the last project of the main cursus of 42.
The goal is to play pong with other users through a web app.
It has been tested and validated on Firefox, Chrome, Safari and Brave.

![](frontend/app/src/assets/game.gif)

***

## Technologies used

It is fully written in Typescript.

It uses the following technologies :
* PostgreSQL as Database system
* Prisma as ORM
* NestJS as backend framework
* React as frontend framework
* Canvas HTML to handle the pong
* Docker to containerize the application 

And here are some of the most important JS librairies we used :
* passport & jwt for session management 
* bcrypt for password encryption
* axios for API requests
* socket-io for websockets management
* Redux for user session persistence
* Bootstrap for responsiveness
* toastify for notifications

***

## API

We used swagger to try out our API endpoints along with Postman.
Authentication, user information, games and chats are all using them.

![](frontend/app/src/assets/api.gif)

***

## Features

### Status

Every user can have the following status :
* offline
* online
* playing
* watching (with the spectator mode)
* queueing (with the matchmaking system)

Here is a view from the spectator mode :

![](frontend/app/src/assets/spectator.gif)

### Private Chat

User can chat with each other through user profile.
Here is what it looks like:

![](frontend/app/src/assets/inviteToPlay.gif)

### Public Chat

They can also join public channels, protected or not by password.
The owner of a channel get admin permissions, and can add and give other users admin permission as well.
But no one can manage the owner of a channel.

![](frontend/app/src/assets/channelsChatFeatures.gif)

***

## How to run it

As it is a secured website, you must add SSL certificate to launch it.
Create a folder named "cert/" at the root of the project.
Add the 2 following files inside:
* certificate.pem
* key.pem

Then add a .env file inside backend/app/
It should look like this :

```
CLIENT_ID=your_42_app_id
CLIENT_SECRET=your_42_app_secret
REDIRECT_URI=https://localhost:3000/home
DATABASE_URL=postgresql://notfij:gohad@database:5432/transcendance?schema=public&connection_limit=1000
SECRET_SESSION=choose_secret_session_pass_for_nest
JWT_SECRET=choose_secret_jwt_for_nest
```

Then ensure that you have docker-compose and postgresql installed and that your postgresql service is not running on port 5432 
Finally :

```
docker-compose up --build
```

If you get a Permission denied error, you may try to :
* change file permissions with chmod
* sudo su + clone repo inside /root/transcendance and launch it from there
