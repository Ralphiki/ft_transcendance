"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
function encrypt(encrypt) {
    const publicKey = Buffer.from(fs.readFileSync("/home/raphiki/Code/transcendance/backend/public.pem", { encoding: "utf-8" }));
    const encryptedData = crypto.publicEncrypt({
        key: publicKey,
        padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
        oaepHash: "sha256",
    }, Buffer.from(encrypt));
    return encryptedData.toString("base64");
}
function decrypte(password) {
    const privateKey = fs.readFileSync("/home/raphiki/Code/transcendance/backend/private.pem", { encoding: "utf-8" });
    const decryptedData = crypto.privateDecrypt({
        key: privateKey,
        padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
        oaepHash: "sha256",
    }, Buffer.from(password, "base64"));
    return decryptedData.toString("utf-8");
}
let ChatService = class ChatService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    decrypt(decrypt) {
        return {
            "password": decrypte(decrypt.password)
        };
    }
    findAll() {
        return `This action returns all chat`;
    }
    findOne(id) {
        return `This action returns a #${id} chat`;
    }
    update(id, updateChatDto) {
        return `This action updates a #${id} chat`;
    }
    remove(id) {
        return `This action removes a #${id} chat`;
    }
    message(data) {
        var json = {
            "message": data.message_data,
            "user_id": data.user_id
        };
        return this.prisma.room.update({
            where: {
                room_uid: data.room_id,
            },
            data: {
                message: {
                    push: json
                }
            }
        });
    }
    create_room(message) {
        const secret = "ok";
        const password = encrypt(message.password);
        return this.prisma.room.create({ data: {
                owner: message.owner,
                typechat: message.type,
                admin: message.admin,
                password: password
            }
        });
    }
};
ChatService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], ChatService);
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map