export declare class CreateData {
    message: string;
}
export declare class CreateChatDto {
    owner: string;
    type: string;
    admin: string[];
    banned: string[];
    message: string[];
    password: string;
}
