export declare class UpdateChatDto {
    user_id: string;
    message_data: string;
    room_id: string;
}
