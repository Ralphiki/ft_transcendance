import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateChatDto } from './dto/update-chat.dto';
import { PrismaService } from "../prisma/prisma.service";
import { Messagedto } from "./dto/insertchat.dto";
export declare class ChatService {
    private prisma;
    constructor(prisma: PrismaService);
    decrypt(decrypt: Messagedto): {
        password: any;
    };
    findAll(): string;
    findOne(id: number): string;
    update(id: number, updateChatDto: UpdateChatDto): string;
    remove(id: number): string;
    message(data: UpdateChatDto): any;
    create_room(message: CreateChatDto): any;
}
