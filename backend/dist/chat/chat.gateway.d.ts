import { ChatService } from './chat.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateChatDto } from './dto/update-chat.dto';
import { Server } from 'socket.io';
import { Messagedto } from "./dto/insertchat.dto";
export declare class ChatGateway {
    private readonly chatService;
    server: Server;
    constructor(chatService: ChatService);
    handleEvent(data: CreateChatDto): any;
    findOne(data: UpdateChatDto): any;
    create_pm(message: Messagedto): {
        password: any;
    };
    remove(id: number): string;
}
