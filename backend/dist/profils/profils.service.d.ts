import { UpdateProfilDto } from './dto/update-profil.dto';
import { PrismaService } from "../prisma/prisma.service";
export declare class ProfilsService {
    private prisma;
    constructor(prisma: PrismaService);
    create(data: any): import(".prisma/client").Prisma.Prisma__ProfilsClient<import(".prisma/client").Profils, never>;
    findAll(): import(".prisma/client").PrismaPromise<import(".prisma/client").Profils[]>;
    findOne(id: number): import(".prisma/client").Prisma.Prisma__ProfilsClient<import(".prisma/client").Profils, never>;
    update(id: number, updateProfilDto: UpdateProfilDto): string;
    remove(id: number): string;
}
