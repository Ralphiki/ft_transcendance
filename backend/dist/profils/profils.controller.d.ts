import { ProfilsService } from './profils.service';
import { UpdateProfilDto } from './dto/update-profil.dto';
export declare class ProfilsController {
    private readonly profilsService;
    constructor(profilsService: ProfilsService);
    findAll(): import(".prisma/client").PrismaPromise<import(".prisma/client").Profils[]>;
    findOne(id: string): import(".prisma/client").Prisma.Prisma__ProfilsClient<import(".prisma/client").Profils, never>;
    upload(file: any): import(".prisma/client").Prisma.Prisma__ProfilsClient<import(".prisma/client").Profils, never>;
    seeUploadedFile(image: any, res: any): any;
    update(id: string, updateProfilDto: UpdateProfilDto): string;
    remove(id: string): string;
}
