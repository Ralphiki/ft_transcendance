"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfilsController = void 0;
const common_1 = require("@nestjs/common");
const profils_service_1 = require("./profils.service");
const update_profil_dto_1 = require("./dto/update-profil.dto");
const platform_express_1 = require("@nestjs/platform-express");
const api_implicit_file_decorator_1 = require("@nestjs/swagger/dist/decorators/api-implicit-file.decorator");
const swagger_1 = require("@nestjs/swagger");
let ProfilsController = class ProfilsController {
    constructor(profilsService) {
        this.profilsService = profilsService;
    }
    findAll() {
        return this.profilsService.findAll();
    }
    findOne(id) {
        return this.profilsService.findOne(+id);
    }
    upload(file) {
        return this.profilsService.create(file.filename);
    }
    seeUploadedFile(image, res) {
        return res.sendFile(image, { root: './uploads' });
    }
    update(id, updateProfilDto) {
        return this.profilsService.update(+id, updateProfilDto);
    }
    remove(id) {
        return this.profilsService.remove(+id);
    }
};
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ProfilsController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ProfilsController.prototype, "findOne", null);
__decorate([
    (0, common_1.Post)("avatar"),
    (0, swagger_1.ApiConsumes)('multipart/form-data'),
    (0, api_implicit_file_decorator_1.ApiImplicitFile)({ name: 'image' }),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)("image", { dest: "./uploads", })),
    __param(0, (0, common_1.UploadedFile)(new common_1.ParseFilePipe({
        validators: [
            new common_1.FileTypeValidator({ fileType: '.(png|jpeg|jpg)' }),
            new common_1.MaxFileSizeValidator({ maxSize: 1024 * 1024 * 4 }),
        ],
    }))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ProfilsController.prototype, "upload", null);
__decorate([
    (0, common_1.Get)('avatar/:imgpath'),
    __param(0, (0, common_1.Param)('imgpath')),
    __param(1, (0, common_1.Res)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], ProfilsController.prototype, "seeUploadedFile", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_profil_dto_1.UpdateProfilDto]),
    __metadata("design:returntype", void 0)
], ProfilsController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ProfilsController.prototype, "remove", null);
ProfilsController = __decorate([
    (0, common_1.Controller)('profils'),
    __metadata("design:paramtypes", [profils_service_1.ProfilsService])
], ProfilsController);
exports.ProfilsController = ProfilsController;
//# sourceMappingURL=profils.controller.js.map