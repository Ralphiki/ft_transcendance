import { GamesService } from './games.service';
import { Game } from './entities/game.entity';
import { CreateGameDto } from "./dto/create-game.dto";
export declare class GamesController {
    private readonly gamesService;
    constructor(gamesService: GamesService);
    create(createGameDto: CreateGameDto): Promise<Game>;
    findAll(): Promise<Game[]>;
    findOne(id: string): Promise<Game>;
    remove(id: string): Promise<void>;
}
