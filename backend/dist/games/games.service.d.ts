import { PrismaService } from "../prisma/prisma.service";
import { CreateGameDto } from "./dto/create-game.dto";
export declare class GamesService {
    private prisma;
    constructor(prisma: PrismaService);
    create(createGameDto: CreateGameDto): import(".prisma/client").Prisma.Prisma__GameClient<import(".prisma/client").Game, never>;
    findAll(): import(".prisma/client").PrismaPromise<import(".prisma/client").Game[]>;
    findOne(id: string): Promise<import(".prisma/client").Game>;
    remove(id: string): import(".prisma/client").Prisma.Prisma__GameClient<import(".prisma/client").Game, never>;
}
