import { GamesService } from "./games.service";
import { Socket, Server } from "socket.io";
import { Player } from "./classes/Players";
export declare class GamesGateway {
    private readonly gamesService;
    constructor(gamesService: GamesService);
    private logger;
    private readonly queue;
    private readonly rooms;
    private readonly currentGames;
    private readonly players;
    server: Server;
    userConnect(client: Socket, user: Player): Promise<void>;
    onPing(data: string): string;
}
