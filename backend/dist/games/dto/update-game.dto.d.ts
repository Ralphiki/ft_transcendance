import { CreateGameDto } from "./create-game.dto";
declare const UpdateGameDto_base: import("@nestjs/common").Type<Partial<CreateGameDto>>;
export declare class UpdateGameDto extends UpdateGameDto_base {
    score1: number;
    score2: number;
    isActive: boolean;
    inQueue: boolean;
    player2: string;
    player2Id: number;
    winnerId: number;
}
export {};
