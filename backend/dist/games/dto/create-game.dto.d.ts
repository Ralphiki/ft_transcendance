export declare class CreateGameDto {
    id: number;
    score1: number;
    score2: number;
    player1Id: number;
    player2Id: number;
    winnerId: number;
}
