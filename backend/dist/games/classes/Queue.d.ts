import { Player } from "./Players";
export default class Queue {
    private capacity;
    private storage;
    constructor(capacity?: number);
    enqueue(item: Player): void;
    dequeue(): Player | undefined;
    size(): number;
    find(username: string): Player;
    remove(player: Player): void;
    isInQueue(player: Player): boolean;
    matchPlayers(): Player[];
}
