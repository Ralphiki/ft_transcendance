"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Players = exports.Player = void 0;
class Player {
    constructor(id, username, socketId) {
        this.id = id;
        this.username = username;
        this.socketId = socketId;
    }
    setUsername(username) {
        this.username = username;
    }
    setSocketId(socketId) {
        this.socketId = socketId;
    }
    setRoomId(roomId) {
        this.roomId = roomId;
    }
}
exports.Player = Player;
class Players {
    constructor(maxPlayer = Infinity) {
        this.maxPlayer = maxPlayer;
        this.players = new Array();
    }
    addPlayer(player) {
        if (this.maxPlayer !== this.players.length)
            this.players.push(player);
    }
    getPlayer(socketId) {
        let index = this.players.findIndex(user => user.socketId === socketId);
        if (index === -1)
            return undefined;
        return this.players[index];
    }
    getPlayerById(id) {
        let index = this.players.findIndex(user => user.id === id);
        if (index === -1)
            return undefined;
        return this.players[index];
    }
}
exports.Players = Players;
//# sourceMappingURL=Players.js.map