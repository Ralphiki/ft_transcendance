import { Player } from "./Players";
export declare class Room {
    roomId: string;
    players: Player[];
    constructor(roomId: string, players: Player[]);
    addPlayer(player: Player): void;
}
