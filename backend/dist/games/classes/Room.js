"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Room = void 0;
class Room {
    constructor(roomId, players) {
        this.roomId = roomId;
        this.players = players;
    }
    addPlayer(player) {
        this.players.push(player);
    }
}
exports.Room = Room;
//# sourceMappingURL=Room.js.map