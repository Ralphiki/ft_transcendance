export declare class Player {
    id: number;
    username: string;
    socketId?: string;
    roomId?: string;
    constructor(id: number, username: string, socketId?: string);
    setUsername(username: string): void;
    setSocketId(socketId: string): void;
    setRoomId(roomId: string | undefined): void;
}
export declare class Players {
    private maxPlayer;
    private players;
    constructor(maxPlayer?: number);
    addPlayer(player: Player): void;
    getPlayer(socketId: string): Player | undefined;
    getPlayerById(id: number): Player | undefined;
}
