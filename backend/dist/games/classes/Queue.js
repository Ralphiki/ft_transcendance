"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Queue {
    constructor(capacity = Infinity) {
        this.capacity = capacity;
        this.storage = [];
    }
    enqueue(item) {
        if (this.size() === this.capacity) {
            throw Error("Queue has reached max capacity, you cannot add more items");
        }
        this.storage.push(item);
    }
    dequeue() {
        return this.storage.shift();
    }
    size() {
        return this.storage.length;
    }
    find(username) {
        return this.storage.find(el => el.username === username);
    }
    remove(player) {
        let index = this.storage.findIndex(user => user.username === player.username);
        if (index !== -1)
            this.storage.splice(index, 1);
    }
    isInQueue(player) {
        return (this.find(player.username) !== undefined);
    }
    matchPlayers() {
        let players = Array();
        players.push(this.dequeue());
        players.push(this.dequeue());
        return players;
    }
}
exports.default = Queue;
//# sourceMappingURL=Queue.js.map