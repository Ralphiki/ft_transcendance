"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GamesGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const games_service_1 = require("./games.service");
const socket_io_1 = require("socket.io");
const common_1 = require("@nestjs/common");
const Queue_1 = require("./classes/Queue");
const Players_1 = require("./classes/Players");
let GamesGateway = class GamesGateway {
    constructor(gamesService) {
        this.gamesService = gamesService;
        this.logger = new common_1.Logger('gameGateway');
        this.queue = new Queue_1.default();
        this.rooms = new Map();
        this.currentGames = new Array();
        this.players = new Players_1.Players();
    }
    async userConnect(client, user) {
        console.log(client);
        console.log(user);
        if (this.players.getPlayer(client.id))
            return;
        this.logger.log(user.username + " joined the lobby");
        let newPlayer = this.players.getPlayerById(user.id);
        if (newPlayer) {
            newPlayer.setSocketId(client.id);
            newPlayer.setUsername(user.username);
        }
        else {
            newPlayer = new Players_1.Player(user.id, user.username, client.id);
        }
        this.players.addPlayer(newPlayer);
        console.log(this.players);
    }
    onPing(data) {
        console.log("pong from server");
        this.server.emit("onPing");
        return data;
    }
};
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", socket_io_1.Server)
], GamesGateway.prototype, "server", void 0);
__decorate([
    (0, websockets_1.SubscribeMessage)('userConnect'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket,
        Players_1.Player]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "userConnect", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('onPing'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", String)
], GamesGateway.prototype, "onPing", null);
GamesGateway = __decorate([
    (0, websockets_1.WebSocketGateway)(3002, { cors: true, transports: ['websocket'] }),
    __metadata("design:paramtypes", [games_service_1.GamesService])
], GamesGateway);
exports.GamesGateway = GamesGateway;
//# sourceMappingURL=games.gateway.js.map