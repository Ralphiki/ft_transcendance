import { Response, Request } from 'express';
import { AuthService } from "./auth.service";
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    handleAuthentication(): void;
    handleRedirection(req: Request, res: Response): Promise<void>;
    logout(req: any, res: Response): void;
}
