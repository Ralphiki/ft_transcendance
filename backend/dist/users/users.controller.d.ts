import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { CreateFriendsDto } from './dto/create-friends.dto';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUserDto: CreateUserDto): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    findAll(): import(".prisma/client").PrismaPromise<import(".prisma/client").Users[]>;
    message(): any;
    findOne(id: string, req: any): Promise<import(".prisma/client").Users>;
    update(id: string, updateUserDto: UpdateUserDto): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    getinfo(req: any): any;
    remove(id: string): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    addfriends(friends: string, req: any): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    deletefriends(friends: string, req: any): Promise<import(".prisma/client").Users>;
    friends: string;
    requestfriends(createfriends: CreateFriendsDto, req: any): Promise<import(".prisma/client").Prisma.BatchPayload>;
    addblocked(blocked: string, req: any): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    deleteblocked(blocked: string, req: any): Promise<import(".prisma/client").Users>;
}
