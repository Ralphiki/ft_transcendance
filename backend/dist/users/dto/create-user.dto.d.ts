export declare class CreateUserDto {
    username: string;
    hash: string;
    avatar: string;
    intra_id: number;
    dblauth: boolean;
    mail: string;
    friends: string[];
    blacklist: string[];
    groups: string[];
}
