import { CreateUserDto } from './create-user.dto';
declare const UpdateUserDto_base: import("@nestjs/common").Type<Partial<CreateUserDto>>;
export declare class UpdateUserDto extends UpdateUserDto_base {
    username: string;
    avatar: string;
    title: string;
    status: string;
    mail: string;
    max_score: number;
}
export {};
