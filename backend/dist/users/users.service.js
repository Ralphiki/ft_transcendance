"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
let UsersService = class UsersService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    create(createUserDto) {
        return this.prisma.users.create({ data: createUserDto });
    }
    findAll() {
        return this.prisma.users.findMany();
    }
    findmessage() {
        return this.prisma.room.findMany();
    }
    async findOne(uid) {
        if (Number.isNaN(uid)) {
            throw new common_1.HttpException('DATA NOT FOUND', common_1.HttpStatus.NOT_FOUND);
        }
        const content = await this.prisma.users.findUnique({
            where: { intra_id: uid },
        });
        if (!content) {
        }
        return content;
    }
    update(id, updateUserDto) {
        return this.prisma.users.update({
            where: { intra_id: id },
            data: updateUserDto,
        });
    }
    remove(id) {
        return this.prisma.users.delete({ where: { intra_id: id } });
    }
    addfriends(friends, intra_id) {
        const amp = parseInt(friends);
        return this.prisma.users.update({
            where: { intra_id: intra_id },
            data: {
                friends: {
                    push: friends,
                },
            },
        });
    }
    async deletefriends(friends, idintra) {
        const amp = parseInt(friends);
        const user = await this.prisma.users.findUnique({
            where: {
                intra_id: idintra,
            },
        });
        const find = user.friends.indexOf(friends);
        user.friends.splice(find, 1);
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                friends: user.friends,
            },
        });
    }
    addblocked(blocked, idintra) {
        const amp = parseInt(blocked);
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                blacklist: {
                    push: blocked,
                },
            },
        });
    }
    async requestfriends(friends, demand) {
        return this.prisma.friends.createMany({
            data: [
                {
                    requester: demand.toString(),
                    status: 'confirm',
                    friends: friends.friends,
                },
                {
                    requester: friends.friends,
                    status: 'confirm',
                    friends: demand.toString(),
                },
            ],
        });
    }
    async deleteblocked(blocked, idintra) {
        const amp = parseInt(blocked);
        const user = await this.prisma.users.findUnique({
            where: {
                intra_id: idintra,
            },
        });
        const find = user.friends.indexOf(blocked);
        if (find > -1) {
            throw new common_1.HttpException('DATA NOT FOUND', common_1.HttpStatus.NOT_FOUND);
        }
        user.blacklist.splice(find, 1);
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                blacklist: user.blacklist,
            },
        });
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map