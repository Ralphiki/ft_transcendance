import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from '../prisma/prisma.service';
import { CreateFriendsDto } from './dto/create-friends.dto';
export declare class UsersService {
    private prisma;
    constructor(prisma: PrismaService);
    create(createUserDto: CreateUserDto): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    findAll(): import(".prisma/client").PrismaPromise<import(".prisma/client").Users[]>;
    findmessage(): any;
    findOne(uid: number): Promise<import(".prisma/client").Users>;
    update(id: number, updateUserDto: UpdateUserDto): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    remove(id: number): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    addfriends(friends: string, intra_id: number): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    deletefriends(friends: string, idintra: number): Promise<import(".prisma/client").Users>;
    addblocked(blocked: string, idintra: number): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    requestfriends(friends: CreateFriendsDto, demand: string): Promise<import(".prisma/client").Prisma.BatchPayload>;
    deleteblocked(blocked: string, idintra: number): Promise<import(".prisma/client").Users>;
}
