/*
  Warnings:

  - A unique constraint covering the columns `[room_uid]` on the table `room` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "room_room_uid_key" ON "room"("room_uid");
