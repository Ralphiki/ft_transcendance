/*
  Warnings:

  - You are about to drop the column `inQueue` on the `Game` table. All the data in the column will be lost.
  - You are about to drop the column `isActive` on the `Game` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Game" DROP COLUMN "inQueue",
DROP COLUMN "isActive",
ADD COLUMN     "isRunning" BOOLEAN NOT NULL DEFAULT false;
