/*
  Warnings:

  - The primary key for the `room` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- DropIndex
DROP INDEX "room_id_key";

-- AlterTable
ALTER TABLE "room" DROP CONSTRAINT "room_pkey",
ADD CONSTRAINT "room_pkey" PRIMARY KEY ("id");
