-- CreateTable
CREATE TABLE "Chat" (
    "id" SERIAL NOT NULL,
    "password" TEXT
);

-- CreateTable
CREATE TABLE "ChatData" (
    "id" SERIAL NOT NULL,
    "message" TEXT,
    "chatdata" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Chat_id_key" ON "Chat"("id");

-- CreateIndex
CREATE UNIQUE INDEX "ChatData_id_key" ON "ChatData"("id");

-- CreateIndex
CREATE UNIQUE INDEX "ChatData_chatdata_key" ON "ChatData"("chatdata");

-- AddForeignKey
ALTER TABLE "ChatData" ADD CONSTRAINT "ChatData_chatdata_fkey" FOREIGN KEY ("chatdata") REFERENCES "Chat"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
