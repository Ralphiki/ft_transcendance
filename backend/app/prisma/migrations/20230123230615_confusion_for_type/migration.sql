/*
  Warnings:

  - You are about to drop the column `type` on the `room` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "room" DROP COLUMN "type",
ADD COLUMN     "typechat" TEXT;
