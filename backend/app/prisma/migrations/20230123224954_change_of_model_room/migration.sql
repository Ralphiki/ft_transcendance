/*
  Warnings:

  - You are about to drop the `Chat` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `ChatData` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "ChatData" DROP CONSTRAINT "ChatData_chatdata_fkey";

-- DropTable
DROP TABLE "Chat";

-- DropTable
DROP TABLE "ChatData";

-- CreateTable
CREATE TABLE "room" (
    "id" SERIAL NOT NULL,
    "owner" TEXT,
    "admin" TEXT[],
    "banned" TEXT[],
    "message" TEXT[],
    "password" TEXT[],
    "room_uid" TEXT NOT NULL,

    CONSTRAINT "room_pkey" PRIMARY KEY ("room_uid")
);

-- CreateIndex
CREATE UNIQUE INDEX "room_id_key" ON "room"("id");
