import { Injectable } from '@nestjs/common';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateMsgDto } from './dto/update-chat.dto';
import { PrismaService } from "../prisma/prisma.service";
import { UsersService } from '../users/users.service'
import * as bcrypt from 'bcrypt';

async function encrypt(password: string) {
    const salt = 10;
    const hash = await bcrypt.hash(password, salt);
    return hash;
}

async function decrypte(password: string, hash: string)
{
    const ret = await bcrypt.compare(password, hash);
    return ret;
}

@Injectable()
export class ChatService {
    constructor(
        private prisma: PrismaService,
        private readonly usersService: UsersService
    ) {}

    async decrypt(password: string, room_uid: string) {
        const room = await this.findOneByRoomId(room_uid);
        return await decrypte(password, room.password);
    }

    async create(chat: CreateChatDto) {
        if (!chat.room_uid) {
            let new_id = await this.prisma.room.count();
            chat.name = "channel#" + new_id
            chat.room_uid = chat.name
        }

        if (chat.password !== undefined && chat.password.length) {
            let password = await encrypt(chat.password)
            const new_chat = await this.prisma.room.create({
                data: {
                    owner: chat.owner,
                    name: chat.name,
                    typechat: chat.typechat,
                    admin: chat.admin,
                    password: password,
                    room_uid: chat.room_uid,
                    members: chat.members,
                    message: [],
                }})
                return new_chat;
        } else
            {
                const new_chat = await this.prisma.room.create({
                    data: {
                        owner: chat.owner,
                        name: chat.name,
                        typechat: chat.typechat,
                        admin: chat.admin,
                        password: "null",
                        room_uid: chat.room_uid,
                        members: chat.members,
                        message: [],
                    }
                })
                return new_chat;
            }
    }

    findAll() {
        return this.prisma.room.findMany();
    }

    async findAllMessages(room_id: string) {
        var room = await this.findOneByRoomId(room_id);
        if (room === null)
            return [];
        return room.message;
    }

    async findOne(id: number) {
        return await this.prisma.room.findFirst({
            where: {
                id: +id,
            }
        })
    }

    async findOneByRoomId(room_id: string) {
        return await this.prisma.room.findFirst({
            where: {
                room_uid: room_id,
            }
        })
    }

    async findMembersByRoomId(id: string) {
        const room = await this.prisma.room.findFirst({
            where: {
                id: +id
            }
        })
        let members = []
        // THIS IS HOW TO DO A FOREACH BUT WITH ASYNC, YOU NEED TO USE MAP, VERY IMPORTANT TO LEARN
        await Promise.all(room["members"].map(async (m_id) => {
            let member = await this.usersService.findOne(+m_id);
            await members.push(member);
        }))
        return members;
    }

    async updateChannelChat(id: number, intra_id: string, action: string) {
        const room = await this.prisma.room.findUnique({
            where: {
                id: +id
            },
        });
        if (action == "addadmin") {
            if (!room["admin"].includes(intra_id)) {
                const ret = await this.prisma.room.update({
                    where: {
                        id: +id,
                    },
                    data: {
                        admin: {
                            push: intra_id
                        }
                    }
                })
                console.log(ret)
                return ret;
            }
        } else if (action == "deleteadmin") {
            const find = room.admin.indexOf(intra_id);
            room.admin.splice(find, 1);
            return await this.prisma.room.update({
                where: {id: +id},
                data: {
                    admin: {
                        set: room.admin.filter((id) => id !== intra_id),
                    }
                },
            });
        } else if (action == "addbanned") {
            if (!room["banned"].includes(intra_id)) {
                return await this.prisma.room.update({
                    where: {
                        id: +id,
                    },
                    data: {
                        members: {
                            set: room.members.filter((id) => id !== intra_id),
                        },
                        banned: {
                            push: intra_id
                        }
                    }
                })
            }
        } else if (action == "deletebanned") {
            const find = room.banned.indexOf(intra_id);
            room.banned.splice(find, 1);
            return await this.prisma.room.update({
                where: {id: +id},
                data: {
                    banned: {
                        set: room.banned.filter((id) => id !== intra_id),
                    }
                },
            });
        } else if (action == "addmembers") {
            if (!room["members"].includes(intra_id)) {
                return this.prisma.room.update({
                    where: {
                        id: +id,
                    },
                    data: {
                        members: {
                            push: intra_id
                        }
                    }
                })
            }
        } else if (action == "deletemembers") {
            const find = room.members.indexOf(intra_id);
            room.members.splice(find, 1);
            return await this.prisma.room.update({
                where: {id: +id},
                data: {
                    members: {
                        set: room.members.filter((id) => id !== intra_id),
                    }
                },
            });
        } else if (action == "mute") {
            if (!room["muted"].includes(intra_id)) {
                return await this.prisma.room.update({
                    where: {id: +id},
                    data: {
                        muted: {
                            push: intra_id,
                        }
                    },
                });
            }
        } else if (action == "unmute") {
            if (room["muted"].includes(intra_id)) {
                return await this.prisma.room.update({
                    where: {id: +id},
                    data: {
                        muted: {
                            set: room.muted.filter((id) => id !== intra_id),
                        }
                    },
                });
            }
        }
        return room;
    }

    remove(room_id: number) {
        return `This action removes a #${room_id} chat`;
    }

    async message(data: UpdateMsgDto) {
        var json = {
            message: data.message_data,
            user_id: data.user_id,
            user_name: data.user_name,
            avatar: data.user_avatar,
            sentAt: data.sentAt
        }
        const r = await this.prisma.room.update({
            where: {
                room_uid: data.room_id
            },
            data: {
                message: {
                    push: json
                }
            }
        });
        return json;
    }

}
