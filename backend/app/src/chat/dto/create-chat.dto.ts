import {IsArray, IsOptional, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class CreateData {
    @ApiProperty()
    @IsString()
    message: string
}

export class CreateChatDto {
    @ApiProperty()
    @IsString()
    @IsOptional()
    owner: string

    @ApiProperty()
    @IsString()
    @IsOptional()
    typechat: string

    @ApiProperty()
    @IsString()
    @IsOptional()
    name: string

    @ApiProperty()
    @IsString()
    @IsArray()
    @IsOptional()
    admin: string[]

    @ApiProperty()
    @IsString()
    @IsArray()
    @IsOptional()
    banned: string[]

    @ApiProperty()
    @IsString()
    @IsArray()
    @IsOptional()
    members: string[]

    @ApiProperty()
    @IsString()
    @IsArray()
    @IsOptional()
    message: string[]

    @ApiProperty()
    @IsString()
    @IsOptional()
    password: string

    @ApiProperty()
    @IsString()
    room_uid: string

    @ApiProperty()
    @IsString()
    @IsArray()
    @IsOptional()
    muted: string[]
}
