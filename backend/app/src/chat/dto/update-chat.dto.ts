import {IsArray, IsOptional, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class UpdateMsgDto  {
  user_id: string
  user_name: string
  user_avatar: string
  room_id: string
  message_data: string
  sentAt: any
}

export class UpdateChatDto {
  @ApiProperty()
  @IsString()
  @IsOptional()
  owner: string

  @ApiProperty()
  @IsString()
  @IsOptional()
  typechat: string

  @ApiProperty()
  @IsString()
  @IsOptional()
  intra_id: string

  @ApiProperty()
  @IsString()
  @IsOptional()
  name: string

  @ApiProperty()
  @IsString()
  @IsArray()
  @IsOptional()
  admin: string[]

  @ApiProperty()
  @IsString()
  @IsArray()
  @IsOptional()
  banned: string[]

  @ApiProperty()
  @IsString()
  @IsArray()
  @IsOptional()
  members: string[]

  @ApiProperty()
  @IsString()
  @IsArray()
  @IsOptional()
  message: string[]

  @ApiProperty()
  @IsString()
  @IsOptional()
  password: string

  @ApiProperty()
  @IsString()
  room_uid: string

  @ApiProperty()
  @IsString()
  action: string
}
