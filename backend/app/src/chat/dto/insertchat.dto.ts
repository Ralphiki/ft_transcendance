import {IsOptional, IsString} from "class-validator";
import {ApiProperty} from "@nestjs/swagger";

export class Messagedto {
    @ApiProperty()
    @IsString()
    @IsOptional()
    password: string
    @IsString()
    token: string
}
