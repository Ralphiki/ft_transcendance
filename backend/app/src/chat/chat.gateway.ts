import {
  ConnectedSocket,
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { ChatService } from './chat.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateMsgDto } from './dto/update-chat.dto';
import { Socket } from 'socket.io';
import { Server } from 'socket.io';

@WebSocketGateway({cors: true})
export class ChatGateway {
constructor(
  private readonly chatService: ChatService
) {}

private user2socket: Map<number, string> = new Map();

@WebSocketServer()
server: Server;

@SubscribeMessage('create_room')
handleEvent(@MessageBody() data: CreateChatDto) 
{
  return this.chatService.create(data)
}

@SubscribeMessage('get_room')
async checkIfRoomExists(@MessageBody('roomid') roomid: string) 
{
  return this.chatService.findOneByRoomId(roomid);
}

@SubscribeMessage('getMsgHistory') // findAllMessages
async getPreviousMessages(@MessageBody('roomid') roomid: string) 
{
  const msg = await this.chatService.findAllMessages(roomid)
  return msg;
}

@SubscribeMessage('public_room')
async  publicroom(@MessageBody('action') action: string, create: CreateChatDto ) 
{
    return this.publicroom(action, create)
}

@SubscribeMessage('new_mp')
async newMessage(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: { json: UpdateMsgDto, roomid: string }) 
{

  const msg = await this.chatService.message(data.json)
  if (data.roomid)
      this.server.to(data.roomid).emit('broadcast_mp', msg);
  return msg;
}

@SubscribeMessage('join_room')
joinRoom(
  @MessageBody() data: { name: string, intra_id: number },
  @ConnectedSocket() client: Socket) 
{
  if (data.intra_id !== undefined) {
    this.user2socket.set(data.intra_id, client.id);
  }
  client.join(data.name);
  return this.user2socket.get(data.intra_id);
}


@SubscribeMessage('kick_member')
async kickMember(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: { intra_id: number, roomid: string }) 
{

    let client_socket = this.user2socket.get(+data.intra_id);
    if (client_socket)
      this.server.to(client_socket).emit('been_kicked');
}

@SubscribeMessage('mute_member')
async muteMember(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: { intra_id: number, roomid: string }) 
{
    let client_socket = this.user2socket.get(+data.intra_id);
    if (client_socket)
        this.server.to(client_socket).emit('been_muted');
}

  @SubscribeMessage('unmute_member')
  updateUnMute(@ConnectedSocket() client: Socket,
               @MessageBody() data: { intra_id: number, roomid: string })
  {
      let client_socket = this.user2socket.get(+data.intra_id);
      if (client_socket)
        this.server.to(client_socket).emit('been_unmuted');
  }

  @SubscribeMessage('leave_room')
  leaveRoom(@ConnectedSocket() client: Socket,
            @MessageBody() roomid: string) 
  {
    if (client)
      client.leave(roomid);
  }

  @SubscribeMessage('decrypt')
  async decrypt(@ConnectedSocket() client: Socket, @MessageBody() data: {intra_id: number, password: string, roomid: string})
  {
      let client_socket = this.user2socket.get(+data.intra_id);
      let right_password = await this.chatService.decrypt(data.password, data.roomid);
      if (client_socket)
        this.server.to(client_socket).emit('been_decrypted', right_password);
  }


  @SubscribeMessage('removeChat')
  remove(client: Socket,  @MessageBody() id: number)
  {
    return this.chatService.remove(id);
  }

  @SubscribeMessage('addMember')
  addMember(@ConnectedSocket() client: Socket,
                @MessageBody() data: {intra_id: number, room_uid: string}) {
      const client_socket = this.user2socket.get(+data.intra_id);
      if (client_socket)
          this.server.to(client_socket).emit("been_added", {id: data.room_uid});
  }

  @SubscribeMessage('updateMembers')
  updateMembers(@ConnectedSocket() client: Socket,
                    @MessageBody() data: {id: number, room_uid: string}) {
    this.server.to(data.room_uid).emit("updateMembers", {id: data.id});
  }

  @SubscribeMessage('updateBlockUser')
  updateBlockUser(@ConnectedSocket() client: Socket,
                  @MessageBody() data: {user: any, target: any, isBlocked: boolean})
  {
      const client_socket = this.user2socket.get(+data.target.intra_id);
      if (client_socket) 
        this.server.to(client_socket).emit("updateBlockUser", {user: data.user, isBlocked: data.isBlocked});
  }

  @SubscribeMessage('add_admin')
  addAdmin(@ConnectedSocket() client: Socket,
             @MessageBody() data: { id: number, intra_id: number, roomid: string })
  {
      let client_socket = this.user2socket.get(+data.intra_id);
      if (client_socket) {
          this.server.to(client_socket).emit('made_admin', {id: data.id});
        this.updateMembers(client, {id: data.id, room_uid: data.roomid});
      }
  }

  @SubscribeMessage('unban_member')
  async updateUnban(@ConnectedSocket() client: Socket,
             @MessageBody() data: { id: number, intra_id: number, roomid: string })
  {
      let client_socket = this.user2socket.get(+data.intra_id);
      if (client_socket) {
          this.server.to(client_socket).emit('been_unbanned');
          this.updateMembers(client, {id: data.id, room_uid: data.roomid});
      }
  }

  @SubscribeMessage('ban_member')
  updateBan(@ConnectedSocket() client: Socket,
             @MessageBody() data: { id: number, intra_id: number, roomid: string })
  {
      let client_socket = this.user2socket.get(+data.intra_id);
      if (client_socket) {
        this.server.to(client_socket).emit('been_banned');
        this.updateMembers(client, {id: data.id, room_uid: data.roomid});
      }
  }

}
