import { Controller, Get, Post, Put, Delete, Body, Param } from '@nestjs/common';
import { ChatService } from './chat.service';
import { Chat } from './entities/chat.entity';
import { CreateChatDto } from "./dto/create-chat.dto";
import {UpdateChatDto} from "./dto/update-chat.dto";

@Controller('chat')
export class ChatController {
    constructor(private readonly chatService: ChatService) {}

    @Post()
    async create(@Body() createChatDto: CreateChatDto): Promise<Chat> {
        return await this.chatService.create(createChatDto);
    }

    @Get()
    async findAll(): Promise<Chat[]> {
        return await this.chatService.findAll();
    }

    @Get('find/:id')
    async findOne(@Param('id') id: number): Promise<Chat> {
        return await this.chatService.findOne(id);
    }

    @Get('findroom/:id')
    async findOneByRoomid(@Param('id') id: string): Promise<Chat> {
        return await this.chatService.findOneByRoomId(id);
    }

    @Get('findmembers/:id')
    async findMembersByRoomid(@Param('id') id: string): Promise<Chat> {
        return await this.chatService.findMembersByRoomId(id);
    }

    @Put('update/:id')
    async update(@Param('id') id: number, @Body() updateChatDto: UpdateChatDto): Promise<Chat> {
        return await this.chatService.updateChannelChat(id, updateChatDto.intra_id, updateChatDto.action);
    }

    @Delete('delete/:id')
    async remove(@Param('id') id: number): Promise<Chat> {
        const find = await this.findOne(id);
        if (find)
            return await this.chatService.remove(id);
    }

}
