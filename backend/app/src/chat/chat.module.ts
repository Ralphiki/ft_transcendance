import { Module } from '@nestjs/common';
import { ChatService } from './chat.service';
import { ChatGateway } from './chat.gateway';
import { ChatController } from './chat.controller';
import { UsersService } from 'src/users/users.service';

@Module({
  providers: [ChatGateway, ChatService, UsersService],
  controllers: [ChatController],
})
export class ChatModule {}
