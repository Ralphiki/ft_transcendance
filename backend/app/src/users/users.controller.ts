import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Request,
  Res,
  ValidationPipe,
  UsePipes,
  UseInterceptors,
  UploadedFile,
  ParseFilePipe,
  FileTypeValidator, MaxFileSizeValidator,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { JwtAuthGuard } from '../auth/jwt.authguard';
import {ApiBearerAuth, ApiConsumes} from '@nestjs/swagger';
import { FileInterceptor } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import { extname } from 'path';


@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':uid')
  async findOne(@Param('uid') id: string, @Request() req) {
    return this.usersService.findOne(+id);
  }

  @Get('/username/:username')
  async findOneByUsername(@Param('username') username: string, @Request() req) {
    return this.usersService.findOneByUsername(username);
  }

  @ApiBearerAuth('JWT-auth')
  @UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe({ transform: true }))
  @Patch('/profile/:id')
  updateprofile(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
      return this.usersService.updateprofile(+id, updateUserDto);
  }

  @ApiBearerAuth('JWT-auth')
  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
      return this.usersService.update(+id, updateUserDto);
  }

  @ApiConsumes('multipart/form-data')
  @Post("/avatar/:id")
  @UseInterceptors(
      FileInterceptor('image', {
        storage: diskStorage({
          destination: './uploads',
          filename: (req, file, cb) => {
            const randomName = Array(32).fill(null).map(() => Math.round(Math.random() * 16).toString(16)).join('');
            cb(null, `${randomName}${extname(file.originalname)}`);
          },
        }),
      }),
  )
  async upload(@Param('id') id: string,
               @UploadedFile(new ParseFilePipe({
                 validators: [
                   new FileTypeValidator({ fileType: '.(png|jpeg|jpg)' }),
                   new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 4 }),
                 ],
               })) file: Express.Multer.File) {
    return await this.usersService.upload(+id, file.filename);
  }

  @Get(':imgpath')
  seeUploadedFile(@Param('imgpath') image, @Res() res) {
    return res.sendFile(image, { root: './uploads' });
  }

  @ApiBearerAuth('JWT-auth')
  @UseGuards(JwtAuthGuard)
  @Get('/userid/id')
  getinfo(@Request() req) {
    return req.user;
  }

  @ApiBearerAuth('JWT-auth')
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }

  @ApiBearerAuth('JWT-auth')
  @Post('friends/add')
  @UseGuards(JwtAuthGuard)
  addfriends(@Body('friends') friends: string, @Request() req) {
    return this.usersService.addfriends(friends, req.user.login42);
  }

  @ApiBearerAuth('JWT-auth')
  @UseGuards(JwtAuthGuard)
  @Post('friends/delete')
  deletefriends(@Body('friends') friends: string, @Request() req) {
    return this.usersService.deletefriends(friends, req.user.login42);
  }

  @UseGuards(JwtAuthGuard)
  @Get('block/add/:blocked')
  addblocked(@Param('blocked') blocked: string, @Request() req) {
    return this.usersService.addblocked(blocked, req.user.login42);
  }

  @ApiBearerAuth('JWT-auth')
  @UseGuards(JwtAuthGuard)
  @Get('block/delete/:id')
  deleteblocked(@Param('id') blocked: string, @Request() req) {
    return this.usersService.deleteblocked(blocked, req.user.login42);
  }
}
