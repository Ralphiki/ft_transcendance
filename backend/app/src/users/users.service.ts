import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from '../prisma/prisma.service';
import { backendURL } from 'src/urls';

@Injectable()
export class UsersService {
    constructor(private prisma: PrismaService) {}

    create(createUserDto: CreateUserDto) {
        return this.prisma.users.create({ data: createUserDto });
    }

    findAll() {
        return this.prisma.users.findMany();
    }

    async findOne(uid: number) {
    	    //if (uid === undefined || Number.isNaN(uid))
	//	{
	//		return ;
	//	}
	try {
        const content = await this.prisma.users.findUnique({
            where: { intra_id: uid },
        });
		//if (content === null)
             	//	throw new HttpException('DATA NOT FOUND', HttpStatus.NOT_FOUND)
        	return content;
    	} catch (error) {
        	console.error(error);
             	throw new HttpException('DATA NOT FOUND', HttpStatus.NOT_FOUND)
    		
	}
    }

    async findOneByUsername(username: string) {
         //if (!username.length) {
	//	 return ;
	 //}
	 try {
	    const content = await this.prisma.users.findFirst({
             where: { username: username },
         });
		//if (content === null)
             	//	throw new HttpException('DATA NOT FOUND', HttpStatus.NOT_FOUND)
         	return content;
	 } catch (error) {
	 	console.error(error);
             	throw new HttpException('DATA NOT FOUND', HttpStatus.NOT_FOUND)
	 }
     }


    async updateprofile(id: number, updateUserDto: UpdateUserDto) {
        const check = await this.findOneByUsername(updateUserDto.username);
        if (check && check.intra_id != id)
            throw new HttpException({ message: ['Username must be unique'] }, HttpStatus.BAD_REQUEST);
        return await this.prisma.users.update({
            where: {intra_id: id},
            data: updateUserDto,
        });
    }

    async update(id: number, updateUserDto: UpdateUserDto) {
        return await this.prisma.users.update({
            where: {intra_id: id},
            data: updateUserDto,
        });
    }

    async upload(intra_id: number, filename: string) {
        let path = backendURL + "/" + filename;
        return await this.prisma.users.update({
            where: { intra_id: intra_id },
            data: {
                avatar: path,
            }});
    }

    remove(id: number) {
        return this.prisma.users.delete({ where: { intra_id: id } });
    }

    async addfriends(friends: string, intra_id: number) {
        await this.prisma.users.update({
            where: { intra_id: +friends },
            data: {
                friends: {
                    push: intra_id.toString(),
                },
            },
        });
        return this.prisma.users.update({
            where: { intra_id: intra_id },
            data: {
                friends: {
                    push: friends,
                },
            },
        });
    }

    async deletefriends(friend: string, idintra: number) {
        const user1 = await this.prisma.users.findUnique({
            where: {
                intra_id: +friend,
            },
        });
        await this.prisma.users.update({
            where: { intra_id: +friend },
            data: {
                friends: {
                    set: user1.friends.filter((id) => id !== idintra.toString()),
                }
            },
        });
        const user2 = await this.prisma.users.findUnique({
            where: {
                intra_id: idintra,
            },
        });
        return this.prisma.users.update({
            where: { intra_id: idintra },
                data: {
                    friends: {
                        set: user2.friends.filter((id) => id !== friend),
                    }
                },
        });
    }

    async addblocked(blocked: string, idintra: number) {
        const user = await this.findOne(idintra);
        if (user.blacklist.includes(blocked))
            return user ;
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                blacklist: {
                    push: blocked,
                },
            },
        });
    }


    async deleteblocked(blocked: string, idintra: number) {
        const user = await this.prisma.users.findUnique({
            where: {
                intra_id: idintra,
            },
        });
        const find = user.blacklist.indexOf(blocked);
        if (find == -1) return user ;
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                blacklist: {
                    set: user.blacklist.filter((id) => id !== blocked),
                }
            },
        });
    }
}
