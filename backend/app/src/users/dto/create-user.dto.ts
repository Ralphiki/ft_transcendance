import { ApiProperty } from '@nestjs/swagger'
import {IsEmail, IsOptional} from "class-validator";

export class CreateUserDto {
    @ApiProperty()
    username: string;
    @ApiProperty()
    hash: string;
    @ApiProperty()
    avatar: string;
    @ApiProperty()
    intra_id: number;
    @ApiProperty()
    dblauth: boolean = false;
    @ApiProperty()
    @IsEmail()
    @IsOptional()
    mail: string
    @ApiProperty()
    friends: string[];
    @ApiProperty()
    blacklist: string[];
    @ApiProperty()
    groups: string[]
    @ApiProperty()
    games: number[]
}

