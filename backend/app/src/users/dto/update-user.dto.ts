import { PartialType } from '@nestjs/swagger';
import { CreateUserDto } from './create-user.dto';
import { ApiProperty} from '@nestjs/swagger'
import {IsEmail, IsOptional} from "class-validator";
export class UpdateUserDto extends PartialType(CreateUserDto) {

    @ApiProperty()
    @IsOptional()
    username: string;

    @ApiProperty()
    @IsOptional()
    avatar: string;

    @IsOptional()
    @ApiProperty()
    title: string;

    @ApiProperty()
    status: string;

    @ApiProperty()
    @IsEmail({ message: 'Email format not valid' })
    @IsOptional()
    mail: string;

    @ApiProperty()
    max_score: number;
    @ApiProperty()
    friends: string[];
    @ApiProperty()
    blacklist: string[];
    @ApiProperty()
    groups: string[]
    @ApiProperty()
    games: number[]
}
