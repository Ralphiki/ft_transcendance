import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as session from 'express-session';
import * as passport from 'passport';
import { PrismaSessionStore } from '@quixo3/prisma-session-store';
import { PrismaClient } from '@prisma/client';
import * as bodyParser from 'body-parser';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import { readFileSync } from "node:fs";

async function bootstrap() {
	const httpsOptions = {
		key: readFileSync('/cert/key.pem'),
		cert: readFileSync('/cert/certificate.pem'),
	};
  const app = await NestFactory.create<NestExpressApplication>(AppModule, { httpsOptions });
  const config = new DocumentBuilder()
    .setTitle('Transcendance api')
    .setDescription('Api for transcendance')
    .setVersion('0.1')
    .addBearerAuth(
      {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'JWT',
        description: 'Enter JWT token',
        in: 'header',
      },
      'JWT-auth', // This name here is important for matching up with @ApiBearerAuth() in your controller!
    )
    .build();

    const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
	app.enableCors({});
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept',
    );
    next();
  });
    app.use(bodyParser.json({limit: '200mb'}));
    app.use(bodyParser.urlencoded({limit: '200mb', extended: true}));
  app.use(
    session({
      cookie: {
        secure: true,
        httpOnly: false,
 	sameSite: "none",
      },
      secret: process.env.SECRET_SESSION,
      resave: false,
      saveUninitialized: false,
      store: new PrismaSessionStore(new PrismaClient(), {
        checkPeriod: 2 * 60 * 1000, //ms
        dbRecordIdIsSessionId: true,
        dbRecordIdFunction: undefined,
      }),
    }),
  );
  app.useStaticAssets(join(__dirname, '..', 'uploads'));
  app.use(passport.initialize());
  app.use(passport.session());
  await app.listen(3001);
}

bootstrap();
