import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { ProfilsModule } from './profils/profils.module';
import { AuthService } from "./auth/auth.service";
import { JwtService } from "@nestjs/jwt";
import { GamesModule } from './games/games.module';
import { ChatModule } from './chat/chat.module';
import { MulterModule } from "@nestjs/platform-express";

@Module({
  imports: [
    PrismaModule, 
    UsersModule,
    MulterModule.register({
      dest: './uploads'
    }),
    AuthModule, 
    ConfigModule.forRoot(),
    PassportModule.register({ session: true }), // this enables session for passport
    // ServeStaticModule.forRoot({
		// 	rootPath: join(__dirname, '../', 'uploads'),
		// }),
    ProfilsModule,
    ChatModule,
    GamesModule,
  ],
  controllers: [AppController],
  providers: [AppService, AuthService, JwtService],
})
export class AppModule {}
