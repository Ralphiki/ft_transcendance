import { UserData } from 'src/prisma/types';
import { UsersService } from '../users/users.service'
import { CreateUserDto } from '../users/dto/create-user.dto';
import { JwtService } from '@nestjs/jwt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthService {

    constructor(
        private readonly usersService: UsersService,
        private jwtService: JwtService
    ) {}

    async validateUser(data: UserData){
        const user =  await this.usersService.findOne(+data["id"]);
        if (user) {
            user["status"] = "online";
            return this.usersService.update(+user.intra_id, user);
        }
        const new_user = new CreateUserDto;
        new_user["username"] = data["username"];
        new_user["avatar"] = data["_json"]["image"]["link"];
        new_user["intra_id"] = parseInt(data["id"]);
        const newUser = this.usersService.create( new_user );
      return newUser;
    }

    async findUser(intratoken: number){
        const user = await this.usersService.findOne( intratoken );
        return user;
    }

    async login(user: any) {
        const payload = { id: user.uid, login42: user.intra_id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async logout(user: any) {
        const toLogout = await this.usersService.findOne( +user.login42 )
        toLogout["status"] = "offline";
        return this.usersService.update(+toLogout.intra_id, toLogout);
    }
}
