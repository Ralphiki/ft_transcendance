import {
    Controller,
    Get,
    UseGuards,
    Req,
    Res,
    Query,
    Inject
} from '@nestjs/common';
import { Auth42Guard } from './auth.guards';
import { Response, Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from "./auth.service";
import { UsersService } from "../users/users.service";
import {frontendURL} from "../urls";


@Controller() // this is default get url for this controller
export class AuthController {
    constructor(
        @Inject('AUTH_SERVICE') private readonly authService: AuthService,
        private readonly jwtService: JwtService,
        private readonly usersService: UsersService,
    ) {};

    @Get('login')  // this is a decorator, it states where the user will be redirected
            // to authenticate
            // do NOT add ; at the end of the line
            // do NOT add / before the beginning of the path, eg. NO '/auth'
    @UseGuards(Auth42Guard)
    handleAuthentication(){}

    @Get('redirect') // this has to match the route ( no need to write localhost:3001 ) redirection of the 42 api settings
    @UseGuards(Auth42Guard)
    async handleRedirection(
        @Req() req: Request,
        @Res({ passthrough: true }) res: Response )
        {
            const user = req.user;
            const jwt = await this.authService.login(user);
            res.cookie('jwt_token', jwt.access_token, { sameSite: 'none', secure: true });
            res.redirect(frontendURL);
        }

    @Get('loginbyusername') // this has to match the route ( no need to write localhost:3001 ) redirection of the 42 api settings
    async loginByUsername(
        @Query('username') username : string, 
        @Res({ passthrough: true }) res: Response )
        {
            const user = await this.usersService.findOneByUsername(username);
            if (!user) {
                res.status(404).redirect(frontendURL);
            }
            else {                
                user["status"] = "online";
                await this.usersService.update(+user.intra_id, user);
                const jwt = await this.authService.login(user);
                res.cookie('jwt_token', jwt.access_token, { sameSite: 'none', secure: true });
                res.redirect(frontendURL);
            }
        }

    @Get('logout')
    async logout(
        @Query('user') jwt_token: string,
        @Res({ passthrough: true }) res: Response) 
        {
            const user = this.jwtService.verify(jwt_token);
            const logout = await this.authService.logout(user);
            res.cookie('jwt_token', '', { httpOnly: false, });
            res.status(302).redirect(frontendURL);
        }
}
