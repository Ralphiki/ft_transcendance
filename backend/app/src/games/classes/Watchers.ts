export class Watcher {
    intra_id: string;
    username: string;
    socketId?: string;
    roomId?: number;
    status: string;

    constructor(intra_id: string, username: string, socketId?: string) {
        this.intra_id = intra_id;
        this.username = username;
        this.socketId = socketId;
    }
}
