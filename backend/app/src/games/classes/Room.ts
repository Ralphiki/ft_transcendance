import {Player} from "./Players";
import {Game} from "../entities/game.entity";
import {Watcher} from "./Watchers";

export class Room {
    roomId: number;
    players: Player[];
    watchers: Watcher[];
    game: Game;

    constructor(
        roomId: number,
        players: Player[],
        watchers: Watcher[],
        game: Game
    ) {
        this.roomId = roomId;
        this.players = players;
        this.watchers = watchers;
        this.game = game;
    }

    addPlayer(player: Player) {
        this.players.push(player);
    }

    setGame(game) {
        this.game = game;
    }
}
