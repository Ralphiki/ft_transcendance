export class Player {
    intra_id: string;
    username: string;
    socketId?: string;
    roomId?: number;
    arrowUp: boolean;
    arrowDown: boolean;
    padX: number;
    padY: number;
    padH: number;
    isLastScorer: boolean;
    status: string;
    avatar: string;

    constructor(intra_id: string, username: string, padX: number, padY: number, socketId: string, avatar: string) {
        this.intra_id = intra_id;
        this.username = username;
        this.socketId = socketId;
        this.arrowUp = false;
        this.arrowDown = false;
        this.isLastScorer = false;
        this.padX = padX;
        this.padY = padY;
        this.avatar = avatar;
    }

    setRoomId(roomId: number | undefined) {
        this.roomId = roomId;
    }

    setArrowUp(arrowUp: boolean) {
        this.arrowUp = arrowUp;
    }
    setArrowDown(arrowDown: boolean) {
        this.arrowDown = arrowDown;
    }
    setPadX(padX: number) {
        this.padX = padX;
    }
    setPadY(padY: number) {
        this.padY = padY;
    }
    setPadH(padH: number) {
        this.padH = padH;
    }
}
