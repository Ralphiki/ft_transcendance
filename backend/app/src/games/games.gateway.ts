import {
    ConnectedSocket,
    MessageBody, OnGatewayConnection, OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer
} from "@nestjs/websockets";
import {GamesService} from "./games.service";
import {Socket, Server} from "socket.io"
import {Logger} from "@nestjs/common";
import {Player} from "./classes/Players";
import {Room} from "./classes/Room";
import {UsersService} from "../users/users.service";
import {Game} from "./entities/game.entity";
import {CreateGameDto} from "./dto/create-game.dto";
import {Watcher} from "./classes/Watchers";

const CANVAS_WIDTH = 1080;
const CANVAS_HEIGHT = 600;
const PAD_WIDTH = 15;
const PAD_HEIGHT = 120;
const PAD_SPEED = 16;
const BALL_SPEED = 7;
const BALL_RAD = 8;
const SURVIVAL_EROSION = 10;
const PU_MAX_SIZE = 150;
const MAX_SCORE = 3;

@WebSocketGateway({cors: true, transports: ['websocket']})
export class GamesGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    constructor(
        private readonly gamesService: GamesService,
        private readonly usersService: UsersService,
    ) {}

    private logger: Logger = new Logger('gamesGateway');
    private readonly rooms: Array<Room> = [];
    private readonly players: Array<Player> = [];
    private roomId: number = 0;
    private games: Map<number, Game> = new Map();
    private user2socket: Map<number, string> = new Map();

    getKeyByValue(map, searchValue) {
        for (let [key, value] of map.entries()) {
            if (value === searchValue)
                return key;
        }
    }

    @WebSocketServer()
    server: Server;

    afterInit(server: Server) {
        this.logger.log(`Init Pong Gateway`);
    }

    async handleConnection(@ConnectedSocket() client: Socket) {
        this.logger.log(`Client connected: ${client.id}`);
    }

    async handleDisconnect(@ConnectedSocket() client: Socket) {
        this.logger.log(`Client disconnected: ${client.id}`);
        const playerIntraId = this.getKeyByValue(this.user2socket, client.id);
        if (playerIntraId) {
            const player = this.players.filter(p => p.intra_id === playerIntraId)[0];
            if (player) {
                const room = this.rooms.filter(r => r.roomId === player.roomId)[0];
                if (room && room.game!== null && room.game.isActive) {
                    if (player.username === room.players[0].username) {
                       this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", {leaver: player.username, winner: room.players[1].username});
                    } else {
                        this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", {leaver: player.username, winner: room.players[0].username});
                    }
                    client.leave(room.roomId.toString());
                    this.gameOver(room.roomId, player.username);
                }
            }
        }
    }

    @SubscribeMessage('onStartAppConnection')
    onStartAppConnection(@ConnectedSocket() client: Socket,
                               @MessageBody() user: Player) {
        if (user.hasOwnProperty('intra_id')) {
            this.user2socket.set(+user.intra_id, client.id);
	    const player = this.players.find(e => e.intra_id == user.intra_id);
            if (player) {
                player.socketId = client.id;
            }
        }
    }

    createRoom(@ConnectedSocket() client: Socket,
                   @MessageBody() player: Player) {
        const room = new Room(this.roomId, [player], [], null);
        player.setRoomId(room.roomId);
        this.roomId++;
        this.rooms.push(room);
        client.join(room.roomId.toString());
        return room;
    }

    async createGame(@ConnectedSocket() client: Socket,
               @MessageBody() room: Room) {
        const new_game = new CreateGameDto;
        new_game["player1"] = room.players[0].username;
        new_game["player1Id"] = +room.players[0].intra_id;
        new_game["player2"] = room.players[1].username;
        new_game["player2Id"] = +room.players[1].intra_id;
        new_game["p1Avatar"] = room.players[0].avatar;
        new_game["p2Avatar"] = room.players[1].avatar;
        const game = await this.gamesService.create(new_game);
        client.join(room.roomId.toString());
        room.setGame(game);
        this.games.set(room.roomId, game);
        const P1SocketId = this.user2socket.get(+room.players[0].intra_id);
        this.server.to(P1SocketId).emit("startGame", {room: room, status: "playing", intra_id: room.players[0].intra_id});
        this.server.to(client.id).emit("startGame", {room: room, status: "playing", intra_id: room.players[1].intra_id});
    }

    @SubscribeMessage('joinRoom')
    async joinRoom(@ConnectedSocket() client: Socket,
                   @MessageBody() user: Player) {
        let player = this.players.find(e => e.intra_id == user.intra_id);
        if (player) {
            const room = this.rooms.filter((r) => r.roomId == player.roomId)[0];
            if (room) {
                client.join(room.roomId.toString());
                if (player.socketId)
                    this.server.to(player.socketId).emit("startGame", {room: room, status: "playing", intra_id: player.intra_id});
            } else {
                const indexPlayer = this.players.findIndex(u => u.socketId == client.id);
                if (player.socketId)
                    this.server.to(player.socketId).emit("updateStatus", {status: "online"});
                this.players.splice(indexPlayer, 1);
            }
            return ;
        } else if (!player && user.status === "playing") {
            if (client.id)
                this.server.to(client.id).emit("updateStatus", {status: "online"});
            return ;
        }
        player = new Player(user.intra_id, user.username, 0, 0, client.id, user.avatar);
        this.players.push(player);
        let room = this.rooms.filter((r) => r.players.length != 2)[0];
        if (room) {
            player.isLastScorer = true;
            room.addPlayer(player);
            player.setRoomId(room.roomId);
            if (room.players.length === 2 && this.games.has(room.roomId) === false) {
                await this.createGame(client, room);
            }
        } else {
            room = this.createRoom(client, player);
        }
        this.logger.log(user.username + " joined the room n°" + room.roomId);
        if (player.socketId)
            this.server.to(player.socketId).emit("setClientRoom", {intra_id: user.intra_id, room: room});
    }

    @SubscribeMessage('deleteRoom')
    async deleteRoom(@ConnectedSocket() client: Socket) {
        let user = this.players.find(u => u.socketId == client.id);
        if (user) {
            client.leave(user.roomId?.toString());
            let inPlayers = this.players.findIndex(u => u.socketId == client.id);
            if (inPlayers !== -1) {
                this.players.splice(inPlayers, 1);
            }
            let inRoom = this.rooms.findIndex(r => r.roomId == user.roomId);
            if (inRoom !== -1) {
                const gameObj = this.games.get(user.roomId);
                const id = gameObj?.id.toString();
                const gameOver = await this.gamesService.findOne(id);
                if (gameOver !== undefined && gameOver["isActive"]) {
                    gameOver["isActive"] = false;
                    gameOver["isRunning"] = false;
                    if (+user.intra_id == gameOver.player1Id) {
                        gameOver["winnerId"] = gameOver.player2Id;
                    } else {
                        gameOver["winnerId"] = gameOver.player1Id;
                    }
                    await this.gamesService.update(+gameOver.id, gameOver);
                }
                this.rooms.splice(inRoom, 1);
                this.games.delete(user.roomId)
            }
        }
    }

    @SubscribeMessage('onKeyDown')
    onKeyDown(@ConnectedSocket() client: Socket,
                  @MessageBody() data: {roomId: number, keycode: string, userId: string}) {
        const room: Room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        if (!room) {
            return ;
        }
        const player: Player = room.players.filter((p) => p.intra_id === data.userId)[0];

        if (data.keycode == 'ArrowUp' && player.padY > 0) {
            player.setArrowUp(true);
            player.setPadY(player.padY - PAD_SPEED);
        }
        if (data.keycode == 'ArrowDown' && player.padY < CANVAS_HEIGHT - player.padH) {
            player.setArrowDown(true);
            player.setPadY(player.padY + PAD_SPEED);
        }
        if (data.keycode == ' ' && !room.game.ballLaunched
            && !player.isLastScorer) {
            if (room.game.ballX < CANVAS_WIDTH / 2) {
                room.game.dirX = 1;
            } else {
                room.game.dirX = -1;
            }
            room.game.ballLaunched = true;
        }
    }

    @SubscribeMessage('initGame')
    async initGame(@MessageBody() data: { roomId: number }) {
        const room: Room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        if (!room) {
            return ;
        }
        room.players[0].setPadY((CANVAS_HEIGHT / 2) - (PAD_HEIGHT / 2)); // ((canvasRef.current!.height / 2) - (PAD_HEIGHT / 2))
        room.players[0].setPadH(PAD_HEIGHT); // ((canvasRef.current!.height / 2) - (PAD_HEIGHT / 2))
        room.players[1].setPadH(PAD_HEIGHT); // ((canvasRef.current!.height / 2) - (PAD_HEIGHT / 2))
        room.players[1].setPadX(CANVAS_WIDTH);                           // canvasRef.current!.width
        room.players[1].setPadY((CANVAS_HEIGHT / 2) - (PAD_HEIGHT / 2)); // ((canvasRef.current!.height / 2) - (PAD_HEIGHT / 2))
        room.game.powerUpX = Math.floor(Math.random() * (CANVAS_WIDTH - PU_MAX_SIZE));
        room.game.powerUpY = Math.floor(Math.random() * (CANVAS_HEIGHT - PU_MAX_SIZE));
        room.game.powerUpW = -10;
        if (room.players[0].isLastScorer) {
            room.game.ballX = CANVAS_WIDTH - PAD_WIDTH - BALL_RAD;
            room.game.ballY = (CANVAS_HEIGHT / 2);
        } else {
            room.game.ballX = PAD_WIDTH + BALL_RAD;
            room.game.ballY = (CANVAS_HEIGHT / 2);
        }
        room.game.isRunning = true;
        let roomId = data.roomId;
        this.gameloop(roomId);
    }

    moveBall(@MessageBody('roomId') roomId: number) {
        const room: Room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
            return ;
        }
        room.game.ballX = room.game.ballX + BALL_SPEED * room.game.dirX;
        room.game.ballY = room.game.ballY + BALL_SPEED * room.game.dirY;

        if (room.game.mode === "survival") {
            if (room.game.powerUpW < PU_MAX_SIZE) {
                room.game.powerUpW = room.game.powerUpW + 0.15;
            }
            if (room.game.ballX > room.game.powerUpX &&
                room.game.ballX < room.game.powerUpX + room.game.powerUpW &&
                room.game.ballY > room.game.powerUpY &&
                room.game.ballY < room.game.powerUpY + room.game.powerUpW) {
                if (room.game.dirX > 0) {
                    room.players[0].setPadH(room.players[0].padH + 20);
                } else {
                    room.players[1].setPadH(room.players[1].padH + 20);
                }
                room.game.powerUpX = Math.floor(Math.random() * (CANVAS_WIDTH - PU_MAX_SIZE));
                room.game.powerUpY = Math.floor(Math.random() * (CANVAS_HEIGHT - PU_MAX_SIZE));
                room.game.powerUpW = -10;
            }
        }
    }

    collisions(@MessageBody('roomId') roomId: number) {
        const room: Room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
            return ;
        }

        const ballX = room.game.ballX;
        const ballY = room.game.ballY;
        const P1PadX = room.players[0].padX;
        const P1PadY = room.players[0].padY;
        const P2PadX = room.players[1].padX;
        const P2PadY = room.players[1].padY;
        const P1PadH = room.players[0].padH;
        const P2PadH = room.players[1].padH;

        if ((ballY + BALL_RAD) >= P1PadY && (ballY - BALL_RAD) <= (P1PadY + P1PadH)
            && (ballX - BALL_RAD) <= P1PadX + PAD_WIDTH) {
            room.game.dirX = 1;
            const P1PadCenter = P1PadY + (P1PadH / 2)
            const distance = P1PadCenter - ballY;
            room.game.dirY = distance * 0.01;
            if (room.game.mode === "survival" && room.game.ballLaunched) {
                room.players[0].padH = room.players[0].padH - SURVIVAL_EROSION;
            }
        } else if ((ballY + BALL_RAD) >= P2PadY && (ballY - BALL_RAD) <= (P2PadY + P2PadH)
            && (ballX + BALL_RAD) >= P2PadX - PAD_WIDTH) {
            room.game.dirX = -1;
            const P2PadCenter = P2PadY + (P2PadH / 2)
            const distance = P2PadCenter - ballY;
            room.game.dirY = distance * 0.01;
            if (room.game.mode === "survival" && room.game.ballLaunched) {
                room.players[1].padH = room.players[1].padH - SURVIVAL_EROSION;
            }
        } else if (ballY <= 0) {
            room.game.dirY = 0.5;
        } else if (ballY >= CANVAS_HEIGHT) { // canvas.current.height ...
            room.game.dirY = - 0.5;
        }
    }

    async checkGoal(@MessageBody('roomId') roomId: number) {
        const room: Room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
             return ;
        }
        if (room.game.ballX < - BALL_RAD) {
            room.game.dirX = 0;
            room.game.dirY = 0;
            room.game.ballLaunched = false;
            room.players[1].isLastScorer = true;
            room.players[0].isLastScorer = false;
            room.game.score2 = room.game.score2 + 1;
            await this.initGame({roomId: roomId});
        } else if (room.game.ballX > CANVAS_WIDTH) { // canvasRef.current!.width
            room.game.dirX = 0;
            room.game.dirY = 0;
            room.game.ballLaunched = false;
            room.players[0].isLastScorer = true;
            room.players[1].isLastScorer = false;
            room.game.score1 = room.game.score1 + 1;
            await this.initGame({roomId: roomId});
        }
    }

    stickBall(@MessageBody('roomId') roomId: number) {
        const room: Room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
            return;
        }
        if (room.players[0].isLastScorer) {
            room.game.ballY = room.players[1].padY + (room.players[1].padH / 2);
        } else if (room.players[1].isLastScorer) {
            room.game.ballY = room.players[0].padY + (room.players[0].padH / 2);
        }
    }

    async updateUser(user: any, roomId: number, gameId: number, score: number, isPlayer: boolean) {
        const userDb = await this.usersService.findOneByUsername(user.username);
        const scoreTotal = userDb.max_score + score;
        const games = userDb.games;
        if (games.filter(game => game === gameId)[0]) {
            return ;
        }
        if (isPlayer) {
            games.push(gameId);
        }
        if (user.socketId)
            this.server.to(user.socketId).emit("updateUserAfterGame", {intra_id: user.intra_id, score: scoreTotal, games: games});
    }

    async updateUsers(room: any) {
        await this.updateUser(room.players[0], room.roomId, room.game.id, room.game.score1, true);
        await this.updateUser(room.players[1], room.roomId, room.game.id, room.game.score2, true);
        room.watchers.map(async watcher => {
            await this.updateUser(watcher, room.roomId, room.game.id, 0, false);
        })
    }

    async updateGameOver(game: any, room: any, disconnected: string) {
        if (!disconnected) {
            if (room.game.score1 > room.game.score2) {
                room.game.winnerId = +room.players[0].intra_id;
            } else {
                room.game.winnerId = +room.players[1].intra_id;
            }
        } else {
            if (disconnected === room.players[0].username) {
                room.game.winnerId = +room.players[1].intra_id;
            } else {
                room.game.winnerId = +room.players[0].intra_id;
            }
        }
        game.isActive = false;
        game.isRunning = false;
        game.ballLaunched = false;
        game.score1 = room.game.score1;
        game.score2 = room.game.score2;
        game.winnerId = room.game.winnerId;
        await this.gamesService.update(room.game.id, game);
    }

    async gameOver(roomId: number, disconnected: string) {
        const room: Room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room || !room.game.isActive) {
            return;
        }
        this.server.to(roomId.toString()).emit("stopGame");
        const game = await this.gamesService.findOne(room.game.id.toString());
        await this.updateGameOver(game, room, disconnected);
        await this.updateUsers(room);
        const indexP1 = this.players.findIndex(u => u.socketId == room.players[0].socketId);
        this.players.splice(indexP1, 1);
        const indexP2 = this.players.findIndex(u => u.socketId == room.players[1].socketId);
        this.players.splice(indexP2, 1);
        this.games.delete(room.roomId);
        const indexRoom = this.rooms.findIndex((r) => r.roomId == room.roomId);
        this.rooms.splice(indexRoom, 1);
    }

    @SubscribeMessage('gameloop')
    async gameloop(@MessageBody('roomId') roomId: number) {
        const room: Room = this.rooms.filter((r) => r.roomId == roomId)[0];
        if (room && room.game.ballLaunched) {
            this.moveBall(roomId);
            this.collisions(roomId);
            await this.checkGoal(roomId);
        } else {
            this.stickBall(roomId);
            if (room.game.score1 >= MAX_SCORE || room.game.score2 >= MAX_SCORE) {
                await this.gameOver(roomId, '');
            }
        }
        this.server.to(roomId.toString()).emit("setRoom", {room: room});
    }

    @SubscribeMessage('joinSpecFromProfile')
    joinSpecFromProfile(@ConnectedSocket() client: Socket,
             @MessageBody() data: {username: string}) {
        const player: Player = this.players.filter((p) => p.username === data.username)[0];
        if (player) {
            const room: Room = this.rooms.filter((r) => r.roomId === player.roomId)[0];
            if (room) {
                if (player.socketId) {
                    this.server.to(client.id).emit("joinSpecFromProfile", {gameId: room.game.id});
           	    }
	        }
        }
    }

    @SubscribeMessage('joinSpec')
    joinSpec(@ConnectedSocket() client: Socket,
                   @MessageBody() data: {user: any, gameId: number, fromProfile: boolean}) {
        const room: Room = this.rooms.filter((r) => r.game.id === data.gameId)[0];
        if (!room) {
            return;
        }
        client.join(room.roomId.toString());
        if (!room.watchers.filter(w => w.username === data.user.username)[0]) {
            const watcher = new Watcher(data.user.intra_id, data.user.username, client.id);
            room.watchers.push(watcher);
        }
       	this.server.to(client.id).emit("startGame", {room: room, status: "watching", fromProfile: data.fromProfile, gameId: data.gameId, intra_id: data.user.intra_id});
    }

    @SubscribeMessage('joinPrivateRoom')
    async joinPrivateRoom(@ConnectedSocket() client: Socket,
                          @MessageBody() data: {user: Player, roomId: number}) {
        let player = this.players.find(p  => p.intra_id === data.user.intra_id);
        if (player) {
            this.joinRoom(client, data.user);
            return ;
        }
        player = new Player(data.user.intra_id, data.user.username, 0, 0, client.id, data.user.avatar);
        const room: Room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        this.players.push(player);
        player.isLastScorer = true;
        room.addPlayer(player);
        player.setRoomId(room.roomId);
        await this.createGame(client, room);
    }

    @SubscribeMessage('startGameOnInvite')
    startGameOnInvite(@ConnectedSocket() client: Socket,
                   @MessageBody() data: {user: Player, target: Player}) {
        const player = new Player(data.user.intra_id, data.user.username, 0, 0, client.id, data.user.avatar);
        this.players.push(player);
        const room = this.createRoom(client, player);
        const targetSocketId = this.user2socket.get(+data.target.intra_id);
        if (targetSocketId) {
                this.server.to(targetSocketId).emit("invitedToPlay", {user: data.target, roomId: room.roomId});
        }
    }

    @SubscribeMessage('leave')
    leave(@ConnectedSocket() client: Socket,
               @MessageBody() data: {user: Player, roomId: number}) {
        const room: Room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        if (!room) {
            return ;
        }
        client.leave(room.roomId.toString());
        if (data.user.username === room.players[0].username) {
            this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", {leaver: data.user.username, winner: room.players[1].username});
        } else {
            this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", {leaver: data.user.username, winner: room.players[0].username});
        }
        this.gameOver(data.roomId, data.user.username);
    }

    @SubscribeMessage('setMode')
    async setMode(@ConnectedSocket() client: Socket,
            @MessageBody() data: {roomId: number, mode: string}) {
        const room: Room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        if (!room) {
            return ;
        }
        const game = await this.gamesService.findOne(room.game.id.toString());
        game.mode = data.mode;
        room.game.mode = data.mode;
        await this.gamesService.update(room.game.id, game);
        this.server.to(room.roomId.toString()).emit("setMode", {mode: data.mode, room: room});
        await this.initGame({roomId: room.roomId});
    }
}
