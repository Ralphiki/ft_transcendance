export class Game {
    id: number
    createdAt: Date
    isRunning: boolean
    isActive: boolean
    score1: number
    score2: number
    player1: string
    player2: string
    player1Id: number
    player2Id: number
    ballLaunched: boolean
    ballX: number
    ballY: number
    dirX: number
    dirY: number
    winnerId: number
    mode: string
    powerUpX: number
    powerUpY: number
    powerUpW: number
    p1Avatar: string
    p2Avatar: string
}
