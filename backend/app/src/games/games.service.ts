import { Injectable } from '@nestjs/common';
import { PrismaService } from "../prisma/prisma.service";
import { CreateGameDto } from "./dto/create-game.dto";
import { UpdateGameDto } from "./dto/update-game.dto";

@Injectable()
export class GamesService {
    constructor(private prisma: PrismaService) {}

    async create(createGameDto: CreateGameDto) {
        return this.prisma.game.create({data: createGameDto});
    }

    async findAll() {
        return this.prisma.game.findMany();
    }

    async findOne(id: string) {
        if (!id) {
            return;
        }
        return await this.prisma.game.findUnique({where: {id : parseInt(id)}})
    }

    async update(id: number, updateGameDto: UpdateGameDto) {
        return this.prisma.game.update(
            {
                where: {id: id},
                data: updateGameDto
            }
        )
    }

    async remove(id: string) {
        if (this.findOne(id)) {
            return this.prisma.game.delete({where: {id: parseInt(id)}})
        }
    }

    async active() {
        return this.prisma.game.findMany({
            where: {
                isActive: true
            }
        })
    }
}
