import { Module } from '@nestjs/common';
import { GamesService } from './games.service';
import { GamesController } from "./games.controller";
import { PrismaModule } from "../prisma/prisma.module";
import {GamesGateway} from "./games.gateway";
import {UsersService} from "../users/users.service";

@Module({
  providers: [GamesService, GamesGateway, UsersService],
  controllers: [GamesController],
  imports: [PrismaModule],
  exports: [GamesGateway]
})
export class GamesModule {}
