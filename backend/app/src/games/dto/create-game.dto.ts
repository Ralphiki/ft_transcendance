import { ApiProperty } from '@nestjs/swagger'
import { IsInt, IsString, IsBoolean } from "class-validator";

export class CreateGameDto {
    @IsInt()
    @ApiProperty()
    id: number

    @IsInt()
    @ApiProperty()
    score1: number

    @IsInt()
    @ApiProperty()
    score2: number

    @IsString()
    @ApiProperty()
    player1: string

    @IsString()
    @ApiProperty()
    player2: string

    @IsInt()
    @ApiProperty()
    player1Id: number

    @IsInt()
    @ApiProperty()
    player2Id: number

    @IsInt()
    @ApiProperty()
    winnerId: number

    @IsBoolean()
    @ApiProperty()
    isRunning: boolean

    @IsBoolean()
    @ApiProperty()
    isActive: boolean

    @IsBoolean()
    @ApiProperty()
    ballLaunched: boolean

    @IsInt()
    @ApiProperty()
    ballX: number

    @IsInt()
    @ApiProperty()
    ballY: number

    @IsInt()
    @ApiProperty()
    dirX: number

    @IsInt()
    @ApiProperty()
    dirY: number

    @IsString()
    @ApiProperty()
    mode: string

    @IsString()
    @ApiProperty()
    p1Avatar: string

    @IsString()
    @ApiProperty()
    p2Avatar: string
}
