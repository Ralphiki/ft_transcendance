import { PartialType } from '@nestjs/swagger';
import { ApiProperty} from '@nestjs/swagger'
import {CreateGameDto} from "./create-game.dto";

export class UpdateGameDto extends PartialType(CreateGameDto) {
    @ApiProperty()
    id: number;
    @ApiProperty()
    score1: number;
    @ApiProperty()
    score2: number;
    @ApiProperty()
    player2: string;
    @ApiProperty()
    player2Id: number;
    @ApiProperty()
    winnerId: number;
    @ApiProperty()
    isRunning: boolean;
    @ApiProperty()
    isActive: boolean;
    @ApiProperty()
    ballLaunched: boolean;
    @ApiProperty()
    ballX: number
    @ApiProperty()
    ballY: number
    @ApiProperty()
    dirX: number
    @ApiProperty()
    dirY: number
    @ApiProperty()
    mode: string
    @ApiProperty()
    p1Avatar: string
    @ApiProperty()
    p2Avatar: string
}
