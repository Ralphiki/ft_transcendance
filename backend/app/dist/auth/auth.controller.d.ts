import { Response, Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from "./auth.service";
import { UsersService } from "../users/users.service";
export declare class AuthController {
    private readonly authService;
    private readonly jwtService;
    private readonly usersService;
    constructor(authService: AuthService, jwtService: JwtService, usersService: UsersService);
    handleAuthentication(): void;
    handleRedirection(req: Request, res: Response): Promise<void>;
    loginByUsername(username: string, res: Response): Promise<void>;
    logout(res: Response, req: Request): Promise<void>;
}
