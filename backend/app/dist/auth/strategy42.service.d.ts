import { AuthService } from './auth.service';
declare const Strategy42_base: new (...args: any[]) => any;
export declare class Strategy42 extends Strategy42_base {
    private readonly authService;
    constructor(authService: AuthService);
    validate(accessToken: any, refreshToken: any, user: any): Promise<import(".prisma/client").Users>;
}
export {};
