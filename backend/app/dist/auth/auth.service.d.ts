import { UserData } from 'src/prisma/types';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
export declare class AuthService {
    private readonly usersService;
    private jwtService;
    constructor(usersService: UsersService, jwtService: JwtService);
    validateUser(data: UserData): Promise<import(".prisma/client").Users>;
    findUser(intratoken: number): Promise<import(".prisma/client").Users>;
    login(user: any): Promise<{
        access_token: string;
    }>;
    logout(user: any): Promise<import(".prisma/client").Users>;
}
