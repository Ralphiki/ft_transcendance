"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const users_service_1 = require("../users/users.service");
const create_user_dto_1 = require("../users/dto/create-user.dto");
const jwt_1 = require("@nestjs/jwt");
const common_1 = require("@nestjs/common");
let AuthService = class AuthService {
    constructor(usersService, jwtService) {
        this.usersService = usersService;
        this.jwtService = jwtService;
    }
    async validateUser(data) {
        console.log('AuthService');
        const user = await this.usersService.findOne(+data["id"]);
        if (user) {
            console.log("User in db");
            user["status"] = "online";
            return this.usersService.update(+user.intra_id, user);
        }
        const new_user = new create_user_dto_1.CreateUserDto;
        new_user["username"] = data["username"];
        new_user["avatar"] = data["_json"]["image"]["link"];
        new_user["intra_id"] = parseInt(data["id"]);
        const newUser = this.usersService.create(new_user);
        console.log('New User created');
        return newUser;
    }
    async findUser(intratoken) {
        const user = await this.usersService.findOne(intratoken);
        return user;
    }
    async login(user) {
        const payload = { id: user.uid, login42: user.intra_id };
        console.log(payload);
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
    async logout(user) {
        const toLogout = await this.usersService.findOne(+user.login42);
        toLogout["status"] = "offline";
        return this.usersService.update(+toLogout.intra_id, toLogout);
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [users_service_1.UsersService,
        jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map