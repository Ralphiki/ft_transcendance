"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateGameDto = void 0;
const swagger_1 = require("@nestjs/swagger");
const swagger_2 = require("@nestjs/swagger");
const create_game_dto_1 = require("./create-game.dto");
class UpdateGameDto extends (0, swagger_1.PartialType)(create_game_dto_1.CreateGameDto) {
}
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "id", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "score1", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "score2", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", String)
], UpdateGameDto.prototype, "player2", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "player2Id", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "winnerId", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Boolean)
], UpdateGameDto.prototype, "isRunning", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Boolean)
], UpdateGameDto.prototype, "isActive", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Boolean)
], UpdateGameDto.prototype, "ballLaunched", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "ballX", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "ballY", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "dirX", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", Number)
], UpdateGameDto.prototype, "dirY", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", String)
], UpdateGameDto.prototype, "mode", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", String)
], UpdateGameDto.prototype, "p1Avatar", void 0);
__decorate([
    (0, swagger_2.ApiProperty)(),
    __metadata("design:type", String)
], UpdateGameDto.prototype, "p2Avatar", void 0);
exports.UpdateGameDto = UpdateGameDto;
//# sourceMappingURL=update-game.dto.js.map