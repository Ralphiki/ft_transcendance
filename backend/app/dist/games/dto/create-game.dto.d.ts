export declare class CreateGameDto {
    id: number;
    score1: number;
    score2: number;
    player1: string;
    player2: string;
    player1Id: number;
    player2Id: number;
    winnerId: number;
    isRunning: boolean;
    isActive: boolean;
    ballLaunched: boolean;
    ballX: number;
    ballY: number;
    dirX: number;
    dirY: number;
    mode: string;
    p1Avatar: string;
    p2Avatar: string;
}
