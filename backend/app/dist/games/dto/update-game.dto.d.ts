import { CreateGameDto } from "./create-game.dto";
declare const UpdateGameDto_base: import("@nestjs/common").Type<Partial<CreateGameDto>>;
export declare class UpdateGameDto extends UpdateGameDto_base {
    id: number;
    score1: number;
    score2: number;
    player2: string;
    player2Id: number;
    winnerId: number;
    isRunning: boolean;
    isActive: boolean;
    ballLaunched: boolean;
    ballX: number;
    ballY: number;
    dirX: number;
    dirY: number;
    mode: string;
    p1Avatar: string;
    p2Avatar: string;
}
export {};
