import { OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from "@nestjs/websockets";
import { GamesService } from "./games.service";
import { Socket, Server } from "socket.io";
import { Player } from "./classes/Players";
import { Room } from "./classes/Room";
import { UsersService } from "../users/users.service";
export declare class GamesGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    private readonly gamesService;
    private readonly usersService;
    constructor(gamesService: GamesService, usersService: UsersService);
    private logger;
    private readonly rooms;
    private readonly players;
    private roomId;
    private games;
    private user2socket;
    getKeyByValue(map: any, searchValue: any): any;
    server: Server;
    afterInit(server: Server): void;
    handleConnection(client: Socket): Promise<void>;
    handleDisconnect(client: Socket): Promise<void>;
    onStartAppConnection(client: Socket, user: Player): void;
    createRoom(client: Socket, player: Player): Room;
    createGame(client: Socket, room: Room): Promise<void>;
    joinRoom(client: Socket, user: Player): Promise<void>;
    deleteRoom(client: Socket): Promise<void>;
    onKeyDown(client: Socket, data: {
        roomId: number;
        keycode: string;
        userId: string;
    }): void;
    initGame(data: {
        roomId: number;
    }): Promise<void>;
    moveBall(roomId: number): void;
    collisions(roomId: number): void;
    checkGoal(roomId: number): Promise<void>;
    stickBall(roomId: number): void;
    updateUser(user: any, roomId: number, gameId: number, score: number, isPlayer: boolean): Promise<void>;
    updateUsers(room: any): Promise<void>;
    updateGameOver(game: any, room: any, disconnected: string): Promise<void>;
    gameOver(roomId: number, disconnected: string): Promise<void>;
    gameloop(roomId: number): Promise<void>;
    joinSpecFromProfile(client: Socket, data: {
        username: string;
    }): void;
    joinSpec(client: Socket, data: {
        user: any;
        gameId: number;
        fromProfile: boolean;
    }): void;
    joinPrivateRoom(client: Socket, data: {
        user: Player;
        roomId: number;
    }): Promise<void>;
    startGameOnInvite(client: Socket, data: {
        user: Player;
        target: Player;
    }): void;
    leave(client: Socket, data: {
        user: Player;
        roomId: number;
    }): void;
    setMode(client: Socket, data: {
        roomId: number;
        mode: string;
    }): Promise<void>;
}
