export declare class Watcher {
    intra_id: string;
    username: string;
    socketId?: string;
    roomId?: number;
    status: string;
    constructor(intra_id: string, username: string, socketId?: string);
}
