"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Player = void 0;
class Player {
    constructor(intra_id, username, padX, padY, socketId, avatar) {
        this.intra_id = intra_id;
        this.username = username;
        this.socketId = socketId;
        this.arrowUp = false;
        this.arrowDown = false;
        this.isLastScorer = false;
        this.padX = padX;
        this.padY = padY;
        this.avatar = avatar;
    }
    setRoomId(roomId) {
        this.roomId = roomId;
    }
    setArrowUp(arrowUp) {
        this.arrowUp = arrowUp;
    }
    setArrowDown(arrowDown) {
        this.arrowDown = arrowDown;
    }
    setPadX(padX) {
        this.padX = padX;
    }
    setPadY(padY) {
        this.padY = padY;
    }
    setPadH(padH) {
        this.padH = padH;
    }
}
exports.Player = Player;
//# sourceMappingURL=Players.js.map