export declare class Player {
    intra_id: string;
    username: string;
    socketId?: string;
    roomId?: number;
    arrowUp: boolean;
    arrowDown: boolean;
    padX: number;
    padY: number;
    padH: number;
    isLastScorer: boolean;
    status: string;
    avatar: string;
    constructor(intra_id: string, username: string, padX: number, padY: number, socketId: string, avatar: string);
    setRoomId(roomId: number | undefined): void;
    setArrowUp(arrowUp: boolean): void;
    setArrowDown(arrowDown: boolean): void;
    setPadX(padX: number): void;
    setPadY(padY: number): void;
    setPadH(padH: number): void;
}
