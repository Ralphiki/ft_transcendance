import { Player } from "./Players";
import { Game } from "../entities/game.entity";
import { Watcher } from "./Watchers";
export declare class Room {
    roomId: number;
    players: Player[];
    watchers: Watcher[];
    game: Game;
    constructor(roomId: number, players: Player[], watchers: Watcher[], game: Game);
    addPlayer(player: Player): void;
    setGame(game: any): void;
}
