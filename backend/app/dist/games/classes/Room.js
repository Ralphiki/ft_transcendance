"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Room = void 0;
class Room {
    constructor(roomId, players, watchers, game) {
        this.roomId = roomId;
        this.players = players;
        this.watchers = watchers;
        this.game = game;
    }
    addPlayer(player) {
        this.players.push(player);
    }
    setGame(game) {
        this.game = game;
    }
}
exports.Room = Room;
//# sourceMappingURL=Room.js.map