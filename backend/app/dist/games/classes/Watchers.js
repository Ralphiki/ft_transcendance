"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Watcher = void 0;
class Watcher {
    constructor(intra_id, username, socketId) {
        this.intra_id = intra_id;
        this.username = username;
        this.socketId = socketId;
    }
}
exports.Watcher = Watcher;
//# sourceMappingURL=Watchers.js.map