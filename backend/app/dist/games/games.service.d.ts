import { PrismaService } from "../prisma/prisma.service";
import { CreateGameDto } from "./dto/create-game.dto";
import { UpdateGameDto } from "./dto/update-game.dto";
export declare class GamesService {
    private prisma;
    constructor(prisma: PrismaService);
    create(createGameDto: CreateGameDto): Promise<import(".prisma/client").Game>;
    findAll(): Promise<import(".prisma/client").Game[]>;
    findOne(id: string): Promise<import(".prisma/client").Game>;
    update(id: number, updateGameDto: UpdateGameDto): Promise<import(".prisma/client").Game>;
    remove(id: string): Promise<import(".prisma/client").Game>;
    active(): Promise<import(".prisma/client").Game[]>;
}
