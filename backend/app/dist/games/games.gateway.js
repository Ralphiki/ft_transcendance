"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GamesGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const games_service_1 = require("./games.service");
const socket_io_1 = require("socket.io");
const common_1 = require("@nestjs/common");
const Players_1 = require("./classes/Players");
const Room_1 = require("./classes/Room");
const users_service_1 = require("../users/users.service");
const create_game_dto_1 = require("./dto/create-game.dto");
const Watchers_1 = require("./classes/Watchers");
const CANVAS_WIDTH = 1080;
const CANVAS_HEIGHT = 600;
const PAD_WIDTH = 15;
const PAD_HEIGHT = 120;
const PAD_SPEED = 16;
const BALL_SPEED = 10;
const BALL_RAD = 8;
const SURVIVAL_EROSION = 10;
const PU_MAX_SIZE = 150;
const MAX_SCORE = 3;
let GamesGateway = class GamesGateway {
    constructor(gamesService, usersService) {
        this.gamesService = gamesService;
        this.usersService = usersService;
        this.logger = new common_1.Logger('gamesGateway');
        this.rooms = [];
        this.players = [];
        this.roomId = 0;
        this.games = new Map();
        this.user2socket = new Map();
    }
    getKeyByValue(map, searchValue) {
        for (let [key, value] of map.entries()) {
            if (value === searchValue)
                return key;
        }
    }
    afterInit(server) {
        this.logger.log(`Init Pong Gateway`);
    }
    async handleConnection(client) {
        this.logger.log(`Client connected: ${client.id}`);
    }
    async handleDisconnect(client) {
        this.logger.log(`Client disconnected: ${client.id}`);
        const playerIntraId = this.getKeyByValue(this.user2socket, client.id);
        console.log(playerIntraId);
        if (playerIntraId) {
            const player = this.players.filter(p => p.intra_id === playerIntraId)[0];
            console.log(player);
            if (player) {
                const room = this.rooms.filter(r => r.roomId === player.roomId)[0];
                console.log(room);
                if (room && room.game !== null && room.game.isActive) {
                    console.log("user disconnect... emit to room");
                    console.log(room.roomId.toString());
                    if (player.username === room.players[0].username) {
                        this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", { leaver: player.username, winner: room.players[1].username });
                    }
                    else {
                        this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", { leaver: player.username, winner: room.players[0].username });
                    }
                    client.leave(room.roomId.toString());
                    this.gameOver(room.roomId, player.username);
                }
            }
        }
    }
    onStartAppConnection(client, user) {
        if (user.hasOwnProperty('intra_id')) {
            console.log(user.username + " connected on app with gamesocket " + client.id);
            this.user2socket.set(+user.intra_id, client.id);
            const player = this.players.find(e => e.intra_id == user.intra_id);
            if (player) {
                player.socketId = client.id;
            }
        }
    }
    createRoom(client, player) {
        const room = new Room_1.Room(this.roomId, [player], [], null);
        player.setRoomId(room.roomId);
        this.roomId++;
        console.log("creating Room...");
        this.rooms.push(room);
        client.join(room.roomId.toString());
        console.log(this.rooms);
        return room;
    }
    async createGame(client, room) {
        const new_game = new create_game_dto_1.CreateGameDto;
        new_game["player1"] = room.players[0].username;
        new_game["player1Id"] = +room.players[0].intra_id;
        new_game["player2"] = room.players[1].username;
        new_game["player2Id"] = +room.players[1].intra_id;
        new_game["p1Avatar"] = room.players[0].avatar;
        new_game["p2Avatar"] = room.players[1].avatar;
        const game = await this.gamesService.create(new_game);
        client.join(room.roomId.toString());
        room.setGame(game);
        this.games.set(room.roomId, game);
        console.log(game);
        console.log(room);
        this.server.to(room.roomId.toString()).emit("startGame", { room: room, status: "playing" });
    }
    async joinRoom(client, user) {
        console.log("joining room...");
        console.log("user is " + user.status);
        let player = this.players.find(e => e.intra_id == user.intra_id);
        if (player) {
            const room = this.rooms.filter((r) => r.roomId == player.roomId)[0];
            if (room) {
                console.log(user.username + " is already in a Room...");
                console.log("socket id " + client.id);
                console.log("player socket id " + player.socketId);
                client.join(room.roomId.toString());
                if (player.socketId)
                    this.server.to(player.socketId).emit("startGame", { room: room, status: "playing" });
            }
            else {
                console.log(user.username + "'s room doesn't exist anymore...");
                const indexPlayer = this.players.findIndex(u => u.socketId == client.id);
                if (player.socketId)
                    this.server.to(player.socketId).emit("updateStatus", { status: "online" });
                this.players.splice(indexPlayer, 1);
            }
            return;
        }
        else if (!player && user.status === "playing") {
            console.log(user.username + "'s room doesn't exist anymore...");
            if (client.id)
                this.server.to(client.id).emit("updateStatus", { status: "online" });
            return;
        }
        player = new Players_1.Player(user.intra_id, user.username, 0, 0, client.id, user.avatar);
        this.players.push(player);
        let room = this.rooms.filter((r) => r.players.length != 2)[0];
        if (room) {
            player.isLastScorer = true;
            room.addPlayer(player);
            player.setRoomId(room.roomId);
            if (room.players.length === 2 && this.games.has(room.roomId) === false) {
                await this.createGame(client, room);
            }
        }
        else {
            room = this.createRoom(client, player);
        }
        this.logger.log(user.username + " joined the room n°" + room.roomId);
        console.log(room);
        if (player.socketId)
            this.server.to(player.socketId).emit("setClientRoom", { intra_id: user.intra_id, room: room });
    }
    async deleteRoom(client) {
        var _a;
        console.log("deleting Room...");
        let user = this.players.find(u => u.socketId == client.id);
        if (user) {
            client.leave((_a = user.roomId) === null || _a === void 0 ? void 0 : _a.toString());
            let inPlayers = this.players.findIndex(u => u.socketId == client.id);
            if (inPlayers !== -1) {
                this.players.splice(inPlayers, 1);
            }
            let inRoom = this.rooms.findIndex(r => r.roomId == user.roomId);
            if (inRoom !== -1) {
                const gameObj = this.games.get(user.roomId);
                const id = gameObj === null || gameObj === void 0 ? void 0 : gameObj.id.toString();
                const gameOver = await this.gamesService.findOne(id);
                if (gameOver !== undefined && gameOver["isActive"]) {
                    console.log("updating game...");
                    gameOver["isActive"] = false;
                    gameOver["isRunning"] = false;
                    if (+user.intra_id == gameOver.player1Id) {
                        gameOver["winnerId"] = gameOver.player2Id;
                    }
                    else {
                        gameOver["winnerId"] = gameOver.player1Id;
                    }
                    await this.gamesService.update(+gameOver.id, gameOver);
                }
                this.rooms.splice(inRoom, 1);
                this.games.delete(user.roomId);
                console.log(this.rooms);
            }
        }
    }
    onKeyDown(client, data) {
        const room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        if (!room) {
            return;
        }
        const player = room.players.filter((p) => p.intra_id === data.userId)[0];
        console.log("before down player : " + player.arrowUp + player.arrowDown);
        if (data.keycode == 'ArrowUp' && player.padY > 0) {
            console.log("arrow up !");
            player.setArrowUp(true);
            player.setPadY(player.padY - PAD_SPEED);
        }
        if (data.keycode == 'ArrowDown' && player.padY < CANVAS_HEIGHT - player.padH) {
            console.log("arrow down !");
            player.setArrowDown(true);
            player.setPadY(player.padY + PAD_SPEED);
        }
        if (data.keycode == ' ' && !room.game.ballLaunched
            && !player.isLastScorer) {
            if (room.game.ballX < CANVAS_WIDTH / 2) {
                room.game.dirX = 1;
            }
            else {
                room.game.dirX = -1;
            }
            room.game.ballLaunched = true;
        }
    }
    async initGame(data) {
        console.log(data.roomId);
        const room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        console.log("init game !");
        if (!room) {
            console.log("couldn't find room in initGame");
            return;
        }
        room.players[0].setPadY((CANVAS_HEIGHT / 2) - (PAD_HEIGHT / 2));
        room.players[0].setPadH(PAD_HEIGHT);
        room.players[1].setPadH(PAD_HEIGHT);
        room.players[1].setPadX(CANVAS_WIDTH);
        room.players[1].setPadY((CANVAS_HEIGHT / 2) - (PAD_HEIGHT / 2));
        room.game.powerUpX = Math.floor(Math.random() * (CANVAS_WIDTH - PU_MAX_SIZE));
        room.game.powerUpY = Math.floor(Math.random() * (CANVAS_HEIGHT - PU_MAX_SIZE));
        room.game.powerUpW = -10;
        if (room.players[0].isLastScorer) {
            room.game.ballX = CANVAS_WIDTH - PAD_WIDTH - BALL_RAD;
            room.game.ballY = (CANVAS_HEIGHT / 2);
        }
        else {
            room.game.ballX = PAD_WIDTH + BALL_RAD;
            room.game.ballY = (CANVAS_HEIGHT / 2);
        }
        room.game.isRunning = true;
        let roomId = data.roomId;
        this.gameloop(roomId);
    }
    moveBall(roomId) {
        const room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
            console.log("couldn't find room in moveBall");
            return;
        }
        room.game.ballX = room.game.ballX + BALL_SPEED * room.game.dirX;
        room.game.ballY = room.game.ballY + BALL_SPEED * room.game.dirY;
        if (room.game.mode === "survival") {
            if (room.game.powerUpW < PU_MAX_SIZE) {
                room.game.powerUpW = room.game.powerUpW + 0.15;
            }
            if (room.game.ballX > room.game.powerUpX &&
                room.game.ballX < room.game.powerUpX + room.game.powerUpW &&
                room.game.ballY > room.game.powerUpY &&
                room.game.ballY < room.game.powerUpY + room.game.powerUpW) {
                if (room.game.dirX > 0) {
                    room.players[0].setPadH(room.players[0].padH + 20);
                }
                else {
                    room.players[1].setPadH(room.players[1].padH + 20);
                }
                room.game.powerUpX = Math.floor(Math.random() * (CANVAS_WIDTH - PU_MAX_SIZE));
                room.game.powerUpY = Math.floor(Math.random() * (CANVAS_HEIGHT - PU_MAX_SIZE));
                room.game.powerUpW = -10;
            }
        }
    }
    collisions(roomId) {
        const room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
            console.log("couldn't find room in collisions");
            return;
        }
        const ballX = room.game.ballX;
        const ballY = room.game.ballY;
        const P1PadX = room.players[0].padX;
        const P1PadY = room.players[0].padY;
        const P2PadX = room.players[1].padX;
        const P2PadY = room.players[1].padY;
        const P1PadH = room.players[0].padH;
        const P2PadH = room.players[1].padH;
        if ((ballY + BALL_RAD) >= P1PadY && (ballY - BALL_RAD) <= (P1PadY + P1PadH)
            && (ballX - BALL_RAD) <= P1PadX + PAD_WIDTH) {
            room.game.dirX = 1;
            const P1PadCenter = P1PadY + (P1PadH / 2);
            const distance = P1PadCenter - ballY;
            room.game.dirY = distance * 0.01;
            if (room.game.mode === "survival" && room.game.ballLaunched) {
                room.players[0].padH = room.players[0].padH - SURVIVAL_EROSION;
            }
        }
        else if ((ballY + BALL_RAD) >= P2PadY && (ballY - BALL_RAD) <= (P2PadY + P2PadH)
            && (ballX + BALL_RAD) >= P2PadX - PAD_WIDTH) {
            room.game.dirX = -1;
            const P2PadCenter = P2PadY + (P2PadH / 2);
            const distance = P2PadCenter - ballY;
            room.game.dirY = distance * 0.01;
            if (room.game.mode === "survival" && room.game.ballLaunched) {
                room.players[1].padH = room.players[1].padH - SURVIVAL_EROSION;
            }
        }
        else if (ballY <= 0) {
            room.game.dirY = 0.5;
        }
        else if (ballY >= CANVAS_HEIGHT) {
            room.game.dirY = -0.5;
        }
    }
    async checkGoal(roomId) {
        const room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
            console.log("couldn't find room in checkGoal");
            return;
        }
        if (room.game.ballX < -BALL_RAD) {
            room.game.dirX = 0;
            room.game.dirY = 0;
            room.game.ballLaunched = false;
            room.players[1].isLastScorer = true;
            room.players[0].isLastScorer = false;
            room.game.score2 = room.game.score2 + 1;
            await this.initGame({ roomId: roomId });
        }
        else if (room.game.ballX > CANVAS_WIDTH) {
            room.game.dirX = 0;
            room.game.dirY = 0;
            room.game.ballLaunched = false;
            room.players[0].isLastScorer = true;
            room.players[1].isLastScorer = false;
            room.game.score1 = room.game.score1 + 1;
            await this.initGame({ roomId: roomId });
        }
    }
    stickBall(roomId) {
        const room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room) {
            console.log("couldn't find room in stickBall");
            return;
        }
        if (room.players[0].isLastScorer) {
            room.game.ballY = room.players[1].padY + (room.players[1].padH / 2);
        }
        else if (room.players[1].isLastScorer) {
            room.game.ballY = room.players[0].padY + (room.players[0].padH / 2);
        }
    }
    async updateUser(user, roomId, gameId, score, isPlayer) {
        console.log("updating user " + user.username);
        console.log(gameId);
        const userDb = await this.usersService.findOneByUsername(user.username);
        const scoreTotal = userDb.max_score + score;
        const games = userDb.games;
        if (games.filter(game => game === gameId)[0]) {
            return;
        }
        if (isPlayer) {
            games.push(gameId);
        }
        if (user.socketId)
            this.server.to(user.socketId).emit("updateUserAfterGame", { intra_id: user.intra_id, score: scoreTotal, games: games });
    }
    async updateUsers(room) {
        await this.updateUser(room.players[0], room.roomId, room.game.id, room.game.score1, true);
        await this.updateUser(room.players[1], room.roomId, room.game.id, room.game.score2, true);
        room.watchers.map(async (watcher) => {
            console.log("ICIIIIIIIII le watcheeeeeeeeer " + watcher);
            await this.updateUser(watcher, room.roomId, room.game.id, 0, false);
        });
    }
    async updateGameOver(game, room, disconnected) {
        if (!disconnected) {
            if (room.game.score1 > room.game.score2) {
                room.game.winnerId = +room.players[0].intra_id;
            }
            else {
                room.game.winnerId = +room.players[1].intra_id;
            }
        }
        else {
            if (disconnected === room.players[0].username) {
                room.game.winnerId = +room.players[1].intra_id;
            }
            else {
                room.game.winnerId = +room.players[0].intra_id;
            }
        }
        game.isActive = false;
        game.isRunning = false;
        game.ballLaunched = false;
        game.score1 = room.game.score1;
        game.score2 = room.game.score2;
        game.winnerId = room.game.winnerId;
        await this.gamesService.update(room.game.id, game);
    }
    async gameOver(roomId, disconnected) {
        console.log("game over !");
        const room = this.rooms.filter((r) => r.roomId === roomId)[0];
        if (!room || !room.game.isActive) {
            console.log("couldn't find room in gameOver");
            return;
        }
        this.server.to(roomId.toString()).emit("stopGame");
        const game = await this.gamesService.findOne(room.game.id.toString());
        await this.updateGameOver(game, room, disconnected);
        await this.updateUsers(room);
        const indexP1 = this.players.findIndex(u => u.socketId == room.players[0].socketId);
        this.players.splice(indexP1, 1);
        const indexP2 = this.players.findIndex(u => u.socketId == room.players[1].socketId);
        this.players.splice(indexP2, 1);
        this.games.delete(room.roomId);
        const indexRoom = this.rooms.findIndex((r) => r.roomId == room.roomId);
        this.rooms.splice(indexRoom, 1);
        console.log(this.players);
    }
    async gameloop(roomId) {
        const room = this.rooms.filter((r) => r.roomId == roomId)[0];
        if (room && room.game.ballLaunched) {
            this.moveBall(roomId);
            this.collisions(roomId);
            await this.checkGoal(roomId);
        }
        else {
            this.stickBall(roomId);
            if (room.game.score1 >= MAX_SCORE || room.game.score2 >= MAX_SCORE) {
                await this.gameOver(roomId, '');
            }
        }
        this.server.to(roomId.toString()).emit("setRoom", { room: room });
    }
    joinSpecFromProfile(client, data) {
        console.log("join spec from profile");
        console.log(data.username);
        console.log(this.players);
        const player = this.players.filter((p) => p.username === data.username)[0];
        console.log(player);
        if (player) {
            const room = this.rooms.filter((r) => r.roomId === player.roomId)[0];
            console.log(room);
            if (room) {
                if (player.socketId) {
                    console.log("emit toooooooooooooooo " + player.socketId);
                    this.server.to(client.id).emit("joinSpecFromProfile", { gameId: room.game.id });
                }
            }
        }
    }
    joinSpec(client, data) {
        console.log(" join spec !");
        console.log(data);
        const room = this.rooms.filter((r) => r.game.id === data.gameId)[0];
        if (!room) {
            console.log("couldn't find room in joinSpec");
            return;
        }
        client.join(room.roomId.toString());
        if (!room.watchers.filter(w => w.username === data.user.username)[0]) {
            const watcher = new Watchers_1.Watcher(data.user.intra_id, data.user.username, client.id);
            room.watchers.push(watcher);
            console.log(watcher);
        }
        console.log(room.watchers);
        console.log(client.id);
        this.server.to(client.id).emit("startGame", { room: room, status: "watching", fromProfile: data.fromProfile, gameId: data.gameId });
    }
    async joinPrivateRoom(client, data) {
        console.log("joining private room...");
        const player = new Players_1.Player(data.user.intra_id, data.user.username, 0, 0, client.id, data.user.avatar);
        const room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        this.players.push(player);
        player.isLastScorer = true;
        room.addPlayer(player);
        player.setRoomId(room.roomId);
        await this.createGame(client, room);
    }
    startGameOnInvite(client, data) {
        console.log("start game on invite !");
        const player = new Players_1.Player(data.user.intra_id, data.user.username, 0, 0, client.id, data.user.avatar);
        this.players.push(player);
        const room = this.createRoom(client, player);
        const targetSocketId = this.user2socket.get(+data.target.intra_id);
        console.log(this.user2socket);
        console.log(data.target.intra_id, typeof data.target.intra_id);
        console.log(targetSocketId);
        if (targetSocketId) {
            console.log("emiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiit");
            this.server.to(targetSocketId).emit("invitedToPlay", { user: data.target, roomId: room.roomId });
        }
    }
    leave(client, data) {
        console.log("leaving room..........");
        const room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        client.leave(room.roomId.toString());
        if (data.user.username === room.players[0].username) {
            this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", { leaver: data.user.username, winner: room.players[1].username });
        }
        else {
            this.server.to(room.roomId.toString()).emit("unexpectedDisconnection", { leaver: data.user.username, winner: room.players[0].username });
        }
        this.gameOver(data.roomId, data.user.username);
    }
    async setMode(client, data) {
        console.log("setting mode...");
        const room = this.rooms.filter((r) => r.roomId === data.roomId)[0];
        const game = await this.gamesService.findOne(room.game.id.toString());
        game.mode = data.mode;
        room.game.mode = data.mode;
        await this.gamesService.update(room.game.id, game);
        console.log(this.games);
        console.log("----------------------------------------");
        console.log(room);
        this.server.to(room.roomId.toString()).emit("setMode", { mode: data.mode, room: room });
        await this.initGame({ roomId: room.roomId });
    }
};
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", socket_io_1.Server)
], GamesGateway.prototype, "server", void 0);
__decorate([
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "handleConnection", null);
__decorate([
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "handleDisconnect", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('onStartAppConnection'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket,
        Players_1.Player]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "onStartAppConnection", null);
__decorate([
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket,
        Players_1.Player]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "createRoom", null);
__decorate([
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket,
        Room_1.Room]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "createGame", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('joinRoom'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket,
        Players_1.Player]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "joinRoom", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('deleteRoom'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "deleteRoom", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('onKeyDown'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "onKeyDown", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('initGame'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "initGame", null);
__decorate([
    __param(0, (0, websockets_1.MessageBody)('roomId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "moveBall", null);
__decorate([
    __param(0, (0, websockets_1.MessageBody)('roomId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "collisions", null);
__decorate([
    __param(0, (0, websockets_1.MessageBody)('roomId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "checkGoal", null);
__decorate([
    __param(0, (0, websockets_1.MessageBody)('roomId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "stickBall", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('gameloop'),
    __param(0, (0, websockets_1.MessageBody)('roomId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "gameloop", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('joinSpecFromProfile'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "joinSpecFromProfile", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('joinSpec'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "joinSpec", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('joinPrivateRoom'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "joinPrivateRoom", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('startGameOnInvite'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "startGameOnInvite", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('leave'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], GamesGateway.prototype, "leave", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('setMode'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", Promise)
], GamesGateway.prototype, "setMode", null);
GamesGateway = __decorate([
    (0, websockets_1.WebSocketGateway)({ cors: true, transports: ['websocket'] }),
    __metadata("design:paramtypes", [games_service_1.GamesService,
        users_service_1.UsersService])
], GamesGateway);
exports.GamesGateway = GamesGateway;
//# sourceMappingURL=games.gateway.js.map