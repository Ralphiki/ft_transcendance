import { GamesService } from './games.service';
import { Game } from './entities/game.entity';
import { CreateGameDto } from "./dto/create-game.dto";
import { UpdateGameDto } from "./dto/update-game.dto";
export declare class GamesController {
    private readonly gamesService;
    constructor(gamesService: GamesService);
    create(createGameDto: CreateGameDto): Promise<Game>;
    findAll(): Promise<Game[]>;
    findOne(id: string): Promise<Game>;
    update(id: string, updateGameDto: UpdateGameDto): Promise<void>;
    remove(id: string): Promise<void>;
    active(): Promise<Game[]>;
}
