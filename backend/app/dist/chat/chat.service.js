"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const users_service_1 = require("../users/users.service");
const bcrypt = require("bcrypt");
async function encrypt(password) {
    const salt = 10;
    const hash = await bcrypt.hash(password, salt);
    return hash;
}
async function decrypte(password, hash) {
    const ret = await bcrypt.compare(password, hash);
    return ret;
}
let ChatService = class ChatService {
    constructor(prisma, usersService) {
        this.prisma = prisma;
        this.usersService = usersService;
    }
    async decrypt(password, room_uid) {
        const room = await this.findOneByRoomId(room_uid);
        return await decrypte(password, room.password);
    }
    async create(chat) {
        if (!chat.room_uid) {
            let new_id = await this.prisma.room.count();
            chat.name = "channel#" + new_id;
            chat.room_uid = chat.name;
        }
        if (chat.password !== undefined && chat.password.length) {
            const secret = "ok";
            let password = await encrypt(chat.password);
            const new_chat = await this.prisma.room.create({
                data: {
                    owner: chat.owner,
                    name: chat.name,
                    typechat: chat.typechat,
                    admin: chat.admin,
                    password: password,
                    room_uid: chat.room_uid,
                    members: chat.members,
                    message: [],
                }
            });
            return new_chat;
        }
        else {
            const new_chat = await this.prisma.room.create({
                data: {
                    owner: chat.owner,
                    name: chat.name,
                    typechat: chat.typechat,
                    admin: chat.admin,
                    password: "null",
                    room_uid: chat.room_uid,
                    members: chat.members,
                    message: [],
                }
            });
            return new_chat;
        }
    }
    findAll() {
        return this.prisma.room.findMany();
    }
    async findAllMessages(room_id) {
        var room = await this.findOneByRoomId(room_id);
        if (room === null)
            return [];
        return room.message;
    }
    async findOne(id) {
        return await this.prisma.room.findFirst({
            where: {
                id: +id,
            }
        });
    }
    async findOneByRoomId(room_id) {
        return await this.prisma.room.findFirst({
            where: {
                room_uid: room_id,
            }
        });
    }
    async findMembersByRoomId(id) {
        const room = await this.prisma.room.findFirst({
            where: {
                id: +id
            }
        });
        let members = [];
        await Promise.all(room["members"].map(async (m_id) => {
            let member = await this.usersService.findOne(+m_id);
            await members.push(member);
        }));
        return members;
    }
    async updateChannelChat(id, intra_id, action) {
        const room = await this.prisma.room.findUnique({
            where: {
                id: +id
            },
        });
        if (action == "addadmin") {
            if (!room["admin"].includes(intra_id)) {
                return await this.prisma.room.update({
                    where: {
                        id: +id,
                    },
                    data: {
                        admin: {
                            push: intra_id
                        }
                    }
                });
            }
        }
        else if (action == "deleteadmin") {
            const find = room.admin.indexOf(intra_id);
            room.admin.splice(find, 1);
            return await this.prisma.room.update({
                where: { id: +id },
                data: {
                    admin: {
                        set: room.admin.filter((id) => id !== intra_id),
                    }
                },
            });
        }
        else if (action == "addbanned") {
            if (!room["banned"].includes(intra_id)) {
                return await this.prisma.room.update({
                    where: {
                        id: +id,
                    },
                    data: {
                        banned: {
                            push: intra_id
                        }
                    }
                });
            }
        }
        else if (action == "deletebanned") {
            const find = room.banned.indexOf(intra_id);
            room.banned.splice(find, 1);
            return await this.prisma.room.update({
                where: { id: +id },
                data: {
                    banned: {
                        set: room.banned.filter((id) => id !== intra_id),
                    }
                },
            });
        }
        else if (action == "addmembers") {
            if (!room["members"].includes(intra_id)) {
                return this.prisma.room.update({
                    where: {
                        id: +id,
                    },
                    data: {
                        members: {
                            push: intra_id
                        }
                    }
                });
            }
        }
        else if (action == "deletemembers") {
            const find = room.members.indexOf(intra_id);
            room.members.splice(find, 1);
            return await this.prisma.room.update({
                where: { id: +id },
                data: {
                    members: {
                        set: room.members.filter((id) => id !== intra_id),
                    }
                },
            });
        }
        else if (action == "mute") {
            if (!room["muted"].includes(intra_id)) {
                return await this.prisma.room.update({
                    where: { id: +id },
                    data: {
                        muted: {
                            push: intra_id,
                        }
                    },
                });
            }
        }
        else if (action == "unmute") {
            if (room["muted"].includes(intra_id)) {
                return await this.prisma.room.update({
                    where: { id: +id },
                    data: {
                        muted: {
                            set: room.muted.filter((id) => id !== intra_id),
                        }
                    },
                });
            }
        }
        return room;
    }
    remove(room_id) {
        return `This action removes a #${room_id} chat`;
    }
    async message(data) {
        var json = {
            message: data.message_data,
            user_id: data.user_id,
            user_name: data.user_name,
            avatar: data.user_avatar,
            sentAt: data.sentAt
        };
        const r = await this.prisma.room.update({
            where: {
                room_uid: data.room_id
            },
            data: {
                message: {
                    push: json
                }
            }
        });
        return json;
    }
};
ChatService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService,
        users_service_1.UsersService])
], ChatService);
exports.ChatService = ChatService;
//# sourceMappingURL=chat.service.js.map