import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateMsgDto } from './dto/update-chat.dto';
import { PrismaService } from "../prisma/prisma.service";
import { UsersService } from '../users/users.service';
export declare class ChatService {
    private prisma;
    private readonly usersService;
    constructor(prisma: PrismaService, usersService: UsersService);
    decrypt(password: string, room_uid: string): Promise<boolean>;
    create(chat: CreateChatDto): Promise<import(".prisma/client").room>;
    findAll(): import(".prisma/client").Prisma.PrismaPromise<import(".prisma/client").room[]>;
    findAllMessages(room_id: string): Promise<import(".prisma/client").Prisma.JsonValue[]>;
    findOne(id: number): Promise<import(".prisma/client").room>;
    findOneByRoomId(room_id: string): Promise<import(".prisma/client").room>;
    findMembersByRoomId(id: string): Promise<any[]>;
    updateChannelChat(id: number, intra_id: string, action: string): Promise<import(".prisma/client").room>;
    remove(room_id: number): string;
    message(data: UpdateMsgDto): Promise<{
        message: string;
        user_id: string;
        user_name: string;
        avatar: string;
        sentAt: any;
    }>;
}
