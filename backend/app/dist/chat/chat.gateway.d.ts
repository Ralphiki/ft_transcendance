import { ChatService } from './chat.service';
import { CreateChatDto } from './dto/create-chat.dto';
import { UpdateMsgDto } from './dto/update-chat.dto';
import { Socket } from 'socket.io';
import { Server } from 'socket.io';
export declare class ChatGateway {
    private readonly chatService;
    constructor(chatService: ChatService);
    private user2socket;
    server: Server;
    handleEvent(data: CreateChatDto): Promise<import(".prisma/client").room>;
    checkIfRoomExists(roomid: string): Promise<import(".prisma/client").room>;
    getPreviousMessages(roomid: string): Promise<import(".prisma/client").Prisma.JsonValue[]>;
    publicroom(action: string, create: CreateChatDto): any;
    newMessage(client: Socket, data: {
        json: UpdateMsgDto;
        roomid: string;
    }): Promise<{
        message: string;
        user_id: string;
        user_name: string;
        avatar: string;
        sentAt: any;
    }>;
    joinRoom(data: {
        name: string;
        intra_id: number;
    }, client: Socket): string;
    kickMember(client: Socket, data: {
        intra_id: number;
        roomid: string;
    }): Promise<void>;
    muteMember(client: Socket, data: {
        intra_id: number;
        roomid: string;
    }): Promise<void>;
    updateUnMute(client: Socket, data: {
        intra_id: number;
        roomid: string;
    }): void;
    leaveRoom(client: Socket, roomid: string): void;
    decrypt(client: Socket, data: {
        intra_id: number;
        password: string;
        roomid: string;
    }): Promise<void>;
    remove(client: Socket, id: number): string;
    updateBlockUser(client: Socket, data: {
        user: any;
        target: any;
        isBlocked: boolean;
    }): void;
    addAdmin(client: Socket, data: {
        intra_id: number;
        roomid: string;
    }): void;
    updateKick(client: Socket, data: {
        intra_id: number;
        roomid: string;
    }): void;
}
