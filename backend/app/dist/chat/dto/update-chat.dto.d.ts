export declare class UpdateMsgDto {
    user_id: string;
    user_name: string;
    user_avatar: string;
    room_id: string;
    message_data: string;
    sentAt: any;
}
export declare class UpdateChatDto {
    owner: string;
    typechat: string;
    intra_id: string;
    name: string;
    admin: string[];
    banned: string[];
    members: string[];
    message: string[];
    password: string;
    room_uid: string;
    action: string;
}
