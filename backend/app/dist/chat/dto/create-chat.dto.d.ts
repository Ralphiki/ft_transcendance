export declare class CreateData {
    message: string;
}
export declare class CreateChatDto {
    owner: string;
    typechat: string;
    name: string;
    admin: string[];
    banned: string[];
    members: string[];
    message: string[];
    password: string;
    room_uid: string;
    muted: string[];
}
