import { ChatService } from './chat.service';
import { Chat } from './entities/chat.entity';
import { CreateChatDto } from "./dto/create-chat.dto";
import { UpdateChatDto } from "./dto/update-chat.dto";
export declare class ChatController {
    private readonly chatService;
    constructor(chatService: ChatService);
    create(createChatDto: CreateChatDto): Promise<Chat>;
    findAll(): Promise<Chat[]>;
    findOne(id: number): Promise<Chat>;
    findOneByRoomid(id: string): Promise<Chat>;
    findMembersByRoomid(id: string): Promise<Chat>;
    update(id: number, updateChatDto: UpdateChatDto): Promise<Chat>;
    remove(id: number): Promise<Chat>;
}
