"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const chat_service_1 = require("./chat.service");
const create_chat_dto_1 = require("./dto/create-chat.dto");
const socket_io_1 = require("socket.io");
const socket_io_2 = require("socket.io");
var jwt = require('jsonwebtoken');
let ChatGateway = class ChatGateway {
    constructor(chatService) {
        this.chatService = chatService;
        this.user2socket = new Map();
    }
    handleEvent(data) {
        return this.chatService.create(data);
    }
    async checkIfRoomExists(roomid) {
        return this.chatService.findOneByRoomId(roomid);
    }
    async getPreviousMessages(roomid) {
        const msg = await this.chatService.findAllMessages(roomid);
        return msg;
    }
    async publicroom(action, create) {
        return this.publicroom(action, create);
    }
    async newMessage(client, data) {
        const msg = await this.chatService.message(data.json);
        if (data.roomid)
            this.server.to(data.roomid).emit('broadcast_mp', msg);
        return msg;
    }
    joinRoom(data, client) {
        if (data.intra_id !== undefined) {
            console.log(data.intra_id + " connected on app with chatsocket " + client.id);
            this.user2socket.set(data.intra_id, client.id);
        }
        client.join(data.name);
        return this.user2socket.get(data.intra_id);
    }
    async kickMember(client, data) {
        let client_socket = this.user2socket.get(+data.intra_id);
        if (client_socket)
            this.server.to(client_socket).emit('been_kicked');
    }
    async muteMember(client, data) {
        let client_socket = this.user2socket.get(+data.intra_id);
        if (client_socket)
            this.server.to(client_socket).emit('been_muted');
    }
    updateUnMute(client, data) {
        let client_socket = this.user2socket.get(+data.intra_id);
        if (client_socket)
            this.server.to(client_socket).emit('been_unmuted');
    }
    leaveRoom(client, roomid) {
        if (client)
            client.leave(roomid);
    }
    async decrypt(client, data) {
        let client_socket = this.user2socket.get(+data.intra_id);
        let right_password = await this.chatService.decrypt(data.password, data.roomid);
        if (client_socket)
            this.server.to(client_socket).emit('been_decrypted', right_password);
    }
    remove(client, id) {
        return this.chatService.remove(id);
    }
    updateBlockUser(client, data) {
        console.log(data.user.username + " " + client.id + " blocks " + data.target.username + " " + data.target.intra_id);
        console.log(this.user2socket);
        console.log(typeof data.target.intra_id);
        const client_socket = this.user2socket.get(data.target.intra_id);
        if (client_socket) {
            console.log("BLOCK " + client_socket);
            this.server.to(client_socket).emit("updateBlockUser", { user: data.user, isBlocked: data.isBlocked });
        }
    }
    addAdmin(client, data) {
        let client_socket = this.user2socket.get(+data.intra_id);
        if (client_socket)
            this.server.to(client_socket).emit('made_admin', data.roomid);
    }
    updateKick(client, data) {
        let client_socket = this.user2socket.get(+data.intra_id);
        if (client_socket)
            this.server.to(client_socket).emit('been_banned', data.roomid);
    }
};
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", socket_io_2.Server)
], ChatGateway.prototype, "server", void 0);
__decorate([
    (0, websockets_1.SubscribeMessage)('create_room'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_chat_dto_1.CreateChatDto]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "handleEvent", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('get_room'),
    __param(0, (0, websockets_1.MessageBody)('roomid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "checkIfRoomExists", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('getMsgHistory'),
    __param(0, (0, websockets_1.MessageBody)('roomid')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "getPreviousMessages", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('public_room'),
    __param(0, (0, websockets_1.MessageBody)('action')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, create_chat_dto_1.CreateChatDto]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "publicroom", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('new_mp'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "newMessage", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('join_room'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, socket_io_1.Socket]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "joinRoom", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('kick_member'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "kickMember", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('mute_member'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "muteMember", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('unmute_member'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "updateUnMute", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('leave_room'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, String]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "leaveRoom", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('decrypt'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", Promise)
], ChatGateway.prototype, "decrypt", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('removeChat'),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Number]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "remove", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('updateBlockUser'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "updateBlockUser", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('add_admin'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "addAdmin", null);
__decorate([
    (0, websockets_1.SubscribeMessage)('ban_member'),
    __param(0, (0, websockets_1.ConnectedSocket)()),
    __param(1, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket, Object]),
    __metadata("design:returntype", void 0)
], ChatGateway.prototype, "updateKick", null);
ChatGateway = __decorate([
    (0, websockets_1.WebSocketGateway)({ cors: true }),
    __metadata("design:paramtypes", [chat_service_1.ChatService])
], ChatGateway);
exports.ChatGateway = ChatGateway;
//# sourceMappingURL=chat.gateway.js.map