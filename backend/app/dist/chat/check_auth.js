"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constant_1 = require("../auth/constant");
var jwt = require("jsonwebtoken");
class api_check {
    constructor(jwt) {
        this.jwter = jwt;
    }
    verify() {
        jwt.verify(this.jwter, constant_1.jwtConstants.secret, (err, verifiedJwt) => {
            if (err) {
                return err.message;
            }
            else {
                return verifiedJwt;
            }
        });
    }
}
module.exports = api_check;
//# sourceMappingURL=check_auth.js.map