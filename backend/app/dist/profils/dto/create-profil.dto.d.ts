export declare class CreateProfilDto {
    uid: number;
    image_url: string;
    hash: string;
    type: string;
}
