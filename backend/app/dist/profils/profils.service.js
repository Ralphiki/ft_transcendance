"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfilsService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const crypto_1 = require("crypto");
let ProfilsService = class ProfilsService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    create(data) {
        const temp = [];
        temp["image_url"] = "http://127.0.0.1:3001/profils/avatar/" + data;
        temp["hash"] = (0, crypto_1.randomUUID)();
        temp["type"] = "avatar";
        return this.prisma.profils.create({ data: {
                image_url: temp["image_url"],
                hash: temp["hash"],
                type: temp["type"]
            } });
    }
    findAll() {
        return this.prisma.profils.findMany();
    }
    findOne(id) {
        return this.prisma.profils.findUnique({ where: { uid: id } });
    }
    update(id, updateProfilDto) {
        return `This action updates a #${id} profil`;
    }
    remove(id) {
        return `This action removes a #${id} profil`;
    }
};
ProfilsService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], ProfilsService);
exports.ProfilsService = ProfilsService;
//# sourceMappingURL=profils.service.js.map