"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../prisma/prisma.service");
const urls_1 = require("../urls");
let UsersService = class UsersService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    create(createUserDto) {
        return this.prisma.users.create({ data: createUserDto });
    }
    findAll() {
        return this.prisma.users.findMany();
    }
    async findOne(uid) {
        if (uid === undefined) {
            console.log("find One returning...");
            return;
        }
        console.log("FIND ONNEEEEEEEEEE " + uid);
        const content = await this.prisma.users.findUnique({
            where: { intra_id: uid },
        });
        return content;
    }
    async findOneByUsername(username) {
        const content = await this.prisma.users.findFirst({
            where: { username: username },
        });
        console.log("content " + content);
        return content;
    }
    async updateprofile(id, updateUserDto) {
        const check = await this.findOneByUsername(updateUserDto.username);
        if (check && check.intra_id != id)
            throw new common_1.HttpException({ message: ['Username must be unique'] }, common_1.HttpStatus.BAD_REQUEST);
        return await this.prisma.users.update({
            where: { intra_id: id },
            data: updateUserDto,
        });
    }
    async update(id, updateUserDto) {
        return await this.prisma.users.update({
            where: { intra_id: id },
            data: updateUserDto,
        });
    }
    async upload(intra_id, filename) {
        let path = urls_1.backendURL + "/" + filename;
        console.log("path. " + path);
        return await this.prisma.users.update({
            where: { intra_id: intra_id },
            data: {
                avatar: path,
            }
        });
    }
    remove(id) {
        return this.prisma.users.delete({ where: { intra_id: id } });
    }
    async addfriends(friends, intra_id) {
        await this.prisma.users.update({
            where: { intra_id: +friends },
            data: {
                friends: {
                    push: intra_id.toString(),
                },
            },
        });
        return this.prisma.users.update({
            where: { intra_id: intra_id },
            data: {
                friends: {
                    push: friends,
                },
            },
        });
    }
    async deletefriends(friend, idintra) {
        const user1 = await this.prisma.users.findUnique({
            where: {
                intra_id: +friend,
            },
        });
        await this.prisma.users.update({
            where: { intra_id: +friend },
            data: {
                friends: {
                    set: user1.friends.filter((id) => id !== idintra.toString()),
                }
            },
        });
        const user2 = await this.prisma.users.findUnique({
            where: {
                intra_id: idintra,
            },
        });
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                friends: {
                    set: user2.friends.filter((id) => id !== friend),
                }
            },
        });
    }
    async addblocked(blocked, idintra) {
        const user = await this.findOne(idintra);
        if (user.blacklist.includes(blocked))
            return user;
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                blacklist: {
                    push: blocked,
                },
            },
        });
    }
    async deleteblocked(blocked, idintra) {
        const user = await this.prisma.users.findUnique({
            where: {
                intra_id: idintra,
            },
        });
        const find = user.blacklist.indexOf(blocked);
        if (find == -1)
            return user;
        return this.prisma.users.update({
            where: { intra_id: idintra },
            data: {
                blacklist: {
                    set: user.blacklist.filter((id) => id !== blocked),
                }
            },
        });
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map