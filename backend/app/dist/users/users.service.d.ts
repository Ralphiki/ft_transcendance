import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from '../prisma/prisma.service';
import { Prisma } from '@prisma/client';
export declare class UsersService {
    private prisma;
    constructor(prisma: PrismaService);
    create(createUserDto: CreateUserDto): Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    findAll(): Prisma.PrismaPromise<import(".prisma/client").Users[]>;
    findOne(uid: number): Promise<import(".prisma/client").Users>;
    findOneByUsername(username: string): Promise<import(".prisma/client").Users>;
    updateprofile(id: number, updateUserDto: UpdateUserDto): Promise<import(".prisma/client").Users>;
    update(id: number, updateUserDto: UpdateUserDto): Promise<import(".prisma/client").Users>;
    upload(intra_id: number, filename: string): Promise<import(".prisma/client").Users>;
    remove(id: number): Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    addfriends(friends: string, intra_id: number): Promise<import(".prisma/client").Users>;
    deletefriends(friend: string, idintra: number): Promise<import(".prisma/client").Users>;
    addblocked(blocked: string, idintra: number): Promise<import(".prisma/client").Users>;
    deleteblocked(blocked: string, idintra: number): Promise<import(".prisma/client").Users>;
}
