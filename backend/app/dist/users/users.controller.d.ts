/// <reference types="multer" />
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUserDto: CreateUserDto): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    findAll(): import(".prisma/client").Prisma.PrismaPromise<import(".prisma/client").Users[]>;
    findOne(id: string, req: any): Promise<import(".prisma/client").Users>;
    findOneByUsername(username: string, req: any): Promise<import(".prisma/client").Users>;
    updateprofile(id: string, updateUserDto: UpdateUserDto): Promise<import(".prisma/client").Users>;
    update(id: string, updateUserDto: UpdateUserDto): Promise<import(".prisma/client").Users>;
    upload(id: string, file: Express.Multer.File): Promise<import(".prisma/client").Users>;
    seeUploadedFile(image: any, res: any): any;
    getinfo(req: any): any;
    remove(id: string): import(".prisma/client").Prisma.Prisma__UsersClient<import(".prisma/client").Users, never>;
    addfriends(friends: string, req: any): Promise<import(".prisma/client").Users>;
    deletefriends(friends: string, req: any): Promise<import(".prisma/client").Users>;
    addblocked(blocked: string, req: any): Promise<import(".prisma/client").Users>;
    deleteblocked(blocked: string, req: any): Promise<import(".prisma/client").Users>;
}
