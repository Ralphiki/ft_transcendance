export declare type User = {
    uid: number;
    username: string | null;
    email: string;
    intra_id: number;
    games: number[];
    groups: string[];
};
