"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const swagger_1 = require("@nestjs/swagger");
const session = require("express-session");
const passport = require("passport");
const prisma_session_store_1 = require("@quixo3/prisma-session-store");
const client_1 = require("@prisma/client");
const bodyParser = require("body-parser");
const path_1 = require("path");
const node_fs_1 = require("node:fs");
async function bootstrap() {
    const httpsOptions = {
        key: (0, node_fs_1.readFileSync)('/cert/key.pem'),
        cert: (0, node_fs_1.readFileSync)('/cert/certificate.pem'),
    };
    const app = await core_1.NestFactory.create(app_module_1.AppModule, { httpsOptions });
    const config = new swagger_1.DocumentBuilder()
        .setTitle('Transcendance api')
        .setDescription('Api for transcendance')
        .setVersion('0.1')
        .addBearerAuth({
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'JWT',
        description: 'Enter JWT token',
        in: 'header',
    }, 'JWT-auth')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('api', app, document);
    app.enableCors({});
    app.use(function (req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    });
    app.use(bodyParser.json({ limit: '200mb' }));
    app.use(bodyParser.urlencoded({ limit: '200mb', extended: true }));
    app.use(session({
        cookie: {
            secure: true,
            httpOnly: false
        },
        secret: 'cocochoc',
        resave: false,
        saveUninitialized: false,
        store: new prisma_session_store_1.PrismaSessionStore(new client_1.PrismaClient(), {
            checkPeriod: 2 * 60 * 1000,
            dbRecordIdIsSessionId: true,
            dbRecordIdFunction: undefined,
        }),
    }));
    app.useStaticAssets((0, path_1.join)(__dirname, '..', 'uploads'));
    app.use(passport.initialize());
    app.use(passport.session());
    await app.listen(3001);
}
bootstrap();
//# sourceMappingURL=main.js.map